classdef gyroparams< fusion.internal.IMUSensorParameters & fusion.internal.UnitDisplayerValue
%   GYROPARAMS Gyroscope sensor parameters
%   params = GYROPARAMS returns a gyroscope parameter object with  default
%   values assigned to each property.
%
%   params = GYROPARAMS('Name', Value) returns a gyroscope parameter object
%   with each specified property name set to the specified value. You can
%   specify additional name-value pair arguments in any order as
%   (Name1, Value1, ...,NameN, ValueN).
%
%   GYROPARAMS properties:
%
%   MeasurementRange          - Maximum sensor reading (rad/s)
%   Resolution                - Resolution of sensor measurements
%                               (rad/s/LSB)
%   ConstantBias              - Constant sensor offset bias (rad/s)
%   AxesMisalignment          - Sensor axes skew (%)
%   NoiseDensity              - Power spectral density of sensor noise
%                               (rad/s/sqrt(Hz))
%   BiasInstability           - Instability of the bias offset (rad/s)
%   RandomWalk                - Integrated white noise of sensor
%                               ((rad/s)*sqrt(Hz))
%   TemperatureBias           - Sensor bias from temperature
%                               (rad/s/degrees C)
%   TemperatureScaleFactor    - Scale factor error from temperature 
%                               (%/degrees C)
%   AccelerationBias          - Sensor bias from linear acceleration
%                               ((rad/s) / (m/s^2))
%
%   EXAMPLE: Generate ideal gyroscope data from stationary input.
%
%   Fs = 100;
%   numSamples = 1000;
%   t = 0:1/Fs:(numSamples-1)/Fs;
%
%   params = gyroparams;
%   imu = imuSensor('accel-gyro', 'SampleRate', Fs, 'Gyroscope', params);
%
%   acc = zeros(numSamples, 3);
%   angvel = zeros(numSamples, 3);
%
%   [~, gyroData] = imu(acc, angvel);
%
%   plot(t, gyroData)
%   title('Gyroscope')
%   xlabel('s')
%   ylabel('rad/s')
%
%   See also ACCELPARAMS, MAGPARAMS, IMUSENSOR

 
%   Copyright 2017-2019 The MathWorks, Inc.

    methods
        function out=gyroparams
            %   GYROPARAMS Gyroscope sensor parameters
            %   params = GYROPARAMS returns a gyroscope parameter object with  default
            %   values assigned to each property.
            %
            %   params = GYROPARAMS('Name', Value) returns a gyroscope parameter object
            %   with each specified property name set to the specified value. You can
            %   specify additional name-value pair arguments in any order as
            %   (Name1, Value1, ...,NameN, ValueN).
            %
            %   GYROPARAMS properties:
            %
            %   MeasurementRange          - Maximum sensor reading (rad/s)
            %   Resolution                - Resolution of sensor measurements
            %                               (rad/s/LSB)
            %   ConstantBias              - Constant sensor offset bias (rad/s)
            %   AxesMisalignment          - Sensor axes skew (%)
            %   NoiseDensity              - Power spectral density of sensor noise
            %                               (rad/s/sqrt(Hz))
            %   BiasInstability           - Instability of the bias offset (rad/s)
            %   RandomWalk                - Integrated white noise of sensor
            %                               ((rad/s)*sqrt(Hz))
            %   TemperatureBias           - Sensor bias from temperature
            %                               (rad/s/degrees C)
            %   TemperatureScaleFactor    - Scale factor error from temperature 
            %                               (%/degrees C)
            %   AccelerationBias          - Sensor bias from linear acceleration
            %                               ((rad/s) / (m/s^2))
            %
            %   EXAMPLE: Generate ideal gyroscope data from stationary input.
            %
            %   Fs = 100;
            %   numSamples = 1000;
            %   t = 0:1/Fs:(numSamples-1)/Fs;
            %
            %   params = gyroparams;
            %   imu = imuSensor('accel-gyro', 'SampleRate', Fs, 'Gyroscope', params);
            %
            %   acc = zeros(numSamples, 3);
            %   angvel = zeros(numSamples, 3);
            %
            %   [~, gyroData] = imu(acc, angvel);
            %
            %   plot(t, gyroData)
            %   title('Gyroscope')
            %   xlabel('s')
            %   ylabel('rad/s')
            %
            %   See also ACCELPARAMS, MAGPARAMS, IMUSENSOR
        end

        function createSystemObjectImpl(in) %#ok<MANU>
        end

        function getDisplayUnitImpl(in) %#ok<MANU>
        end

        function getPropertyGroups(in) %#ok<MANU>
        end

        function updateSystemObjectImpl(in) %#ok<MANU>
        end

    end
    methods (Abstract)
    end
    properties
        % AccelerationBias Sensor bias from acceleration ((rad/s)/(m/s^2))
        % Specify the acceleration bias as a real scalar or 3-element row
        % vector. The default value is [0 0 0].
        AccelerationBias;

    end
end
