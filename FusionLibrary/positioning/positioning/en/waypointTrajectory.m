classdef waypointTrajectory< fusion.internal.PositioningSystemBase & matlab.system.mixin.FiniteSource & fusion.scenario.internal.mixin.PlatformTrajectory
%WAYPOINTTRAJECTORY Waypoint trajectory generator
%   TRAJ = WAYPOINTTRAJECTORY returns a System object, TRAJ, that generates
%   a trajectory based on default stationary waypoints.
%
%   TRAJ = WAYPOINTTRAJECTORY(POINTS, T) returns a System object, TRAJ,
%   that generates a trajectory based on the specified waypoints POINTS and
%   times T. POINTS is an N-by-3 matrix that specifies the positions along
%   the trajectory. T is an N-element vector that specifies the times at
%   which the trajectory crosses the corresponding waypoints.
%
%   TRAJ = WAYPOINTTRAJECTORY(POINTS, T, ..., 'Velocities', VEL) returns a
%   WAYPOINTTRAJECTORY System object that generates a trajectory based on
%   the specified velocities VEL. VEL is an N-by-3 matrix that specifies
%   the velocities at the corresponding waypoints and times. If
%   'Velocities' is not specified, then it is inferred from the waypoints.
%
%   TRAJ = WAYPOINTTRAJECTORY(POINTS, T, ..., 'Orientation', ORIENT)
%   returns a WAYPOINTTRAJECTORY System object that generates a trajectory
%   based on the specified orientation ORIENT. ORIENT is a quaternion
%   N-element column vector or a 3-by-3-by-N rotation matrix that specifies
%   the orientation at the corresponding waypoints and times. If
%   'Orientation' is not specified, then it is inferred from the waypoints.
%
%   TRAJ = WAYPOINTTRAJECTORY(..., 'Name', Value, ...) returns a
%   WAYPOINTTRAJECTORY System object with each specified property name set
%   to the specified value. You can specify additional name-value pair
%   arguments in any order as (Name1,Value1,...,NameN, ValueN).
%
%   Step method syntax:
%
%   [POS, ORIENT, VEL, ACC, ANGVEL] = step(TRAJ) outputs a frame of 
%   trajectory data based on the specified waypoints.
%
%   The outputs of WAYPOINTTRAJECTORY are defined as follows:
%
%       POS       Position in the local navigation coordinate system 
%                 specified as a real finite N-by-3 array in meters. N is
%                 specified by the SamplesPerFrame property.
%
%       ORIENT    Orientation with respect to the local navigation 
%                 coordinate system specified as a quaternion N-element
%                 column vector or a 3-by-3-by-N rotation matrix. Each
%                 quaternion or rotation matrix is a frame rotation from
%                 the local navigation coordinate system to the current
%                 body coordinate system. N is specified by the
%                 SamplesPerFrame property.
%
%       VEL       Velocity in the local navigation coordinate system 
%                 specified as a real finite N-by-3 array in meters per
%                 second. N is specified by the SamplesPerFrame property.
%
%       ACC       Acceleration in the local navigation coordinate system 
%                 specified as a real finite N-by-3 array in meters per
%                 second squared. N is specified by the SamplesPerFrame
%                 property.
%
%       ANGVEL    Angular velocity in the local navigation coordinate 
%                 system specified as a real finite N-by-3 array in radians
%                 per second. N is specified by the SamplesPerFrame
%                 property.
%
%   System objects may be called directly like a function instead of using
%   the step method. For example, y = step(obj, x) and y = obj(x) are
%   equivalent.
%
%   WAYPOINTTRAJECTORY methods:
%
%   step            - See above description for use of this method
%   waypointInfo    - Get waypoint information for the specified trajectory
%   release         - Allow property value and input characteristics to 
%                     change, and release WAYPOINTTRAJECTORY resources
%   clone           - Create WAYPOINTTRAJECTORY object with same property 
%                     values
%   isLocked        - Display locked status (logical)
%   <a href="matlab:help matlab.System/reset   ">reset</a>           - Reset the states of the WAYPOINTTRAJECTORY
%   isDone          - True if entire trajectory has been output
%
%   WAYPOINTTRAJECTORY properties:
%
%   SampleRate         - Sample rate of trajectory (Hz)
%   SamplesPerFrame    - Number of samples in the output
%
%   % EXAMPLE 1: Visualize specified waypoints and computed position.
%   % Record the generated position and verify that it passes through the
%   % specified waypoints.
%   Fs = 50;
%   wps = [0 0 0;
%          1 0 0;
%          1 1 0;
%          1 2 0;
%          1 3 0];
%   t = 0:(size(wps,1)-1);
%   
%   traj = waypointTrajectory(wps, t, 'SampleRate', Fs);
%   
%   waypointTable = waypointInfo(traj);
%   waypoints = waypointTable.Waypoints;
%   
%   pos = traj();
%   while ~isDone(traj)
%       pos(end+traj.SamplesPerFrame,:) = traj();
%   end
%   % Plot generated positions and specified waypoints.
%   plot(pos(:,1),pos(:,2), waypoints(:,1),waypoints(:,2), '--o')
%   title('Position')
%   xlabel('X (m)')
%   ylabel('Y (m)')
%   zlabel('Z (m)')
%   legend({'Position', 'Waypoints'})
%
%   % EXAMPLE 2: Generate a racetrack trajectory by specifying the velocity
%   % and orientation at each waypoint.
%   Fs = 100;
%   wps = [0 0 0;
%          20 0 0;
%          20 5 0;
%          0 5 0;
%          0 0 0];
%   t = cumsum([0 10 1.25*pi 10 1.25*pi]).';
%   vels = [2 0 0;
%          2 0 0;
%          -2 0 0;
%          -2 0 0;
%          2 0 0];
%   eulerAngs = [0 0 0;
%                0 0 0;
%                180 0 0;
%                180 0 0;
%                0 0 0];
%   q = quaternion(eulerAngs, 'eulerd', 'ZYX', 'frame');
%   
%   traj = waypointTrajectory(wps, 'SampleRate', Fs, ...
%       'TimeOfArrival', t, 'Velocities', vels, 'Orientation', q);
%   
%   waypointTable = waypointInfo(traj);
%   waypoints = waypointTable.Waypoints;
%   
%   [pos, orient, vel, acc, angvel] = traj();
%   i = 1;
%   spf = traj.SamplesPerFrame;
%   while ~isDone(traj)
%       idx = (i+1):(i+spf);
%       [pos(idx,:), orient(idx,:), ...
%           vel(idx,:), acc(idx,:), angvel(idx,:)] = traj();
%       i = i+spf;
%   end
%   % Plot generated positions and specified waypoints.
%   plot(pos(:,1),pos(:,2), waypoints(:,1),waypoints(:,2), '--o')
%   title('Position')
%   xlabel('X (m)')
%   ylabel('Y (m)')
%   zlabel('Z (m)')
%   legend({'Position', 'Waypoints'})
%   axis equal
%
%   See also IMUSENSOR, GPSSENSOR, KINEMATICTRAJECTORY

 
%   Copyright 2018-2019 The MathWorks, Inc.

    methods
        function out=waypointTrajectory
            %WAYPOINTTRAJECTORY Waypoint trajectory generator
            %   TRAJ = WAYPOINTTRAJECTORY returns a System object, TRAJ, that generates
            %   a trajectory based on default stationary waypoints.
            %
            %   TRAJ = WAYPOINTTRAJECTORY(POINTS, T) returns a System object, TRAJ,
            %   that generates a trajectory based on the specified waypoints POINTS and
            %   times T. POINTS is an N-by-3 matrix that specifies the positions along
            %   the trajectory. T is an N-element vector that specifies the times at
            %   which the trajectory crosses the corresponding waypoints.
            %
            %   TRAJ = WAYPOINTTRAJECTORY(POINTS, T, ..., 'Velocities', VEL) returns a
            %   WAYPOINTTRAJECTORY System object that generates a trajectory based on
            %   the specified velocities VEL. VEL is an N-by-3 matrix that specifies
            %   the velocities at the corresponding waypoints and times. If
            %   'Velocities' is not specified, then it is inferred from the waypoints.
            %
            %   TRAJ = WAYPOINTTRAJECTORY(POINTS, T, ..., 'Orientation', ORIENT)
            %   returns a WAYPOINTTRAJECTORY System object that generates a trajectory
            %   based on the specified orientation ORIENT. ORIENT is a quaternion
            %   N-element column vector or a 3-by-3-by-N rotation matrix that specifies
            %   the orientation at the corresponding waypoints and times. If
            %   'Orientation' is not specified, then it is inferred from the waypoints.
            %
            %   TRAJ = WAYPOINTTRAJECTORY(..., 'Name', Value, ...) returns a
            %   WAYPOINTTRAJECTORY System object with each specified property name set
            %   to the specified value. You can specify additional name-value pair
            %   arguments in any order as (Name1,Value1,...,NameN, ValueN).
            %
            %   Step method syntax:
            %
            %   [POS, ORIENT, VEL, ACC, ANGVEL] = step(TRAJ) outputs a frame of 
            %   trajectory data based on the specified waypoints.
            %
            %   The outputs of WAYPOINTTRAJECTORY are defined as follows:
            %
            %       POS       Position in the local navigation coordinate system 
            %                 specified as a real finite N-by-3 array in meters. N is
            %                 specified by the SamplesPerFrame property.
            %
            %       ORIENT    Orientation with respect to the local navigation 
            %                 coordinate system specified as a quaternion N-element
            %                 column vector or a 3-by-3-by-N rotation matrix. Each
            %                 quaternion or rotation matrix is a frame rotation from
            %                 the local navigation coordinate system to the current
            %                 body coordinate system. N is specified by the
            %                 SamplesPerFrame property.
            %
            %       VEL       Velocity in the local navigation coordinate system 
            %                 specified as a real finite N-by-3 array in meters per
            %                 second. N is specified by the SamplesPerFrame property.
            %
            %       ACC       Acceleration in the local navigation coordinate system 
            %                 specified as a real finite N-by-3 array in meters per
            %                 second squared. N is specified by the SamplesPerFrame
            %                 property.
            %
            %       ANGVEL    Angular velocity in the local navigation coordinate 
            %                 system specified as a real finite N-by-3 array in radians
            %                 per second. N is specified by the SamplesPerFrame
            %                 property.
            %
            %   System objects may be called directly like a function instead of using
            %   the step method. For example, y = step(obj, x) and y = obj(x) are
            %   equivalent.
            %
            %   WAYPOINTTRAJECTORY methods:
            %
            %   step            - See above description for use of this method
            %   waypointInfo    - Get waypoint information for the specified trajectory
            %   release         - Allow property value and input characteristics to 
            %                     change, and release WAYPOINTTRAJECTORY resources
            %   clone           - Create WAYPOINTTRAJECTORY object with same property 
            %                     values
            %   isLocked        - Display locked status (logical)
            %   <a href="matlab:help matlab.System/reset   ">reset</a>           - Reset the states of the WAYPOINTTRAJECTORY
            %   isDone          - True if entire trajectory has been output
            %
            %   WAYPOINTTRAJECTORY properties:
            %
            %   SampleRate         - Sample rate of trajectory (Hz)
            %   SamplesPerFrame    - Number of samples in the output
            %
            %   % EXAMPLE 1: Visualize specified waypoints and computed position.
            %   % Record the generated position and verify that it passes through the
            %   % specified waypoints.
            %   Fs = 50;
            %   wps = [0 0 0;
            %          1 0 0;
            %          1 1 0;
            %          1 2 0;
            %          1 3 0];
            %   t = 0:(size(wps,1)-1);
            %   
            %   traj = waypointTrajectory(wps, t, 'SampleRate', Fs);
            %   
            %   waypointTable = waypointInfo(traj);
            %   waypoints = waypointTable.Waypoints;
            %   
            %   pos = traj();
            %   while ~isDone(traj)
            %       pos(end+traj.SamplesPerFrame,:) = traj();
            %   end
            %   % Plot generated positions and specified waypoints.
            %   plot(pos(:,1),pos(:,2), waypoints(:,1),waypoints(:,2), '--o')
            %   title('Position')
            %   xlabel('X (m)')
            %   ylabel('Y (m)')
            %   zlabel('Z (m)')
            %   legend({'Position', 'Waypoints'})
            %
            %   % EXAMPLE 2: Generate a racetrack trajectory by specifying the velocity
            %   % and orientation at each waypoint.
            %   Fs = 100;
            %   wps = [0 0 0;
            %          20 0 0;
            %          20 5 0;
            %          0 5 0;
            %          0 0 0];
            %   t = cumsum([0 10 1.25*pi 10 1.25*pi]).';
            %   vels = [2 0 0;
            %          2 0 0;
            %          -2 0 0;
            %          -2 0 0;
            %          2 0 0];
            %   eulerAngs = [0 0 0;
            %                0 0 0;
            %                180 0 0;
            %                180 0 0;
            %                0 0 0];
            %   q = quaternion(eulerAngs, 'eulerd', 'ZYX', 'frame');
            %   
            %   traj = waypointTrajectory(wps, 'SampleRate', Fs, ...
            %       'TimeOfArrival', t, 'Velocities', vels, 'Orientation', q);
            %   
            %   waypointTable = waypointInfo(traj);
            %   waypoints = waypointTable.Waypoints;
            %   
            %   [pos, orient, vel, acc, angvel] = traj();
            %   i = 1;
            %   spf = traj.SamplesPerFrame;
            %   while ~isDone(traj)
            %       idx = (i+1):(i+spf);
            %       [pos(idx,:), orient(idx,:), ...
            %           vel(idx,:), acc(idx,:), angvel(idx,:)] = traj();
            %       i = i+spf;
            %   end
            %   % Plot generated positions and specified waypoints.
            %   plot(pos(:,1),pos(:,2), waypoints(:,1),waypoints(:,2), '--o')
            %   title('Position')
            %   xlabel('X (m)')
            %   ylabel('Y (m)')
            %   zlabel('Z (m)')
            %   legend({'Position', 'Waypoints'})
            %   axis equal
            %
            %   See also IMUSENSOR, GPSSENSOR, KINEMATICTRAJECTORY
        end

        function fetchOrientationFromQuaternions(in) %#ok<MANU>
        end

        function fetchPosition(in) %#ok<MANU>
        end

        function isDoneImpl(in) %#ok<MANU>
        end

        function loadObjectImpl(in) %#ok<MANU>
            % Load public properties.
        end

        function lookupPose(in) %#ok<MANU>
            % interpolate motion from piecewise model
        end

        function resetImpl(in) %#ok<MANU>
        end

        function saveObjectImpl(in) %#ok<MANU>
            % Save public properties.
        end

        function setOrientation(in) %#ok<MANU>
        end

        function setProperties(in) %#ok<MANU>
        end

        function setTimeOfArrival(in) %#ok<MANU>
        end

        function setVelocities(in) %#ok<MANU>
        end

        function setWaypoints(in) %#ok<MANU>
        end

        function setupImpl(in) %#ok<MANU>
        end

        function setupInterpolants(in) %#ok<MANU>
        end

        function setupOrientationInterpolant(in) %#ok<MANU>
        end

        function setupPositionInterpolant(in) %#ok<MANU>
        end

        function setupWaypointParams(in) %#ok<MANU>
        end

        function stepImpl(in) %#ok<MANU>
        end

        function validateWaypointSizes(in) %#ok<MANU>
        end

        function waypointInfo(in) %#ok<MANU>
            %WAYPOINTINFO Get waypoint information table
            %
            %   WPTABLE = WAYPOINTINFO(TRAJ) returns a table of waypoints,
            %   times of arrival, velocities, and orientation for the 
            %   System object, TRAJ.
        end

    end
    methods (Abstract)
    end
    properties
        % SampleRate Sampling rate (Hz)
        % Specify the sampling frequency of the trajectory as a positive
        % scalar. The default value is 100. This property is tunable.
        SampleRate;

        % SamplesPerFrame Number of samples per output frame
        % Specify the number of samples to buffer into each output frame.
        % The default value is 1.
        SamplesPerFrame;

    end
end
