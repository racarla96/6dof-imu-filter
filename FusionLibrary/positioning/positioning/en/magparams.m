classdef magparams< fusion.internal.IMUSensorParameters & fusion.internal.UnitDisplayerValue
%   MAGPARAMS Magnetometer sensor parameters
%   params = MAGPARAMS returns a magnetometer parameter object with
%   default values assigned to each property.
%
%   params = MAGPARAMS('Name', Value) returns a magnetometer parameter
%   object with each specified property name set to the specified value.
%   You can specify additional name-value pair arguments in any order as
%   (Name1, Value1, ...,NameN, ValueN).
%
%   MAGPARAMS properties:
%
%   MeasurementRange          - Maximum sensor reading (uT)
%   Resolution                - Resolution of sensor measurements 
%                               (uT/LSB)
%   ConstantBias              - Constant sensor offset bias (uT)
%   AxesMisalignment          - Sensor axes skew (%)
%   NoiseDensity              - Power spectral density of sensor noise
%                               (uT/sqrt(Hz))
%   BiasInstability           - Instability of the bias offset (uT)
%   RandomWalk                - Integrated white noise of sensor
%                               (uT*sqrt(Hz))
%   TemperatureBias           - Sensor bias from temperature 
%                               (uT/degrees C)
%   TemperatureScaleFactor    - Scale factor error from temperature 
%                               (%/degrees C)
%
%   EXAMPLE: Generate ideal magnetometer data from stationary input.
%
%   Fs = 100;
%   numSamples = 1000;
%   t = 0:1/Fs:(numSamples-1)/Fs;
%
%   params = magparams;
%   imu = imuSensor('accel-mag', 'SampleRate', Fs, 'Magnetometer', params);
%
%   acc = zeros(numSamples, 3);
%   angvel = zeros(numSamples, 3);
%
%   [~, magData] = imu(acc, angvel);
%
%   plot(t, magData)
%   title('Magnetometer')
%   xlabel('s')
%   ylabel('uT')
%
%   See also ACCELPARAMS, GYROPARAMS, IMUSENSOR

 
%   Copyright 2017-2019 The MathWorks, Inc.

    methods
        function out=magparams
            %   MAGPARAMS Magnetometer sensor parameters
            %   params = MAGPARAMS returns a magnetometer parameter object with
            %   default values assigned to each property.
            %
            %   params = MAGPARAMS('Name', Value) returns a magnetometer parameter
            %   object with each specified property name set to the specified value.
            %   You can specify additional name-value pair arguments in any order as
            %   (Name1, Value1, ...,NameN, ValueN).
            %
            %   MAGPARAMS properties:
            %
            %   MeasurementRange          - Maximum sensor reading (uT)
            %   Resolution                - Resolution of sensor measurements 
            %                               (uT/LSB)
            %   ConstantBias              - Constant sensor offset bias (uT)
            %   AxesMisalignment          - Sensor axes skew (%)
            %   NoiseDensity              - Power spectral density of sensor noise
            %                               (uT/sqrt(Hz))
            %   BiasInstability           - Instability of the bias offset (uT)
            %   RandomWalk                - Integrated white noise of sensor
            %                               (uT*sqrt(Hz))
            %   TemperatureBias           - Sensor bias from temperature 
            %                               (uT/degrees C)
            %   TemperatureScaleFactor    - Scale factor error from temperature 
            %                               (%/degrees C)
            %
            %   EXAMPLE: Generate ideal magnetometer data from stationary input.
            %
            %   Fs = 100;
            %   numSamples = 1000;
            %   t = 0:1/Fs:(numSamples-1)/Fs;
            %
            %   params = magparams;
            %   imu = imuSensor('accel-mag', 'SampleRate', Fs, 'Magnetometer', params);
            %
            %   acc = zeros(numSamples, 3);
            %   angvel = zeros(numSamples, 3);
            %
            %   [~, magData] = imu(acc, angvel);
            %
            %   plot(t, magData)
            %   title('Magnetometer')
            %   xlabel('s')
            %   ylabel('uT')
            %
            %   See also ACCELPARAMS, GYROPARAMS, IMUSENSOR
        end

        function createSystemObjectImpl(in) %#ok<MANU>
        end

        function getDisplayUnitImpl(in) %#ok<MANU>
        end

        function getPropertyGroups(in) %#ok<MANU>
        end

    end
    methods (Abstract)
    end
end
