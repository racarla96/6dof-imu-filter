classdef waypointTrajectory < fusion.internal.PositioningSystemBase ...
        & matlab.system.mixin.FiniteSource ...
        & fusion.scenario.internal.mixin.PlatformTrajectory
%WAYPOINTTRAJECTORY Waypoint trajectory generator
%   TRAJ = WAYPOINTTRAJECTORY returns a System object, TRAJ, that generates
%   a trajectory based on default stationary waypoints.
%
%   TRAJ = WAYPOINTTRAJECTORY(POINTS, T) returns a System object, TRAJ,
%   that generates a trajectory based on the specified waypoints POINTS and
%   times T. POINTS is an N-by-3 matrix that specifies the positions along
%   the trajectory. T is an N-element vector that specifies the times at
%   which the trajectory crosses the corresponding waypoints.
%
%   TRAJ = WAYPOINTTRAJECTORY(POINTS, T, ..., 'Velocities', VEL) returns a
%   WAYPOINTTRAJECTORY System object that generates a trajectory based on
%   the specified velocities VEL. VEL is an N-by-3 matrix that specifies
%   the velocities at the corresponding waypoints and times. If
%   'Velocities' is not specified, then it is inferred from the waypoints.
%
%   TRAJ = WAYPOINTTRAJECTORY(POINTS, T, ..., 'Orientation', ORIENT)
%   returns a WAYPOINTTRAJECTORY System object that generates a trajectory
%   based on the specified orientation ORIENT. ORIENT is a quaternion
%   N-element column vector or a 3-by-3-by-N rotation matrix that specifies
%   the orientation at the corresponding waypoints and times. If
%   'Orientation' is not specified, then it is inferred from the waypoints.
%
%   TRAJ = WAYPOINTTRAJECTORY(..., 'Name', Value, ...) returns a
%   WAYPOINTTRAJECTORY System object with each specified property name set
%   to the specified value. You can specify additional name-value pair
%   arguments in any order as (Name1,Value1,...,NameN, ValueN).
%
%   Step method syntax:
%
%   [POS, ORIENT, VEL, ACC, ANGVEL] = step(TRAJ) outputs a frame of 
%   trajectory data based on the specified waypoints.
%
%   The outputs of WAYPOINTTRAJECTORY are defined as follows:
%
%       POS       Position in the local navigation coordinate system 
%                 specified as a real finite N-by-3 array in meters. N is
%                 specified by the SamplesPerFrame property.
%
%       ORIENT    Orientation with respect to the local navigation 
%                 coordinate system specified as a quaternion N-element
%                 column vector or a 3-by-3-by-N rotation matrix. Each
%                 quaternion or rotation matrix is a frame rotation from
%                 the local navigation coordinate system to the current
%                 body coordinate system. N is specified by the
%                 SamplesPerFrame property.
%
%       VEL       Velocity in the local navigation coordinate system 
%                 specified as a real finite N-by-3 array in meters per
%                 second. N is specified by the SamplesPerFrame property.
%
%       ACC       Acceleration in the local navigation coordinate system 
%                 specified as a real finite N-by-3 array in meters per
%                 second squared. N is specified by the SamplesPerFrame
%                 property.
%
%       ANGVEL    Angular velocity in the local navigation coordinate 
%                 system specified as a real finite N-by-3 array in radians
%                 per second. N is specified by the SamplesPerFrame
%                 property.
%
%   System objects may be called directly like a function instead of using
%   the step method. For example, y = step(obj, x) and y = obj(x) are
%   equivalent.
%
%   WAYPOINTTRAJECTORY methods:
%
%   step            - See above description for use of this method
%   waypointInfo    - Get waypoint information for the specified trajectory
%   release         - Allow property value and input characteristics to 
%                     change, and release WAYPOINTTRAJECTORY resources
%   clone           - Create WAYPOINTTRAJECTORY object with same property 
%                     values
%   isLocked        - Display locked status (logical)
%   reset           - Reset the states of the WAYPOINTTRAJECTORY
%   isDone          - True if entire trajectory has been output
%
%   WAYPOINTTRAJECTORY properties:
%
%   SampleRate         - Sample rate of trajectory (Hz)
%   SamplesPerFrame    - Number of samples in the output
%
%   % EXAMPLE 1: Visualize specified waypoints and computed position.
%   % Record the generated position and verify that it passes through the
%   % specified waypoints.
%   Fs = 50;
%   wps = [0 0 0;
%          1 0 0;
%          1 1 0;
%          1 2 0;
%          1 3 0];
%   t = 0:(size(wps,1)-1);
%   
%   traj = waypointTrajectory(wps, t, 'SampleRate', Fs);
%   
%   waypointTable = waypointInfo(traj);
%   waypoints = waypointTable.Waypoints;
%   
%   pos = traj();
%   while ~isDone(traj)
%       pos(end+traj.SamplesPerFrame,:) = traj();
%   end
%   % Plot generated positions and specified waypoints.
%   plot(pos(:,1),pos(:,2), waypoints(:,1),waypoints(:,2), '--o')
%   title('Position')
%   xlabel('X (m)')
%   ylabel('Y (m)')
%   zlabel('Z (m)')
%   legend({'Position', 'Waypoints'})
%
%   % EXAMPLE 2: Generate a racetrack trajectory by specifying the velocity
%   % and orientation at each waypoint.
%   Fs = 100;
%   wps = [0 0 0;
%          20 0 0;
%          20 5 0;
%          0 5 0;
%          0 0 0];
%   t = cumsum([0 10 1.25*pi 10 1.25*pi]).';
%   vels = [2 0 0;
%          2 0 0;
%          -2 0 0;
%          -2 0 0;
%          2 0 0];
%   eulerAngs = [0 0 0;
%                0 0 0;
%                180 0 0;
%                180 0 0;
%                0 0 0];
%   q = quaternion(eulerAngs, 'eulerd', 'ZYX', 'frame');
%   
%   traj = waypointTrajectory(wps, 'SampleRate', Fs, ...
%       'TimeOfArrival', t, 'Velocities', vels, 'Orientation', q);
%   
%   waypointTable = waypointInfo(traj);
%   waypoints = waypointTable.Waypoints;
%   
%   [pos, orient, vel, acc, angvel] = traj();
%   i = 1;
%   spf = traj.SamplesPerFrame;
%   while ~isDone(traj)
%       idx = (i+1):(i+spf);
%       [pos(idx,:), orient(idx,:), ...
%           vel(idx,:), acc(idx,:), angvel(idx,:)] = traj();
%       i = i+spf;
%   end
%   % Plot generated positions and specified waypoints.
%   plot(pos(:,1),pos(:,2), waypoints(:,1),waypoints(:,2), '--o')
%   title('Position')
%   xlabel('X (m)')
%   ylabel('Y (m)')
%   zlabel('Z (m)')
%   legend({'Position', 'Waypoints'})
%   axis equal
%
%   See also IMUSENSOR, GPSSENSOR, KINEMATICTRAJECTORY

%   Copyright 2018-2019 The MathWorks, Inc.

%#codegen

    properties
        % SampleRate Sampling rate (Hz)
        % Specify the sampling frequency of the trajectory as a positive
        % scalar. The default value is 100. This property is tunable.
        SampleRate = 100;
    end
    properties(Nontunable)
        % SamplesPerFrame Number of samples per output frame
        % Specify the number of samples to buffer into each output frame.
        % The default value is 1.
        SamplesPerFrame = 1;
    end
    
    properties (Access = private)
        % Waypoints Positions in the navigation frame (m)
        % Specify the position at each waypoint as an N-by-3 matrix in
        % meters. The default value is [0 0 0; 0 0 0].
        Waypoints;
        % TimeOfArrival Time at each waypoint (s)
        % Specify the times at which the trajectory crosses the
        % corresponding waypoints as an N-element vector. If it is not
        % specified, then it is inferred from each waypoint.
        TimeOfArrival;
        % Velocities Velocity in the navigation frame at each waypoint (m/s)
        % Specify the velocities at the corresponding waypoints as an
        % N-by-3 matrix. If it is not specified, then it is inferred from
        % each waypoint.
        Velocities;
        
        % Rotation matrix orientation at each waypoint
        RotMats;
        % Quaternion orientation at each waypoint
        Quaternions;
    end
    
    properties (Access = private, Dependent)
        % Orientation Orientation at each waypoint
        % Specify the orientation at the corresponding waypoints as a 
        % quaternion N-element vector or a 3-by-3-by-N rotation matrix.
        % Each quaternion or rotation matrix is a frame rotation from the
        % local navigation coordinate system to the current body coordinate
        % system. If it is not specified, then it is inferred from each
        % waypoint.
        Orientation;
    end
    
    properties (Nontunable, Access = private)
        IsWaypointsSpecified = false;
        IsTimeOfArrivalSpecified = false;
        IsVelocitiesSpecified = false;
        IsOrientationSpecified = false;
        
        IsOrientationQuaternion = true;
    end
    
    properties (Access = private)
        % Position interpolant parameters.
        HorizontalCumulativeDistance;
        HorizontalDistancePiecewisePolynomial;
        HorizontalSpeedPiecewisePolynomial;
        HorizontalAccelerationPiecewisePolynomial;
        HorizontalJerkPiecewisePolynomial;
        HorizontalCurvatureInitial;
        HorizontalCurvatureFinal;
        HorizontalInitialPosition;
        HorizontalPiecewiseLength;
        VerticalDistancePiecewisePolynomial;
        VerticalSpeedPiecewisePolynomial;
        VerticalAccelerationPiecewisePolynomial;
        VerticalJerkPiecewisePolynomial;
        Course;
        PathDuration;

        % Orientation interpolant parameters.
        SegmentTimes;
        RadianSlewAngles;
        AxesOfRotation;
        RadianAngularVelocities;
        
        % Internal states.
        CurrentTime;
        IsDoneStatus;
    end
    
    properties (Hidden, SetAccess = protected)
        CurrentPosition
        CurrentVelocity
        CurrentAcceleration
        CurrentOrientation
        CurrentAngularVelocity
        CurrentPoseValid = false
    end
    
    methods
        function val = get.Orientation(obj)
            if obj.IsOrientationQuaternion
                val = obj.Quaternions;
            else
                val = obj.RotMats;
            end
        end
        
        function obj = waypointTrajectory(varargin)
            setProperties(obj, varargin{:});
            obj.IsDoneStatus = false;
        end
        
        function wpTable = waypointInfo(obj)
            %WAYPOINTINFO Get waypoint information table
            %
            %   WPTABLE = WAYPOINTINFO(TRAJ) returns a table of waypoints,
            %   times of arrival, velocities, and orientation for the 
            %   System object, TRAJ.
            
            valsCell = {obj.TimeOfArrival, obj.Waypoints};
            varsCell = {'TimeOfArrival', 'Waypoints'};
            
            if obj.IsVelocitiesSpecified
                valsCell{end+1} = obj.Velocities;
                varsCell{end+1} = 'Velocities';
            end
            if obj.IsOrientationSpecified
                orient = obj.Orientation;
                if ~obj.IsOrientationQuaternion
                    for i = size(orient, 3):-1:1
                        orientCell{i,:} = orient(:,:,i);
                    end
                else
                    for i = size(orient, 1):-1:1
                        orientCell{i,:} = orient(i,:);
                    end
                end
                valsCell{end+1} = orientCell;
                varsCell{end+1} = 'Orientation';
            end
            wpTable = table(valsCell{:}, 'VariableNames', varsCell);
        end
        
        function set.SampleRate(obj, val)
            validateattributes(val, {'double'}, ...
                {'real','finite','positive','scalar'}, ...
                '', ...
                'SampleRate');
            obj.SampleRate = val;
        end
        
        function set.SamplesPerFrame(obj, val)
            validateattributes(val, {'double'}, ...
                {'real','finite','positive','scalar','integer'}, ...
                '', ...
                'SamplesPerFrame');
            obj.SamplesPerFrame = val;
        end
    end
    
    methods (Access = protected)
        function setProperties(obj, varargin)
            switch numel(varargin)
                case 0
                    setWaypoints(obj, [0 0 0; 0 0 0]);
                    setTimeOfArrival(obj, [0; 1]);
                case 1
                    coder.internal.error('shared_positioning:waypointTrajectory:InvalidConstruction');
                otherwise
                    param = varargin{1};
                    isValidParam = ( isa(param, 'char') && (size(param, 1) == 1) ) ...
                        || (isa(param, 'string') && isscalar(param) );
                    if ~isValidParam
                        obj.IsWaypointsSpecified = true;
                        setWaypoints(obj, param);
                        param = varargin{2};
                        isValidParam = ( isa(param, 'char') && (size(param, 1) == 1) ) ...
                            || (isa(param, 'string') && isscalar(param) );
                        if ~isValidParam
                            obj.IsTimeOfArrivalSpecified = true;
                            setTimeOfArrival(obj, param);
                            pvPairStartIdx = 3;
                        else
                            pvPairStartIdx = 2;
                        end
                    else
                        pvPairStartIdx = 1;
                    end
                    
                    numArgs = numel(varargin)-(pvPairStartIdx-1);
                    oddNumOfArgs = (rem(numArgs, 2) ~= 0);
                    coder.internal.errorIf(oddNumOfArgs, ...
                        'MATLAB:system:invalidPVPairs');
                    for i=pvPairStartIdx:2:numel(varargin)
                        param = varargin{i};
                        isValidParam = ( isa(param, 'char') && (size(param, 1) == 1) ) ...
                            || (isa(param, 'string') && isscalar(param) );
                        coder.internal.errorIf(~isValidParam, 'MATLAB:system:invalidPVPairs');
                        val = varargin{i+1};
                        switch param
                            case 'Waypoints'
                                setWaypoints(obj, val);
                                obj.IsWaypointsSpecified = true;
                            case 'TimeOfArrival'
                                setTimeOfArrival(obj, val);
                                obj.IsTimeOfArrivalSpecified = true;
                            case 'Velocities'
                                setVelocities(obj, val);
                                obj.IsVelocitiesSpecified = true;
                            case 'Orientation'
                                setOrientation(obj, val);
                                obj.IsOrientationSpecified = true;
                            otherwise
                                % Set public property.
                                matlabshared.fusionutils.internal.setProperties(obj, 2, param, val);
                        end
                    end
                    if ~obj.IsWaypointsSpecified && ~obj.IsTimeOfArrivalSpecified
                        setWaypoints(obj, [0 0 0; 0 0 0]);
                        setTimeOfArrival(obj, [0; 1]);
                    elseif ~(obj.IsWaypointsSpecified && obj.IsTimeOfArrivalSpecified)
                        coder.internal.error('shared_positioning:waypointTrajectory:InvalidConstruction');
                    end
                    validateWaypointSizes(obj);
            end
        end
        
        % Private Set Methods 
        function setWaypoints(obj, val)
            validateattributes(val, {'double'}, ...
                {'ncols', 3, '2d', 'real', 'finite'}, ...
                '', ...
                'Waypoints');
            coder.internal.assert(size(val, 1) > 1, ...
                'shared_positioning:waypointTrajectory:InsufficientWaypoints');
            obj.Waypoints = val;
        end
        
        function setTimeOfArrival(obj, val)
            validateattributes(val, ...
                {'double'}, ...
                {'finite', 'real', 'increasing', 'vector'}, ...
                '', ...
                'TimeOfArrival');
            coder.internal.errorIf(val(1) ~= 0, ...
                'shared_positioning:waypointTrajectory:TimeOfArrivalMustStartAtZero');
            % Ensure it is a column vector.
            obj.TimeOfArrival = val(:);
        end
        
        function setVelocities(obj, val)
            validateattributes(val, ...
                {'double'}, ...
                {'ncols', 3, '2d', 'real', 'finite'}, ...
                '', ...
                'Velocities');
            obj.Velocities = val;
            obj.CurrentPoseValid = false;
        end
        
        function setOrientation(obj, val)
            isQuat = isa(val, 'quaternion');
            if isQuat
                validateattributes(parts(val), ...
                    {'double'}, ...
                    {'ncols', 1, '2d'}, ...
                    '', ...
                    'Orientation');
                validateattributes(compact(val), ...
                    {'double'}, ...
                    {'real', 'finite'}, ...
                    '', ...
                    'Orientation');
                obj.Quaternions = val;
                obj.RotMats = rotmat(val, 'frame');
            else
                validateattributes(val, ...
                    {'double'}, ...
                    {'real','finite', 'size', [3 3 NaN]}, ...
                    '', ...
                    'Orientation');
                obj.Quaternions = quaternion(val, 'rotmat', 'frame');
                obj.RotMats = val;
            end
            obj.IsOrientationQuaternion = isQuat;
            obj.CurrentPoseValid = false;
        end
        function validateWaypointSizes(obj)
            if ~obj.IsWaypointsSpecified && ~obj.IsTimeOfArrivalSpecified
                coder.internal.errorIf(obj.IsVelocitiesSpecified, 'shared_positioning:waypointTrajectory:InvalidWaypointSpecification', '''Velocities''');
                coder.internal.errorIf(obj.IsOrientationSpecified, 'shared_positioning:waypointTrajectory:InvalidWaypointSpecification', '''Orientation''');
            else
                n = size(obj.Waypoints, 1);
                if obj.IsTimeOfArrivalSpecified
                    validateattributes(obj.TimeOfArrival, ...
                        {'double'}, ...
                        {'numel', n}, ...
                        '', ...
                        'TimeOfArrival');
                end
                
                if obj.IsVelocitiesSpecified
                    validateattributes(obj.Velocities, ...
                        {'double'}, ...
                        {'nrows', n}, ...
                        '', ...
                        'Velocities');
                end
                
                if obj.IsOrientationSpecified
                    if obj.IsOrientationQuaternion
                        validateattributes(parts(obj.Quaternions), ...
                            {'double'}, ...
                            {'nrows', n}, ...
                            '', ...
                            'Orientation');
                    else
                        validateattributes(obj.RotMats, ...
                            {'double'}, ...
                            {'size', [NaN NaN n]}, ...
                            '', ...
                            'Orientation');
                    end
                end
            end
        end
        
        function setupImpl(obj)
            obj.CurrentTime = obj.TimeOfArrival(1);
            setupInterpolants(obj);
            lookupPose(obj, obj.TimeOfArrival(1)); % set current pose
            obj.IsDoneStatus = false;
        end
        
        function resetImpl(obj)
            obj.CurrentTime = obj.TimeOfArrival(1);
            lookupPose(obj, obj.TimeOfArrival(1)); % set current pose
            obj.IsDoneStatus = false;
        end
        
        function [position, orientation, velocity, acceleration, angularVelocity] = stepImpl(obj)
            t = obj.CurrentTime;
            dt = 1/obj.SampleRate;
            spf = obj.SamplesPerFrame;
            
            position = zeros(spf, 3);
            q = quaternion.ones(spf, 1);
            velocity = zeros(spf, 3);
            angularVelocity = zeros(spf, 3);
            acceleration = zeros(spf, 3);
            if ~isDone(obj)
                for i = 1:obj.SamplesPerFrame
                    t = t + dt;
                    [position(i,:), q(i,:), velocity(i,:), angularVelocity(i,:), acceleration(i,:)] = lookupPose(obj, t);
                end
                if ~isDone(obj)
                    obj.CurrentTime = t;
                    if (t+dt) > obj.PathDuration
                        obj.IsDoneStatus = true;
                    end
                end
            end
            if obj.IsOrientationQuaternion
                orientation = q;
            else
                orientation = rotmat(q, 'frame');
            end
        end
        
        function status = isDoneImpl(obj)
            status = obj.IsDoneStatus;
        end
        
        function setupInterpolants(obj)
            setupPositionInterpolant(obj);
            setupOrientationInterpolant(obj);
            setupWaypointParams(obj);
            obj.CurrentPoseValid = true;
        end
        
        function setupPositionInterpolant(obj)
            t = obj.TimeOfArrival;
            waypoints = obj.Waypoints;
            n = numel(t);
            hl = zeros(n-1,1); %horiz curve lengths
            k0 = zeros(n-1,1); %init curvature
            k1 = zeros(n-1,1); %final curvature
            
            % Find the course angles at the start of each segment.
            if obj.IsVelocitiesSpecified
                course = angle(complex(obj.Velocities(:,1), obj.Velocities(:,2)));
            else
                course = matlabshared.tracking.internal.scenario.clothoidG2fitCourse(waypoints);
            end
            
            % Obtain the (horizontal) initial positions.
            hip = complex(waypoints(:,1), waypoints(:,2));
            for i=1:n-1
              [k0(i), k1(i), hl(i)] = matlabshared.tracking.internal.scenario.clothoidG1fit(...
                  hip(i),course(i),hip(i+1),course(i+1));
            end

            % Report cumulative horizontal distance traveled from initial point.
            hcd = [0; cumsum(hl)];
            
            % Get the piecewise polynomial to evaluate d(t).
            if obj.IsVelocitiesSpecified
                gndspeed = sqrt(sum(obj.Velocities(:,1:2).^2, 2));
                hpp = matlabshared.tracking.internal.scenario.chermite(t, hcd, gndspeed);
            else
                hpp = pchip(t, hcd);
            end
            
            % Cache the piecewise polynomial derivatives with respect
            % to length.
            hspp = matlabshared.tracking.internal.scenario.derivpp(hpp);
            happ = matlabshared.tracking.internal.scenario.derivpp(hspp);
            hjpp = matlabshared.tracking.internal.scenario.derivpp(happ);
            
            % Get the piecewise polynomial for elevation.
            if obj.IsVelocitiesSpecified
                vpp = matlabshared.tracking.internal.scenario.chermite(t, waypoints(:,3), obj.Velocities(:,3));
            else
                vpp = pchip(t, waypoints(:,3));
            end
            
            % Cache the piecewise polynomial derivatives with respect
            % to length.
            vspp = matlabshared.tracking.internal.scenario.derivpp(vpp);
            vapp = matlabshared.tracking.internal.scenario.derivpp(vspp);
            vjpp = matlabshared.tracking.internal.scenario.derivpp(vapp);
            
            % Save results.
            obj.HorizontalCumulativeDistance = hcd;
            obj.HorizontalDistancePiecewisePolynomial = hpp;
            obj.HorizontalSpeedPiecewisePolynomial = hspp;
            obj.HorizontalAccelerationPiecewisePolynomial = happ;
            obj.HorizontalJerkPiecewisePolynomial = hjpp;
            obj.HorizontalCurvatureInitial = k0;
            obj.HorizontalCurvatureFinal = k1;
            obj.HorizontalInitialPosition = hip;
            obj.HorizontalPiecewiseLength = hl;
            obj.VerticalDistancePiecewisePolynomial = vpp;
            obj.VerticalSpeedPiecewisePolynomial = vspp;
            obj.VerticalAccelerationPiecewisePolynomial = vapp;
            obj.VerticalJerkPiecewisePolynomial = vjpp;
            obj.Course = course;
            obj.PathDuration = t(end);
        end
        
        function setupOrientationInterpolant(obj)
            
            if obj.IsOrientationSpecified
                q = obj.Quaternions;
                t = obj.TimeOfArrival;
                wi = [0 0 0];
                wf = [0 0 0];
                maxit = 10;
                tol = 1e-9;
                [h,dtheta,e,w] = fusion.scenario.internal.quaternionC2fit(q,t,wi,wf,maxit,tol);
                
                % Save results.
                obj.SegmentTimes = h;
                obj.RadianSlewAngles = dtheta;
                obj.AxesOfRotation = e;
                obj.RadianAngularVelocities = w;
            end
        end
        
        function setupWaypointParams(obj)
            fillVelocities = ~obj.IsVelocitiesSpecified;
            fillOrientation = ~obj.IsOrientationSpecified;
            if (fillVelocities || fillOrientation)
                t = obj.TimeOfArrival;
                n = numel(t);
                vel = zeros(n, 3);
                q = quaternion.ones(n, 1);
                for i = 1:numel(t)
                    [~, q(i), vel(i,:), ~, ~] = lookupPose(obj, t(i));
                end
                if fillVelocities
                    obj.Velocities = vel;
                end
                if fillOrientation
                    obj.RotMats = rotmat(q, 'frame');
                    obj.Quaternions = q;
                end
            end
        end
        
        function [position, orientation, velocity, angularVelocity, acceleration] = lookupPose(obj, simulationTime)
            % interpolate motion from piecewise model
            if simulationTime <= obj.PathDuration
                % get positioning
                [position, velocity, acceleration] = fetchPosition(obj, simulationTime);
                
                % fetch orientation
                if ~obj.IsOrientationSpecified
                    [upVector, dupVector, forwardVector, dforwardVector] = fetchOrientationVectors(velocity, acceleration);
                    
                    % determine angular velocity
                    leftVector = cross(upVector, forwardVector);
                    dleftVector = cross(dupVector, forwardVector) + cross(upVector, dforwardVector);
                    R = [forwardVector; leftVector; upVector]';
                    dR = [dforwardVector; dleftVector; dupVector]';
                    W = dR * R';
                    angularVelocity = [W(3,2) W(1,3) W(2,1)];
                    angularVelocity(~isfinite(angularVelocity)) = 0;
                    if any(~isfinite(R(:)))
                        R = eye(3);
                    end
                    orientation = quaternion(R.', 'rotmat', 'frame');
                else
                    [orientation, angularVelocity] = fetchOrientationFromQuaternions(obj,simulationTime);
                end
            else
                position = zeros(1,3);
                orientation = quaternion.ones;
                velocity = zeros(1,3);
                angularVelocity = zeros(1,3);
                acceleration = zeros(1,3);
                obj.IsDoneStatus = true;
            end
            
            obj.CurrentPosition = position;
            obj.CurrentVelocity = velocity;
            obj.CurrentAcceleration = acceleration;
            obj.CurrentAngularVelocity = angularVelocity;
            obj.CurrentOrientation = orientation;
        end
        
        function [position, velocity, acceleration, jerk] = fetchPosition(obj, simulationTime)
            hcd = obj.HorizontalCumulativeDistance;
            hpp = obj.HorizontalDistancePiecewisePolynomial;
            hspp = obj.HorizontalSpeedPiecewisePolynomial;
            happ = obj.HorizontalAccelerationPiecewisePolynomial;
            hjpp = obj.HorizontalJerkPiecewisePolynomial;
            k0 = obj.HorizontalCurvatureInitial;
            k1 = obj.HorizontalCurvatureFinal;
            hip = obj.HorizontalInitialPosition;
            hl = obj.HorizontalPiecewiseLength;
            vpp = obj.VerticalDistancePiecewisePolynomial;
            vspp = obj.VerticalSpeedPiecewisePolynomial;
            vapp = obj.VerticalAccelerationPiecewisePolynomial;
            vjpp = obj.VerticalJerkPiecewisePolynomial;
            course = obj.Course;

            % interpolate horizontal position, velocity, acceleration,
            % and jerk based on the simulation time.
            [ph, vh, ah, jh] = fusion.scenario.internal.getPositionalState(hcd, hip, hl, ...
                k0, k1, course, hpp, hspp, happ, hjpp, simulationTime);

            % interpolate vertical z-position and derivatives based upon
            % simulation time
            pz = ppval(vpp, simulationTime);
            vz = ppval(vspp, simulationTime);
            az = ppval(vapp, simulationTime);
            jz = ppval(vjpp, simulationTime);

            % assemble the 3D positions
            position = zeros(1,3);
            position(:) = [real(ph) imag(ph) pz];
            velocity = [real(vh) imag(vh) vz];
            acceleration = [real(ah) imag(ah) az];
            jerk = [real(jh) imag(jh) jz];
        end
        
        function [q, radianAngularVelocity] = fetchOrientationFromQuaternions(obj,t)
            y = obj.Quaternions;
            h = obj.SegmentTimes;
            dtheta = obj.RadianSlewAngles;
            e = obj.AxesOfRotation;
            w = obj.RadianAngularVelocities;
            x = obj.TimeOfArrival;
            [q, angularVelocityBodyFrame] = fusion.scenario.internal.getOrientationState(t, x, y, h, dtheta, e, w);
            m = rotmat(q, 'frame');
            radianAngularVelocity = angularVelocityBodyFrame*m;
        end
        
        
        function s = saveObjectImpl(obj)
            % Save public properties.
            s = saveObjectImpl@matlab.System(obj);
            
             % Save private properties created during construction.
            s.Waypoints = obj.Waypoints;
            s.TimeOfArrival = obj.TimeOfArrival;
            s.Velocities = obj.Velocities;
            s.RotMats = obj.RotMats;
            s.Quaternions = obj.Quaternions;
            
            s.IsWaypointsSpecified = obj.IsWaypointsSpecified;
            s.IsTimeOfArrivalSpecified = obj.IsTimeOfArrivalSpecified;
            s.IsVelocitiesSpecified = obj.IsVelocitiesSpecified;
            s.IsOrientationSpecified = obj.IsOrientationSpecified;
            s.IsOrientationQuaternion = obj.IsOrientationQuaternion;
            
            s.CurrentPoseValidStatus = obj.CurrentPoseValid;
            
            % Save private properties. 
            if isLocked(obj)
                s.HorizontalCumulativeDistance = obj.HorizontalCumulativeDistance;
                s.HorizontalDistancePiecewisePolynomial = obj.HorizontalDistancePiecewisePolynomial;
                s.HorizontalSpeedPiecewisePolynomial = obj.HorizontalSpeedPiecewisePolynomial;
                s.HorizontalAccelerationPiecewisePolynomial = obj.HorizontalAccelerationPiecewisePolynomial;
                s.HorizontalJerkPiecewisePolynomial = obj.HorizontalJerkPiecewisePolynomial;
                s.HorizontalCurvatureInitial = obj.HorizontalCurvatureInitial;
                s.HorizontalCurvatureFinal = obj.HorizontalCurvatureFinal;
                s.HorizontalInitialPosition = obj.HorizontalInitialPosition;
                s.HorizontalPiecewiseLength = obj.HorizontalPiecewiseLength;
                s.VerticalDistancePiecewisePolynomial = obj.VerticalDistancePiecewisePolynomial;
                s.VerticalSpeedPiecewisePolynomial = obj.VerticalSpeedPiecewisePolynomial;
                s.VerticalAccelerationPiecewisePolynomial = obj.VerticalAccelerationPiecewisePolynomial;
                s.VerticalJerkPiecewisePolynomial = obj.VerticalJerkPiecewisePolynomial;
                s.Course = obj.Course;
                s.PathDuration = obj.PathDuration;
                
                s.SegmentTimes = obj.SegmentTimes;
                s.RadianSlewAngles = obj.RadianSlewAngles;
                s.AxesOfRotation = obj.AxesOfRotation;
                s.RadianAngularVelocities = obj.RadianAngularVelocities;
                
                s.CurrentTime = obj.CurrentTime;
                s.IsDoneStatus = obj.IsDoneStatus;
            end
        end
        
        function loadObjectImpl(obj, s, wasLocked)
            % Load public properties. 
            loadObjectImpl@matlab.System(obj, s, wasLocked);
            
            % Load private properties created during construction.
            obj.Waypoints = s.Waypoints;
            obj.TimeOfArrival = s.TimeOfArrival;
            obj.Velocities = s.Velocities;
            obj.RotMats = s.RotMats;
            obj.Quaternions = s.Quaternions;
            
            obj.IsWaypointsSpecified = s.IsWaypointsSpecified;
            obj.IsTimeOfArrivalSpecified = s.IsTimeOfArrivalSpecified;
            obj.IsVelocitiesSpecified = s.IsVelocitiesSpecified;
            obj.IsOrientationSpecified = s.IsOrientationSpecified;
            obj.IsOrientationQuaternion = s.IsOrientationQuaternion;
                
            s.CurrentPoseValidStatus = obj.CurrentPoseValid;
            
            % Load private properties.
            if wasLocked
                obj.HorizontalCumulativeDistance = s.HorizontalCumulativeDistance;
                obj.HorizontalDistancePiecewisePolynomial = s.HorizontalDistancePiecewisePolynomial;
                obj.HorizontalSpeedPiecewisePolynomial = s.HorizontalSpeedPiecewisePolynomial;
                obj.HorizontalAccelerationPiecewisePolynomial = s.HorizontalAccelerationPiecewisePolynomial;
                obj.HorizontalJerkPiecewisePolynomial = s.HorizontalJerkPiecewisePolynomial;
                obj.HorizontalCurvatureInitial = s.HorizontalCurvatureInitial;
                obj.HorizontalCurvatureFinal = s.HorizontalCurvatureFinal;
                obj.HorizontalInitialPosition = s.HorizontalInitialPosition;
                obj.HorizontalPiecewiseLength = s.HorizontalPiecewiseLength;
                obj.VerticalDistancePiecewisePolynomial = s.VerticalDistancePiecewisePolynomial;
                obj.VerticalSpeedPiecewisePolynomial = s.VerticalSpeedPiecewisePolynomial;
                obj.VerticalAccelerationPiecewisePolynomial = s.VerticalAccelerationPiecewisePolynomial;
                obj.VerticalJerkPiecewisePolynomial = s.VerticalJerkPiecewisePolynomial;
                obj.Course = s.Course;
                obj.PathDuration = s.PathDuration;
                
                obj.SegmentTimes = s.SegmentTimes;
                obj.RadianSlewAngles = s.RadianSlewAngles;
                obj.AxesOfRotation = s.AxesOfRotation;
                obj.RadianAngularVelocities = s.RadianAngularVelocities;
                
                obj.CurrentTime = s.CurrentTime;
                obj.IsDoneStatus = s.IsDoneStatus;
            end
        end
    end
    
    methods (Hidden)
        function restart(obj)
            reset(obj);
        end
        
        function initTrajectory(obj)
            release(obj);
            obj.SamplesPerFrame = 1;
            setup(obj);
        end
        
        function initUpdateRate(obj, newUpdateRate)
            if obj.SampleRate ~= newUpdateRate
                obj.SampleRate = newUpdateRate;
            end
            if obj.SamplesPerFrame ~= 1
                release(obj);
                obj.SamplesPerFrame = 1;
            end
        end
        
        function status = move(obj, ~)
            status = ~obj.IsDoneStatus;
            step(obj);
        end
    end
    
    methods(Static, Hidden)
        function flag = isAllowedInSystemBlock
            flag = false;
        end
    end
end

function [upVector, dupVector, forwardVector, dforwardVector] = fetchOrientationVectors(velocity, acceleration)
   v = velocity;
   a = acceleration;
   v(3) = 0;
   a(3) = 0;
   upVector = [0 0 1];
   dupVector = [0 0 0];

   forwardVector = v / norm(v);
   dforwardVector = a / norm(v) - v * dot(v,a) / norm(v).^3;
end
