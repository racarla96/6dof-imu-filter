classdef insfilterAsync < fusion.internal.AsyncMARGGPSFuserBase & ...
    fusion.internal.UnitDisplayer 
    %INSFILTERASYNC Pose from MARG and GPS using asynchronous sensors 
    %
    %   FILT = INSFILTERASYNC implements sensor fusion of MARG and GPS data
    %   to estimate pose in the navigation reference frame. MARG (magnetic,
    %   angular rate, gravity) data is typically derived from magnetometer,
    %   gyroscope and accelerometer data, respectively. The filter uses a
    %   28-element state vector to track the orientation quaternion,
    %   velocity, position, MARG sensor biases, and geomagnetic vector. The
    %   INSFILTERASYNC class uses a continuous-discrete extended Kalman
    %   filter to estimate these quantities.
    % 
    %   FILT = INSFILTERASYNC('ReferenceFrame', RF) returns an inertial
    %   navigation filter that estimates pose relative to the reference
    %   frame RF. Specify the reference frame as 'NED' (North-East-Down) or
    %   'ENU' (East-North-Up). The default value is 'NED'.
    %
    %   INSFILTERASYNC Methods: 
    %
    %       predict   - Propagate states forward in time
    %       fuseaccel - Correct states using accelerometer data
    %       fusegyro  - Correct states using gyroscope data
    %       fusemag   - Correct states using magnetometer data
    %       fusegps   - Correct states using GPS data
    %       correct   - Correct states with direct state measurements
    %       pose      - Current position, orientation, and velocity 
    %                 - estimate
    %       reset     - Reinitialize internal states
    %       stateinfo - Definition of each element of State property vector
    %   
    %   INSFILTERASYNC Properties:
    %
    %       ReferenceLocation      - Reference location (deg, deg, meters)
    %       QuaternionNoise        - Orientation quaternion process noise 
    %                                variance
    %       AngularVelocityNoise   - Angular velocity process noise
    %                                 variance (rad/s)^2
    %       PositionNoise          - Position process noise variance m^2
    %       VelocityNoise          - Velocity process noise variance 
    %                                (m/s)^2
    %       AccelerationNoise      - Acceleration process noise variance
    %                                (m/s^2)^2
    %       GyroscopeBiasNoise     - Gyroscope bias process noise variance
    %                                (rad/s)^2
    %       AccelerometerBiasNoise - Accelerometer bias process noise 
    %                                variance (m/s^2)^2
    %       GeomagneticVectorNoise - Geomagnetic vector process noise 
    %                                variance (uT^2)
    %       MagnetometerBiasNoise  - Magnetometer bias process noise 
    %                                variance (uT^2)
    %       State                  - State vector of extended Kalman filter
    %       StateCovariance        - State error covariance for 
    %                                extended Kalman filter
    %   
    %   Example : Estimate the pose of a UAV
    %   
    %   % Load logged sensor data and ground truth pose
    %   load('uavshort.mat', 'refloc', 'initstate', 'imuFs', ...
    %       'accel', 'gyro', 'mag', 'lla', 'gpsvel', ...
    %       'trueOrient', 'truePos')
    %
    %   % Set up the fusion filter
    %   is = [initstate(1:4);0;0;0;initstate(5:10);0;0;0; ...
    %       initstate(11:end)];
    %   f = insfilterAsync('ReferenceLocation', refloc, 'State', is);
    %   
    %   gpsidx = 1;
    %   N = size(accel,1);
    %   p = zeros(N,3);
    %   q = zeros(N,1, 'quaternion');
    %  
    %   % Sensor measurement noises from datasheets and experimentation
    %   Rmag = 80;
    %   Rvel = 0.0464;
    %   Racc = 800;
    %   Rgyro = 1e-4;
    %   Rpos = 34;
    %   
    %   % Fuse accelerometer, gyroscope, magnetometer, and GPS
    %   for ii=1:size(accel,1)
    %       % Fuse IMU
    %       f.predict(1./imuFs);
    %   
    %       f.fuseaccel(accel(ii,:), Racc);
    %       f.fusegyro(gyro(ii,:), Rgyro);
    %   
    %       % Fuse magnetometer at 1/2 the IMU rate
    %       if ~mod(ii, fix(imuFs/2))
    %           f.fusemag(mag(ii,:), Rmag);
    %       end
    %   
    %       % Fuse GPS once per second
    %       if ~mod(ii, imuFs)
    %           f.fusegps(lla(gpsidx,:), Rpos, gpsvel(gpsidx,:), Rvel);
    %           gpsidx = gpsidx  + 1;
    %       end
    %   
    %       [p(ii,:),q(ii)] = pose(f);
    %   end
    %   
    %   % RMS errors
    %   posErr = truePos - p;
    %   qErr = rad2deg(dist(trueOrient,q));
    %   pRMS = sqrt(mean(posErr.^2));
    %   qRMS = sqrt(mean(qErr.^2));
    %   fprintf('Position RMS Error\n');
    %   fprintf('\tX: %.2f , Y: %.2f, Z: %.2f (meters)\n\n', pRMS(1), ...
    %       pRMS(2), pRMS(3));
    %   
    %   fprintf('Quaternion Distance RMS Error\n');
    %   fprintf('\t%.2f (degrees)\n\n', qRMS);
    %
    %
    %   See also INSFILTERMARG, INSFILTERERRORSTATE, INSFILTERNONHOLONOMIC
    
    %   Copyright 2018-2019 The MathWorks, Inc.

    %#codegen

    properties (Constant, Hidden)
        ReferenceLocationUnits      = '[deg deg m]';
        AngularVelocityNoiseUnits        = ['(rad/s)' char(178)];
        PositionNoiseUnits     = ['m' char(178) ];
        VelocityNoiseUnits     = ['(m/s)' char(178)];
        AccelerationNoiseUnits     = ['(m/s' char(178) ')' char(178)];
        GyroscopeBiasNoiseUnits    = ['(rad/s)' char(178)];
        AccelerometerBiasNoiseUnits = ['(m/s' char(178) ')' char(178)];
        GeomagneticVectorNoiseUnits = ['uT' char(178)];
        MagnetometerBiasNoiseUnits  = ['uT' char(178)];
    end    

    methods
        function obj = insfilterAsync(varargin)
            obj@fusion.internal.AsyncMARGGPSFuserBase(varargin{:});
        end
    end

    methods (Access = protected)

        function displayScalarObject(obj)
            displayScalarObjectWithUnits(obj);
        end
        function groups = getPropertyGroups(~)
            % Add section titles to property display
            basic = {'ReferenceLocation'};
            procNoise = {'QuaternionNoise', 'AngularVelocityNoise', ...
                'PositionNoise', 'VelocityNoise', 'AccelerationNoise', ...
                'GyroscopeBiasNoise', 'AccelerometerBiasNoise', ...
                'GeomagneticVectorNoise', 'MagnetometerBiasNoise'};

            initVals = {'State', 'StateCovariance'};
            
            g1 = matlab.mixin.util.PropertyGroup(cat(2, basic, initVals));
            g2 = matlab.mixin.util.PropertyGroup(procNoise, ...
                'Additive Process Noise Variances');
            groups = [g1,g2];        
        end
    end
    
    methods (Access=public, Hidden, Static)
        function name = matlabCodegenRedirect(~)
            name = 'fusion.internal.coder.insfilterAsyncCG';
        end
    end
end % classdef

