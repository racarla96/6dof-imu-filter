classdef (Hidden) IMUBasicESKF < fusion.internal.PositioningHandleBase 
%IMUBASICESKF Abstract class for ESKFs fusing IMU data
%
%   This class is for internal use only. It may be removed in the future.

%   Copyright 2018-2019 The MathWorks, Inc.

%#codegen 
    properties (Abstract)
        State;
        StateCovariance;
    end
    
    properties (Abstract, Hidden, Constant)
        NumStates;
        NumErrorStates;
    end
    
    properties (Hidden)
        ReferenceFrame = fusion.internal.frames.ReferenceFrame.getDefault;
    end
    
    properties
        % IMUSampleRate IMU sampling rate (Hz)
        % Specify the sampling frequency of the IMU as a positive scalar.
        % The default value is 100.
        IMUSampleRate = 100;
    end
    
    methods (Abstract, Access = protected)
        injectError(obj, errorState)
        resetError(obj)
    end
    
    methods (Sealed)
        
        function correct(obj, idx, z, Rin)
        %CORRECT Correct states using direct state measurements
        %   CORRECT(FUSE, IDX, Z, R) corrects the state and state estimation error 
        %   covariance based on the measurement Z and covariance R. The measurement
        %   Z maps directly to the states specified by the indices IDX.
            
            validateattributes(idx, ...
                {'numeric'}, ...
                {'vector','positive','integer','<=',obj.NumStates,'increasing'}, ...
                '', ...
                'idx');
            N = uint32(numel(idx));
            validateattributes(z, ...
                {'double','single'}, ...
                {'vector','real','finite','numel',N}, ...
                '', ...
                'z');
            % Ensure it is a column vector.
            z = z(:);
            idx = idx(:);
            
            x = obj.State;
            if all(idx(:) > 4)
                h = x(idx);
                zminush = z - h;
                errIdx = idx - 1;
            else
                coder.internal.assert(N >= 4 && all(idx(1:4) == (1:4).'), ...
                    'shared_positioning:insfilter:InvalidStateCorrection');
                % Convert subtract quaternion measurement using
                % multiplication and store the delta angle.
                zminush = zeros(N-1,1, 'like', z);
                zQ = quaternion(z(1:4).');
                hQ = quaternion(x(1:4).');
                deltaAng = rotvec( zQ * conj(hQ) ).';
                zminush(1:3) = deltaAng;
                
                % Adjust error state index.
                errIdx = zeros(N-1,1, 'like', z);
                errIdx(1:3) = (1:3).';
                
                % Subtract the remaining measurements, if any.
                if (N > 4)
                    zminush(4:end) = z(5:end) - x(idx(5:end));
                    errIdx(4:end) = idx(5:end) - 1;
                end
                N = N-1;
            end

            
            % Expand Rin and make sure it matches the number of element in 
            % the measurement.
            R = obj.validateExpandNoise(Rin, N, 'R');
            
            I = eye(obj.NumErrorStates);
            H = I(errIdx,:);
            correctEqn(obj, zminush, H, R)
        end
    end
    
    methods
        function set.IMUSampleRate(obj, val)
            validateattributes(val, {'double','single'}, ...
                {'real','scalar','positive','finite'}, ...
                '', ...
                'IMUSampleRate');
            obj.IMUSampleRate = val;
        end
        
        function set.ReferenceFrame(obj, val)
           obj.ReferenceFrame = validatestring(val, ...
               fusion.internal.frames.ReferenceFrame.getOptions, ...
               '', 'ReferenceFrame');
       end
    end
    
    methods (Access = protected)
        function correctEqn(obj, zminush, H, R)
            P = obj.StateCovariance;
            
            S = H*P*(H.') + R;
            W = P*(H.') / S;
            
            dx = W*zminush;
            obj.StateCovariance = P - W*H*P;
            
            injectError(obj, dx);
            resetError(obj);
        end

        function x = repairQuaternion(obj, x)
            % Normalize quaternion and enforce a positive angle of
            % rotation. Avoid constructing a quaternion here. Just do the
            % math inline.

            qparts = x(1:4);
            n = sqrt(sum(qparts.^2));
            qparts = qparts./n;
            if qparts(1) < 0
                x(1:4) = -qparts;
            else
                x(1:4) = qparts;
            end
        end

        function P = predictCovEqn(~, P, F, U, G)
            Q = G*U*(G.');
            P = F*P*(F.') + Q;
        end
        
        function Rout = validateExpandNoise(obj, Rin, num, argName)%#ok<INUSL>
            %VALIDATEEXPANDNOISE validate measurement noise input and expand
            %to matrix

            validateattributes(Rin, {'double', 'single'}, ...
                {'2d', 'nonempty', 'real'}, '', argName);

            sz = size(Rin);
            coder.internal.assert(isscalar(Rin) || ...
                isequal(sz, [num 1]) || isequal(sz, [1 num]) || ...
                isequal(size(Rin), [num num]), ...
                'shared_positioning:insfilter:MeasurementNoiseSize',  ...
                argName, sprintf('%u',num) );
            
            switch numel(Rin)
                case 1
                    Rout = diag( repmat(Rin,1,num) );
                case num
                    Rout = diag(Rin);
                otherwise % matrix 
                    Rout = Rin;
            end
        end
    end
    methods (Hidden, Static)
        function props = matlabCodegenNontunableProperties(~)
            props = {'ReferenceFrame'};
        end
    end
end
