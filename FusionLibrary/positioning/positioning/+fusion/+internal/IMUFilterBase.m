classdef (Hidden) IMUFilterBase < fusion.internal.IMUFusionCommon
%IMUFILTERBASE Base class for imufilter
%
%   This class is for internal use only. It may be removed in the future.

%   Copyright 2017-2019 The MathWorks, Inc.
    
%#codegen

   
        
    properties (Nontunable)
        %InitialProcessNoise Covariance matrix for process noise
        %   The initial process covariance matrix accounts for the error in
        %   the process model.  Specify the initial process covariance
        %   matrix as a 9-by-9 real, finite matrix.
        InitialProcessNoise = fusion.internal.IMUFilterBase.getInitialProcCov()      

    end

    methods

        function set.InitialProcessNoise(obj,val)
            validateattributes(val, {'numeric'}, ...
                {'real', 'size', [9 9], 'finite', ...
                'nonsparse'}, ...
                'set.InitialProcessNoise', 'InitialProcessNoise' );
            obj.InitialProcessNoise = val;
        end

    end

    methods(Access = protected)
        function resetImpl(obj)

            ex = obj.pInputPrototype;
            obj.pOrientPost = quaternion.ones(1,1, 'like', ex); 

            % Zero out initial errors and Gyro Offset
            obj.pGyroOffset = zeros(1,3,'like', ex);
      
            % Initialize noise variances
            updateMeasurementErrCov(obj);

            obj.pQw = cast(obj.InitialProcessNoise, 'like', ex);

            obj.pLinAccelPost = zeros(1,3,'like', ex);
            
            obj.pFirstTime = true;
        end

        function validateInputsImpl(obj, accelIn, gyroIn)
            validateattributes(accelIn, {'double', 'single'}, ...
                {'real', 'nonempty', '2d', 'ncols', 3}, ...
                '', 'acceleration',1);
            r = size(accelIn,1);
            dt = class(accelIn);
            validateattributes(gyroIn, {dt}, ...
                {'real', 'nonempty', '2d', 'ncols', 3, 'nrows', r}, ...
                '', 'angularVelocity',2);

            % From validation above we know both have the same number of
            % rows. Validate compatibility with frame size.
            validateFrameSize(obj, accelIn);
        end

        function num = getNumInputsImpl(~)
          num = 2;
        end
        
        function [orientOut, av] = stepImpl(obj, accelIn, gyroIn)

            % Fuse the sensor readings from the accelerometer
            % and gyroscope. 
            %   accelIn - Nx3 matrix of accel samples in m/s^2
            %   gyroIn - Nx3 matrix of gyro samples in rad/s
            
            if isa(accelIn, 'single') || isa(gyroIn, 'single') 
                cls = 'single';
            else
                cls = 'double';
            end

            
            % Want to have DecimationFactor-by-3-by-??? matrices so we can
            % page through the 3rd dimension on each trip through the
            % for...loop below.
            accelIn = reshape(accelIn', 3, obj.DecimationFactor, []);
            gyroIn = reshape(gyroIn', 3, obj.DecimationFactor, []);

            accelIn = permute(accelIn, [2,1,3]);
            gyroIn = permute(gyroIn, [2,1,3]);
            
            ref = obj.pRefSys;

            numiters = size(accelIn,3);

            % Allocate output
            [av, orientOut] = allocateOutputs(obj, numiters, cls);

            % Loop through each frame. 
            for iter=1:numiters
                % Indirect Kalman filter. Track the *errors* in the
                % estimates of the 
                %   the orientation (as a 3-element rotation vector)
                %   gyroscope bias
                %   linear acceleration
                %
                % The Kalman filter tracks these errors. The actual
                % orientation, gyroscope bias and linear acceleration are
                % updated with the Kalman filter states after
                % predict & correct phases.
                %

                afast = accelIn(:,:,iter);
                gfast = gyroIn(:,:,iter);


                angularVelocity = computeAngularVelocity(obj, gfast, ...
                    obj.pGyroOffset);
            
                % We only need fast gyro readings. Accel can be
                % downsampled to the most recent reading.
                accel = afast(end,:);


                
                if obj.pFirstTime
                    % Basic tilt corrected ecompass orientation algorithm.
                    % Do this the first time only. Need inputs, so not in setupImpl.
                    
                    % No magnetometer - assume device is pointing north
                    m = zeros(1,3, cls);
                    m(ref.NorthIndex) = 1; 
                    Rpost = ref.ecompass(accel, m);
                    obj.pFirstTime = false;
                    obj.pOrientPost = quaternion(Rpost, 'rotmat', 'frame');
               end

                % Update the orientation quaternion based on the gyroscope
                % readings. 
                obj.pOrientPrior = predictOrientation(obj, gfast, ...
                    obj.pGyroOffset, obj.pOrientPost);
                
                % Back to Rotation matrix
                Rprior = rotmat(obj.pOrientPrior, 'frame'); 

                %%%%%%%%%%%%%%%%
                % The Kalman filter measurement:
                %   Accel estimate of gravity - Gyro estimate of gravity
                
                % Gyro : Rprior is from the gyro measurements (above).
                % Gravity vector is one column of that matrix.
                gravityGyroMeas = ref.GravityAxisSign * Rprior(:,ref.GravityIndex).'; 

                % Accel: Decay the estimate of the linear acceleration and
                % subtract it from the accelerometer reading. 
                obj.pLinAccelPrior = obj.LinearAccelerationDecayFactor * obj.pLinAccelPost; 
                gravityAccelMeas = ref.GravitySign*accel + obj.pLinAccelPrior; 

                gravityAccelGyroDiff = gravityAccelMeas - gravityGyroMeas;
                
                %%%%%%%%%%%%%%%%
                % Compute the Measurement Matrix H and Kalman Gain K
                % Measurement matrix H 3-by-9
                h1 = obj.buildHPart(gravityGyroMeas);
                
                h3 = -h1.*obj.pKalmanPeriod; 
                H = [h1, h3, eye(3)];

                % Calculate the Kalman Gain K
                Qv = obj.pQv;
                Qw = obj.pQw;
                
                tmp = ((H * Qw * (H.') + Qv).');
                K = Qw * (H.')  / ( tmp);
             
                % Update a posteriori error using the Kalman gain
                ze = gravityAccelGyroDiff.';
                
                xe_post = K * ze;
                % Corrected error estimates
                orientErr = xe_post(1:3).';
                gyroOffsetErr = xe_post(4:6).';
                linAccelErr = xe_post(7:9).';

                % Estimate estimates based on the Kalman filtered error
                % estimates.
                %
                % Convert orientation error into a quaternion.
                % Update a posteriori orientation
                qerr = conj(quaternion(orientErr, 'rotvec')); 
                obj.pOrientPost = obj.pOrientPrior * qerr;
                
                % Force rotation angle to be positive
                if parts(obj.pOrientPost) < 0
                    obj.pOrientPost = -obj.pOrientPost;
                end
                
                obj.pOrientPost = normalize(obj.pOrientPost);

                % Subtract estimated errors for gyro bias and linear
                % acceleration 
                obj.pGyroOffset = obj.pGyroOffset - gyroOffsetErr;
                obj.pLinAccelPost = obj.pLinAccelPrior - linAccelErr; 
               
                % Calculate a posterior error covariance matrix
                Ppost = Qw - K * (H * Qw);
               
                % Update Qw - the process noise. Qw is a function of Ppost
                Qw = zeros(9, cls);

                Qw(getDiags(1:3)) = Ppost(getDiags(1:3)) + ((obj.pKalmanPeriod).^2)* ...
                    (Ppost(getDiags(4:6)) + (obj.GyroscopeDriftNoise + ...
                    obj.GyroscopeNoise));

                Qw(getDiags(4:6)) = Ppost(getDiags(4:6)) + ...
                    obj.GyroscopeDriftNoise;

                offDiag = -obj.pKalmanPeriod * Qw(getDiags(4:6));

                Qw([4 14 24]) = offDiag;
                Qw([28 38 48]) = offDiag;

                Qw(getDiags(7:9)) = (obj.LinearAccelerationDecayFactor.^2)* ...
                    Ppost(getDiags(7:9)) + obj.LinearAccelerationNoise;

                obj.pQw = Qw;
               
                % Populate output arguments
                av(iter,:) = angularVelocity;
                if strcmpi(obj.OrientationFormat, 'quaternion') 
                    orientOut(iter,:) = obj.pOrientPost;
                else
                    orientOut(:,:,iter) = rotmat(obj.pOrientPost, 'frame');
                end
            end

        end

        function processTunedPropertiesImpl(obj)
            updateMeasurementErrCov(obj);
        end

     
    end

    methods (Access = private)
        function updateMeasurementErrCov(obj)
            accelMeasNoiseVar = obj.AccelerometerNoise + ...
                obj.LinearAccelerationNoise + ((obj.pKalmanPeriod).^2) *...
                    (obj.GyroscopeDriftNoise + obj.GyroscopeNoise);

            obj.pQv = accelMeasNoiseVar.* eye(3); 
        end
    end

    methods (Static, Hidden)
        function covinit = getInitialProcCov()
        %GETINITIALPROCCOV - default initial process covariance
            covinit = zeros(9,9);
            covinit(1:3,1:3) = fusion.internal.IMUFilterBase.cOrientErrVar*eye(3);
            covinit(4:6,4:6) = fusion.internal.IMUFilterBase.cGyroBiasErrVar*eye(3);
            covinit(1:3,4:6) = fusion.internal.IMUFilterBase.cOrientGyroBiasErrVar*eye(3);
            covinit(4:6,1:3) = covinit(1:3,4:6);
            covinit(7:9,7:9) = fusion.internal.IMUFilterBase.cAccErrVar* eye(3);
            covinit = covinit .* fusion.internal.IMUFilterBase.getInitialProcCovMask; 
        end
        function msk = getInitialProcCovMask()
        %GETINITIALPROCCOVMASK - show possible nonzero entries in mask
            msk = false(9,9);
            msk(1:3,1:3) = true*eye(3);
            msk(4:6,4:6) = true*eye(3);
            msk(1:3,4:6) = true*eye(3);
            msk(4:6,1:3) = msk(1:3,4:6);
            msk(7:9,7:9) = true* eye(3);
        end
    end
end

function d = getDiags(r)
%Get diagonal elements in a 9 x 9 matrix where r is the column (or row) index
sz = 9;
rm1 = r - 1;
d = rm1*(sz+1) + 1;
end

