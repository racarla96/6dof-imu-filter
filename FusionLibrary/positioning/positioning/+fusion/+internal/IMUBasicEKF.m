classdef (Hidden) IMUBasicEKF < fusion.internal.PositioningHandleBase
%IMUBASICEKF Abstract class for EKFs fusing IMU data
%
%   This class is for internal use only. It may be removed in the future.

%   Copyright 2018-2019 The MathWorks, Inc.

%#codegen 
    properties (Abstract)
        State;
        StateCovariance;
    end
    
    properties (Abstract, Hidden, Constant)
        NumStates;
    end
    
    properties (Hidden)
        ReferenceFrame = fusion.internal.frames.ReferenceFrame.getDefault;
    end
    
    properties
        % IMUSampleRate IMU sampling rate (Hz)
        % Specify the sampling frequency of the IMU as a positive scalar.
        % The default value is 100.
        IMUSampleRate = 100;
    end
    
    methods (Sealed)
        
        function correct(obj, idx, z, Rin)
        %CORRECT Correct states using direct state measurements
        %   CORRECT(FUSE, IDX, Z, R) corrects the state and state estimation error 
        %   covariance based on the measurement Z and covariance R. The measurement
        %   Z maps directly to the states specified by the indices IDX.
            
            validateattributes(idx, ...
                {'numeric'}, ...
                {'vector','positive','integer','<=',obj.NumStates,'increasing'}, ...
                '', ...
                'idx');
            N = numel(idx);
            validateattributes(z, ...
                {'double','single'}, ...
                {'vector','real','finite','numel',N}, ...
                '', ...
                'z');
            % Ensure it is a column vector.
            z = z(:);
            
            % Expand Rin and make sure it matches idx.
            R = obj.validateExpandNoise(Rin, N, 'R');
            
            x = obj.State;
            P = obj.StateCovariance;
            
            h = x(idx);
            I = eye(obj.NumStates);
            H = I(idx,:);
            [obj.State, obj.StateCovariance] = correctEqn(obj, ...
                x, P, h, H, z, R);
        end
    end
    
    methods
        function set.IMUSampleRate(obj, val)
            validateattributes(val, {'double','single'}, ...
                {'real','scalar','positive','finite'}, ...
                '', ...
                'IMUSampleRate');
            obj.IMUSampleRate = val;
        end
        
        function set.ReferenceFrame(obj, val)
           obj.ReferenceFrame = validatestring(val, ...
               fusion.internal.frames.ReferenceFrame.getOptions, ...
               '', 'ReferenceFrame');
       end
    end
    
    methods (Access = protected)
        function [x, P] = correctEqn(obj, x, P, h, H, z, R)
            S = H*P*(H.') + R;
            W = P*(H.') / S;
            
            x = x + W*(z-h);
            P = P - W*H*P;

            % Assume first 4 elements are a quaternion. Normalize and
            % enforce a positive angle constraint. Subclasses that don't
            % use a quaternion in elements x(1:4) should override the
            % 'repairQuaternion' method.
            x = repairQuaternion(obj, x);
        end

        function x = repairQuaternion(obj, x)
            % Normalize quaternion and enforce a positive angle of
            % rotation. Avoid constructing a quaternion here. Just do the
            % math inline.

            qparts = x(1:4);
            n = sqrt(sum(qparts.^2));
            qparts = qparts./n;
            if qparts(1) < 0
                x(1:4) = -qparts;
            else
                x(1:4) = qparts;
            end
        end

        function P = predictCovEqn(~, P, F, U, G)
            Q = G*U*(G.');
            P = F*P*(F.') + Q;
        end
        
        function Rout = validateExpandNoise(obj, Rin, num, argName)%#ok<INUSL>
            %VALIDATEEXPANDNOISE validate measurement noise input and expand
            %to matrix

            validateattributes(Rin, {'double', 'single'}, ...
                {'2d', 'nonempty', 'real'}, '', argName);

            sz = size(Rin);
            coder.internal.assert(isscalar(Rin) || ...
                isequal(sz, [num 1]) || isequal(sz, [1 num]) || ...
                isequal(size(Rin), [num num]), ...
                'shared_positioning:insfilter:MeasurementNoiseSize',  ...
                argName, sprintf('%u',num) );
            
            switch numel(Rin)
                case 1
                    Rout = diag( repmat(Rin,1,num) );
                case num
                    Rout = diag(Rin);
                otherwise % matrix 
                    Rout = Rin;
            end
        end
    end
    methods (Hidden, Static)
        function props = matlabCodegenNontunableProperties(~)
            props = {'ReferenceFrame'};
        end
    end
end
