classdef (Hidden) UnitDisplayerValue < fusion.internal.UnitDisplayer
%UNITDISPLAYERVALUE - Object display with units for value classes
%
%   This class is for internal use only. It may be removed in the future.

%   Copyright 2017-2019 The MathWorks, Inc.


    methods (Access = protected)
        function displayScalarObject(obj)
            displayScalarObjectWithUnits(obj)
        end
    end
end
