classdef (Hidden) insfilterMARGCG < fusion.internal.MARGGPSFuserBase
%   This class is for internal use only. It may be removed in the future.

%   Copyright 2018-2019 The MathWorks, Inc.

%#codegen

    methods
        function obj = insfilterMARGCG(varargin)
            obj@fusion.internal.MARGGPSFuserBase(varargin{:});
        end
    end
    methods (Static, Hidden)
        function name = matlabCodegenUserReadableName
            name = 'insfilterMARG';
        end
    end
end
