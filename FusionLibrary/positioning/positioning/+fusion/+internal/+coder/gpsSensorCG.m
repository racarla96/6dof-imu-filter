classdef (Hidden) gpsSensorCG < fusion.internal.GPSSensorBase
%GPSSENSORCG - Codegen class for gpsSensor
%
%   This class is for internal use only. It may be removed in the future.

%   Copyright 2018-2019 The MathWorks, Inc.

%#codegen

    methods
        function obj = gpsSensorCG(varargin)
            obj@fusion.internal.GPSSensorBase(varargin{:});
        end
    end

    methods (Static, Hidden)
        function name = matlabCodegenUserReadableName
            name = 'gpsSensor';
        end
    end
end
