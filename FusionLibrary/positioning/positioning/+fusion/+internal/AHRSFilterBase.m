classdef (Hidden) AHRSFilterBase < fusion.internal.IMUFusionCommon 
%AHRSFILTERBASE Base class for ahrsfilter
%
%   This class is for internal use only. It may be removed in the future.


%   Copyright 2017-2019 The MathWorks, Inc.

%#codegen

    properties 
        %MagnetometerNoise Noise variance in the magnetometer data 
        %   Specify the noise in the magnetometer data in units of uT^2.
        %   Magnetometer noise variance must be a positive scalar value.
        %   The default value for this property is 0.1 uT^2. This property is
        %   tunable. 
        MagnetometerNoise= 0.1;
        
        %MagneticDisturbanceNoise Variance for magnetic disturbance 
        %   Specify the noise in the magnetic disturbance model in units of
        %   uT^2. Magnetic disturbance is modeled as a first order Markov
        %   process. Magnetic disturbance noise variance must be a positive
        %   scalar value. The default value for this property is 0.5 uT^2.
        %   This property is tunable. 
        MagneticDisturbanceNoise = 0.5;

        %MagneticDisturbanceDecayFactor Decay factor for magnetic disturbance 
        %   Specify the decay factor in the magnetic disturbance model.
        %   Magnetic disturbance is modeled as a first order Markov process.
        %   Magnetic disturbance decay factor must be a positive scalar
        %   value between 0 and 1. This property is tunable. 
        MagneticDisturbanceDecayFactor = 0.5;

        %ExpectedMagneticFieldStrength Expected estimate of magnetic field strength
        %   Specify the expected magnetic field strength as a real positive
        %   scalar value.  The expected magnetic field strength is an
        %   estimate of the Earth's magnetic field strength at the current
        %   location in units of uT.  The default value for this property
        %   is 50 uT. This property is tunable.
        ExpectedMagneticFieldStrength = 50;
    end

    properties (Nontunable)
        %InitialProcessNoise Covariance matrix for process noise
        %   The initial process covariance matrix accounts for the error in
        %   the process model.  Specify the initial process covariance
        %   matrix as a 9-by-9 real, finite matrix.
        InitialProcessNoise = fusion.internal.AHRSFilterBase.getInitialProcCov()      
    end

    properties (Constant, Hidden) %Noise Variance for Covariance Matrix
        cMagErrVar       = 600e-3; %var in mag disturbance error estim
    end

    properties (Constant, Access = protected)
        cJammingFactor = 4; % Mag field strength >4x expected --> Jamming.
        cInclinationLimit = deg2rad(90);
    end

    properties (Access = private)
        pMagVec                  % Earth's geomagnetic vector (uT)
                                 % in global frame, East component = 0
                                 
    end
    
    methods
        function set.MagnetometerNoise(obj,val)
            validateattributes(val, {'numeric'}, ...
                {'real', 'nonempty', 'scalar', 'finite', 'positive', ...
                'nonsparse'}, ...
                'set.MagnetometerNoise', 'MagnetometerNoise' );
            obj.MagnetometerNoise = val;
        end

        function set.MagneticDisturbanceNoise(obj,val)
            validateattributes(val, {'numeric'}, ...
                {'real', 'nonempty', 'scalar', 'finite', 'positive', ...
                'nonsparse'}, ...
                'set.MagneticDisturbanceNoise', 'MagneticDisturbanceNoise' );
            obj.MagneticDisturbanceNoise = val;
        end

        function set.MagneticDisturbanceDecayFactor(obj,val)
            validateattributes(val, {'numeric'}, ...
                {'real', 'nonempty', 'scalar', 'finite', ...
                '<' 1, '>=', 0, ...
                'nonsparse'}, ...
                'set.MagneticDisturbanceDecayFactor', 'MagneticDisturbanceDecayFactor' );
            obj.MagneticDisturbanceDecayFactor = val;
        end

        function set.InitialProcessNoise(obj,val)
            validateattributes(val, {'numeric'}, ...
                {'real','size', [12 12],'finite', ...
                'nonsparse'}, ...
                'set.InitialProcessNoise', 'InitialProcessNoise' );
            obj.InitialProcessNoise = val;
        end

        function set.ExpectedMagneticFieldStrength(obj,val)
            validateattributes(val, {'numeric'}, ...
                {'real', 'nonempty', 'scalar', 'finite', 'positive', ...
                'nonsparse'}, ...
                'set.ExpectedMagneticFieldStrength', 'ExpectedMagneticFieldStrength' );
            obj.ExpectedMagneticFieldStrength = val;
        end

    end

    methods(Access = protected)
        function resetImpl(obj)

            ex = obj.pInputPrototype;
            obj.pOrientPost = quaternion.ones(1,1, 'like', ex); 

            % Zero out initial errors and Gyro Offset
            obj.pGyroOffset = zeros(1,3,'like', ex);
            ref = obj.pRefSys;

            obj.pMagVec = zeros(1,3,'like', ex);
            obj.pMagVec(obj.pRefSys.NorthIndex) = ref.NorthAxisSign* ...
                cast(obj.ExpectedMagneticFieldStrength, 'like',ex);

            % Initialize noise variances
            updateMeasurementErrCov(obj);

            obj.pQw = cast(obj.InitialProcessNoise, 'like', ex);

            obj.pLinAccelPost = zeros(1,3,'like', ex);
            
            obj.pFirstTime = true;
        end

        function s = saveObjectImpl(obj)
            % Default implementation saves all public properties
            s = saveObjectImpl@fusion.internal.IMUFusionCommon(obj);
            if isLocked(obj)
                s.pMagVec           = obj.pMagVec;
            end
        end        

        function s = loadObjectImpl(obj, s, wasLocked)
            % Reload states if saved version was locked 
            if wasLocked 
                obj.pMagVec           = s.pMagVec;
            end
            loadObjectImpl@fusion.internal.IMUFusionCommon(obj, s, wasLocked);
        end        

        function validateInputsImpl(obj, accelIn, gyroIn, magIn)
            validateattributes(accelIn, {'double', 'single'}, ...
                {'real', 'nonempty', '2d', 'ncols', 3}, ...
                '', 'acceleration',1);
            r = size(accelIn,1);
            validateattributes(gyroIn, {'double', 'single'}, ...
                {'real', 'nonempty', '2d', 'ncols', 3, 'nrows', r}, ...
                '', 'angularVelocity',2);

            validateattributes(magIn, {'double', 'single'}, ... 
                {'real', 'nonempty', '2d', 'ncols', 3, 'nrows', r}, ...
                '', 'magneticField',3);

            % From validation above we know both have the same number of
            % rows. Validate compatibility with frame size.
            validateFrameSize(obj, accelIn);
        end

        function num = getNumInputsImpl(~)
          num = 3;
        end

        function [orientOut, av] = stepImpl(obj, accelIn, gyroIn, magIn)

            % Fuse the sensor readings from the accelerometer, magnetometer
            % and gyroscope. 

            if isa(accelIn, 'single') || isa(gyroIn, 'single') || ...
                isa(magIn, 'single')
                cls = 'single';
            else
                cls = 'double';
            end
            
            % Want to have DecimationFactor-by-3-by-??? matrices so we can
            % page through the 3rd dimension on each trip through the
            % for...loop below.
            afastmat = reshape(accelIn.', 3, obj.DecimationFactor, []);
            mfastmat = reshape(magIn.', 3, obj.DecimationFactor, []);
            gfastmat = reshape(gyroIn.', 3, obj.DecimationFactor, []);

            afastmat = permute(afastmat, [2,1,3]);
            mfastmat = permute(mfastmat, [2,1,3]);
            gfastmat = permute(gfastmat, [2,1,3]);
            
            ref = obj.pRefSys;
            northIdx = ref.NorthIndex;
            gravIdx = ref.GravityIndex;

            numiters = size(afastmat,3);

            % Allocate output
            [av, orientOut] = allocateOutputs(obj, numiters, cls);


            % Loop through each frame. 
            for iter=1:numiters
                % Indirect Kalman filter. Track the *errors* in the
                % estimates of the 
                %   the orientation (as a 3-element rotation vector)
                %   gyroscope bias
                %   linear acceleration
                %   magnetic disturbance
                %
                % The Kalman filter tracks these errors. The actual
                % orientation, gyroscope bias and linear acceleration are
                % updated with the Kalman filter states after
                % predict & correct phases.
                %

                afast = afastmat(:,:,iter);
                gfast = gfastmat(:,:,iter);
                mfast = mfastmat(:,:,iter);
            
                angularVelocity = computeAngularVelocity(obj, gfast, ...
                    obj.pGyroOffset);

                % We only need fast gyro readings. Mag and Accel can be
                % downsampled to the most recent reading.
                mag = mfast(end,:);
                accel = afast(end,:);

                if obj.pFirstTime
                    % Basic tilt corrected ecompass orientation algorithm.
                    % Do this the first time only. Need inputs, so not in setupImpl.
                    Rpost = ref.ecompass(accel, mag);
                    obj.pFirstTime = false;
                    obj.pOrientPost = quaternion(Rpost, 'rotmat', 'frame');
               end
               
                % Update the orientation quaternion based on the gyroscope
                % readings. 
                obj.pOrientPrior = predictOrientation(obj, gfast, ...
                    obj.pGyroOffset, obj.pOrientPost);

                % Back to Rotation matrix
                Rprior = rotmat(obj.pOrientPrior, 'frame'); 

                %%%%%%%%%%%%%%%%
                % The Kalman filter measurement:
                %   Accel estimate of gravity - Gyro estimate of gravity
                %   Mag estimate of MagVec - Gyro estimate of MagVec
                
                % Gyro : Rprior is from the gyro measurements (above).
                % Gravity vector is one column of that matrix.
                gravityGyroMeas = ref.GravityAxisSign * Rprior(:,ref.GravityIndex).'; 

                % Accel: Decay the estimate of the linear acceleration and
                % subtract it from the accelerometer reading. 
                obj.pLinAccelPrior = obj.LinearAccelerationDecayFactor * obj.pLinAccelPost; 
                gravityAccelMeas = ref.GravitySign*accel + obj.pLinAccelPrior; 

                gravityAccelGyroDiff = gravityAccelMeas - gravityGyroMeas;

                % Earth's mag vec (uT) from gyro updates
                magVecGyroMeas = (Rprior * obj.pMagVec')';

                magVecMagGyroDiff = mag - magVecGyroMeas;

                %%%%%%%%%%%%%%%%
                % Compute the Measurement Matrix H and Kalman Gain K
                % Measurement matrix H 6-by-12
                
                h1 = obj.buildHPart(gravityGyroMeas);
                h2 = obj.buildHPart(magVecGyroMeas);
                
                h3 = -h1.*obj.pKalmanPeriod; 
                h4 = -h2.*obj.pKalmanPeriod; 
                   
                H = [h1 h3 eye(3) zeros(3); 
                    h2 h4 zeros(3) -eye(3)];

                % Calculate the Kalman Gain K
                Qv = obj.pQv;
                Qw = obj.pQw;
                
                tmp = ((H * Qw * (H.') + Qv).');

                % Kalman gain is 12-by-6
                K = Qw * (H.')  / ( tmp);

                % Update a posteriori error using the Kalman gain
                ze = [ gravityAccelGyroDiff.';magVecMagGyroDiff.' ];                
                magDistErr = (K(10:12,:) * ze).';

                %%%%%%%%%%%%%%%%%%
                % Jamming Detection
                % Determine if magnetic jamming is happening
                % Power in the magnetic disturbance error:
                magDistPower = (norm(magDistErr.')).^2;
                isJamming = (magDistPower > ...
                    obj.cJammingFactor*(obj.ExpectedMagneticFieldStrength).^2);

                % If jamming is happening, don't use magnetometer
                % measurements.
                if isJamming
                    jamze = gravityAccelGyroDiff';
                    jamxe_post = K(1:9, 1:3) * jamze;
                    orientErr = jamxe_post(1:3).';
                    gyroOffsetErr = jamxe_post(4:6).';
                    linAccelErr = jamxe_post(7:9).';      
                else
                    % Normal case. No Jamming
                    xe_post = K * ze;
                    % Error parts of xe_post
                    orientErr = xe_post(1:3).';
                    gyroOffsetErr = xe_post(4:6).';
                    linAccelErr = xe_post(7:9).';
                end

                % Estimate estimates based on the Kalman filtered error
                % estimates.
                %
                % Convert orientation error into a quaternion.
                % Update a posteriori orientation
                qerr = conj(quaternion(orientErr, 'rotvec')); 
                obj.pOrientPost = obj.pOrientPrior * qerr;
                
                % Force rotation angle to be positive
                if parts(obj.pOrientPost) < 0
                    obj.pOrientPost = -obj.pOrientPost;
                end
                
                obj.pOrientPost = normalize(obj.pOrientPost);
                Rpost = rotmat(obj.pOrientPost, 'frame');

                % Subtract estimated errors for gyro bias and linear
                % acceleration 
                obj.pGyroOffset = obj.pGyroOffset - gyroOffsetErr;
                obj.pLinAccelPost = obj.pLinAccelPrior - linAccelErr; 

                % If no jamming, update the Magnetic Vector estimate 
                if ~isJamming
                    % Rotate the magnetic disturbance to the global frame
                    
                    magDistErrGlobal = ((Rpost.')*magDistErr.').';
                    mtmp = obj.pMagVec - magDistErrGlobal;
                    inclination = atan2(ref.GravityAxisSign * mtmp(ref.GravityIndex), ...
                        ref.NorthAxisSign * mtmp(northIdx));

                    % Limit the inclination angle
                    if inclination < -obj.cInclinationLimit
                        inclination(:) = -obj.cInclinationLimit;
                    end
                    if inclination > obj.cInclinationLimit
                        inclination(:) = obj.cInclinationLimit;
                    end

                    % Use the inclination angle to build a pMagVec
                    obj.pMagVec = zeros(1,3,cls);
                    obj.pMagVec(northIdx) = ref.NorthAxisSign*cos(inclination);
                    obj.pMagVec(gravIdx) = ref.GravityAxisSign*sin(inclination);
                    obj.pMagVec = obj.ExpectedMagneticFieldStrength .* obj.pMagVec;
                    
                end
              
                % Calculate posterior covariance matrix
                Ppost = Qw - K * (H * Qw);
               
                % Update Qw - the process noise. Qw is a function of Ppost
                Qw = zeros(12, cls);

                Qw(getDiags(1:3)) = Ppost(getDiags(1:3)) + ((obj.pKalmanPeriod).^2)* ...
                    (Ppost(getDiags(4:6)) + (obj.GyroscopeDriftNoise + ...
                    obj.GyroscopeNoise));

                Qw(getDiags(4:6)) = Ppost(getDiags(4:6)) + ...
                    obj.GyroscopeDriftNoise;

                offDiag = -obj.pKalmanPeriod * Qw(getDiags(4:6));
                Qw([4 17 30]) = offDiag;
                Qw([37 50 63]) = offDiag;

                Qw(getDiags(7:9)) = (obj.LinearAccelerationDecayFactor.^2)* ...
                    Ppost(getDiags(7:9)) + obj.LinearAccelerationNoise;
                    

                Qw(getDiags(10:12)) = (obj.MagneticDisturbanceDecayFactor.^2)* ...
                    Ppost(getDiags(10:12)) + ...
                    obj.MagneticDisturbanceNoise; 

                obj.pQw = Qw;
                 
                % Done with estimate updates
                av(iter,:) = angularVelocity;
                if strcmpi(obj.OrientationFormat, 'quaternion') 
                    orientOut(iter,:) = obj.pOrientPost;
                else
                    orientOut(:,:,iter) = Rpost; 
                end
            end

        end

        function processTunedPropertiesImpl(obj)
            updateMeasurementErrCov(obj);
        end
 
     end
    
    methods (Access = private)
         function updateMeasurementErrCov(obj)
             accelMeasNoiseVar = obj.AccelerometerNoise + ...
                 obj.LinearAccelerationNoise + ((obj.pKalmanPeriod).^2) *...
                     (obj.GyroscopeDriftNoise  + obj.GyroscopeNoise);
 
             magMeasNoiseVar = obj.MagnetometerNoise + ...
                 obj.MagneticDisturbanceNoise + ((obj.pKalmanPeriod).^2) *...
                     (obj.GyroscopeDriftNoise+ obj.GyroscopeNoise);
 
             obj.pQv = blkdiag(accelMeasNoiseVar*eye(3), ...
                 magMeasNoiseVar*eye(3) );
 
         end
    end

    methods (Static)
        function covinit = getInitialProcCov()
        %GETINITIALPROCCOV - default initial process covariance
            covinit = zeros(12,12);
            covinit(1:3,1:3) = fusion.internal.AHRSFilterBase.cOrientErrVar*eye(3);
            covinit(4:6,4:6) = fusion.internal.AHRSFilterBase.cGyroBiasErrVar*eye(3);
            covinit(1:3,4:6) = fusion.internal.AHRSFilterBase.cOrientGyroBiasErrVar*eye(3);
            covinit(4:6,1:3) = covinit(1:3,4:6);
            covinit(7:9,7:9) = fusion.internal.AHRSFilterBase.cAccErrVar* eye(3);
            covinit(10:12,10:12) = fusion.internal.AHRSFilterBase.cMagErrVar * eye(3);
            covinit = covinit .* fusion.internal.AHRSFilterBase.getInitialProcCovMask; 
        end
        function msk = getInitialProcCovMask()
        %GETINITIALPROCCOVMASK - show possible nonzero entries in mask
            msk = false(12,12);
            msk(1:3,1:3) = true*eye(3);
            msk(4:6,4:6) = true*eye(3);
            msk(1:3,4:6) = true*eye(3);
            msk(4:6,1:3) = msk(1:3,4:6);
            msk(7:9,7:9) = true* eye(3);
            msk(10:12,10:12) = true * eye(3);
        end
    end
end


function d = getDiags(r)
% Get diagonal elements in a 12 x 12 matrix where r is the column (or row) index
sz = 12;
rm1 = r - 1;
d = rm1*(sz+1) + 1;
end

