classdef (Hidden) ErrorStateIMUGPSFuserBase < fusion.internal.INSFilterESKF
%ErrorStateIMUGPSFuserBase Base class for insfilterErrorState
%
%   This class is for internal use only. It may be removed in the future.

%   Copyright 2018-2019 The MathWorks, Inc.

%#codegen
    
    properties
        % KF Values
        
        %State Nominal state vector
        %   Specify the state vector as a 17-element real finite vector. 
        %   The units and indices for each state are as follows:
        %
        %       States                            Units    Index
        %       Orientation (quaternion parts)             1:4  
        %       Position (NAV)                    m        5:7  
        %       Velocity (NAV)                    m/s      8:10 
        %       Gyroscope Bias (XYZ)              rad/s    11:13
        %       Accelerometer Bias (XYZ)          m/s^2    14:16
        %       Visual Odometry Scale                      17   
        %
        %   The default value is [1; zeros(15,1); 1].
        State = [1; zeros(15,1); 1];
        %StateCovariance Error covariance matrix
        %   Specify the value of the error covariance matrix. The error
        %   covariance matrix is a 16-by-16 matrix. The default value is
        %   ones(16).
        StateCovariance = ones(16);

        % Process noises
        
        %AccelerometerNoise Noise in the accelerometer signal (m/s^2)^2
        %   Specify the noise in the accelerometer data as a positive 
        %   scalar or 3-element row vector in (m/s^2)^2. The default value 
        %   is [1.0e-4 1.0e-4 1.0e-4].
        AccelerometerNoise = 1e-4*ones(1,3);
        %GyroscopeNoise Noise in the gyroscope signal (rad/s)^2
        %   Specify the noise in the gyroscope data as a positive scalar or
        %   3-element row vector in (rad/s)^2. The default value is 
        %   [1.0e-6 1.0e-6 1.0e-6].
        GyroscopeNoise = 1e-6*ones(1,3);
        %AccelerometerBiasNoise Noise in the accelerometer bias (m/s^2)^2
        %   Specify the noise in the accelerometer bias as a positive 
        %   scalar or 3-element row vector in (m/s^2)^2. The default value 
        %   is [1.0e-4 1.0e-4 1.0e-4].
        AccelerometerBiasNoise = 1e-4*ones(1,3);
        %GyroscopeBiasNoise Noise in the gyroscope bias (rad/s)^2
        %   Specify the noise in the gyroscope bias as a positive scalar or
        %   3-element row vector in (rad/s)^2. The default value is 
        %   [1.0e-9 1.0e-9 1.0e-9].
        GyroscopeBiasNoise = 1e-9*ones(1,3);
    end
    
    properties (Hidden, Constant)
        NumStates = 17;
        NumErrorStates = 16;
    end
    
    methods
        function obj = ErrorStateIMUGPSFuserBase(varargin)
            matlabshared.fusionutils.internal.setProperties(obj, nargin, varargin{:});
        end
        
        function predict(obj, accelMeas, gyroMeas)
        %PREDICT(FILT, ACCELMEAS, GYROMEAS) fuses the accelerometer and
        %   gyroscope data to update the state and the state estimation
        %   error covariance.
        %
        %   The inputs to predict are defined as follows:
        %
        %       ACCELMEAS    Accelerometer measurement in the local sensor 
        %                    body reference frame, specified as a 3-element
        %                    row vector in meters per second squared.
        %
        %       GYROMEAS     Gyroscope measurement in the local sensor body
        %                    reference frame, specified as a 3-element row
        %                    vector in radians per second.
            
            validateattributes(accelMeas, {'double','single'}, ...
                {'real','finite','2d','ncols',3,'nonempty'}, ...
                '', ...
                'acceleration');
            validateattributes(gyroMeas, {'double','single'}, ...
                {'real','finite','2d','ncols',3,'nonempty'}, ...
                '', ...
                'angularVelocity');
            n = size(accelMeas, 1);
            coder.internal.assert(size(gyroMeas, 1) == n, ...
                'shared_positioning:insfilter:RowMismatch');
            
            rf = rfconfig(obj.ReferenceFrame);
            % Invert the accelerometer signal if linear acceleration is
            % negative in the reference frame.
            accelMeas = rf.LinAccelSign.*accelMeas;
            
            x = obj.State;
            obj.State = stateTransition(obj, x, accelMeas(:), gyroMeas(:));
            
            F = stateTransitionJacobian(obj, x, accelMeas(:), gyroMeas(:));
            G = processNoiseJacobian(obj, x);
            U = processNoiseCovariance(obj);
            obj.StateCovariance = predictCovEqn(obj, obj.StateCovariance, F, U, G);
        end
        
        function fusemvo(obj, voPos, RposIn, voOrientIn, RorientIn)
        %FUSEMVO Correct states using monocular visual odometry
        %
        %   FUSEMVO(FILT, VOPOS, RPOS, VOORIENT, RORIENT) fuses the
        %   position and orientation data in the monocular visual odometry
        %   measurement to correct the state and state estimation error
        %   covariance.
        %
        %   The inputs to FUSEMVO are defined as follows:
        %
        %       VOPOS       Position of the camera in the NAV
        %                   coordinate system specified as a real finite
        %                   3-element row vector in meters.
        %
        %       RPOS        Measurement covariance matrix of the monocular
        %                   visual odometry position measurements in the
        %                   local NAV coordinate system. This is specified
        %                   as a real finite scalar, 3-element vector, or 
        %                   3-by-3 matrix in meters squared.
        %
        %       VOORIENT    Orientation of the camera with respect to the
        %                   NAV coordinate system specified as a scalar
        %                   quaternion or a single or double 3-by-3
        %                   rotation matrix. The quaternion or rotation
        %                   matrix is a frame rotation from the NAV
        %                   coordinate system to the current camera
        %                   coordinate system.
        %
        %       RORIENT     Measurement covariance matrix of the monocular
        %                   visual odometry orientation measurements. This
        %                   is specified as a real finite scalar, 3-element
        %                   vector, or 3-by-3 matrix in radians squared.
        
            validateattributes(voPos, {'double','single'}, ...
                {'real','finite','2d','nrows',1,'ncols',3,'nonempty'}, ...
                '', ...
                'voPos');
            if isa(voOrientIn, 'quaternion')
                validateattributes(voOrientIn, {'quaternion'}, ...
                    {'finite', 'scalar'}, ...
                    '', ...
                    'voOrient');
                voOrient = voOrientIn;
            else
                validateattributes(voOrientIn, {'double','single'}, ...
                    {'real','finite','2d','nrows',3,'ncols',3,'nonempty'}, ...
                    '', ...
                    'voOrient');
                voOrient = quaternion(voOrientIn, 'rotmat', 'frame');
            end
            Rpos = validateExpandNoise(obj, RposIn, 3, 'Rpos');
            Rorient = validateExpandNoise(obj, RorientIn, 3, 'Rorient');
            
            x = obj.State;
            P = obj.StateCovariance;
            
            z = [voPos(:); compact(voOrient).'];
            h = measurementMVO(obj, x);
            deltaPos = z(1:3) - h(1:3);
            zQ = quaternion(z(4:7).');
            hQ = quaternion(h(4:7).');
            deltaQ = conj(hQ) * zQ;
            deltaAng = rotvec(deltaQ).';
            
            zminush = [deltaPos; deltaAng];
            
            H = cast(measurementJacobianMVO(obj, x), 'like', x);
            
            R = cast(blkdiag(Rpos, Rorient), 'like', x);
            
            S = H*P*(H.') + R;
            K = P*(H.') / S;
            errorState = K*zminush;
            
            obj.StateCovariance = (eye(size(K*H)) - K*H)*P*((eye(size(K*H)) - K*H).') ...
                + K*R*(K.');
            
            injectError(obj, errorState);
            resetError(obj);
        end
        
        function fusegps(obj, gpsPos, RposIn, gpsVel, RvelIn)
        %FUSEGPS Correct state estimates using GPS 
        %
        %   FUSEGPS(FILT, LLA, RPOS, VEL, RVEL) fuses GPS data to
        %   correct the state estimate. 
        %
        %   The inputs to FUSEGPS are defined as follows:
        %       
        %       GPSLLA    Position of the GPS receiver in geodetic 
        %                 latitude, longitude, and altitude coordinates
        %                 specified as a real finite 3-element row vector.
        %                 Latitude and longitude are in degrees with north
        %                 and east being positive. Altitude is in meters.
        %  
        %       POSCOV    Measurement covariance matrix of the GPS receiver 
        %                 position measurements in the local NAV coordinate
        %                 system. This is specified as a real finite 
        %                 scalar, 3-element vector, or 3-by-3 matrix in 
        %                 meters squared.
        %  
        %       GPSVEL    Velocity of the GPS receiver in the local NAV 
        %                 coordinate system specified as a real finite
        %                 3-element row vector in meters per second.
        %  
        %       VELCOV    Measurement covariance matrix of the GPS receiver
        %                 velocity measurements in the local NAV coordinate
        %                 system. This is specified as a real finite 
        %                 scalar, 3-element vector, or 3-by-3 matrix in 
        %                 meters squared per second squared.
            
            validateattributes(gpsPos, {'double','single'}, ...
                {'real','finite','2d','nrows',1,'ncols',3,'nonempty'}, ...
                '', ...
                'gpsPos');
            validateattributes(gpsVel, {'double','single'}, ...
                {'real','finite','2d','nrows',1,'ncols',3,'nonempty'}, ...
                '', ...
                'gpsVel');
            Rpos = validateExpandNoise(obj, RposIn, 3, 'Rpos');
            Rvel = validateExpandNoise(obj, RvelIn, 3, 'Rvel');
            
            x = obj.State;
            P = obj.StateCovariance;
            
            rf = rfconfig(obj.ReferenceFrame);
            z = [rf.lla2frame(gpsPos, obj.ReferenceLocation).'; gpsVel(:)];
            h = measurementGPS(obj, x);
            zminush = z - h;
            
            H = measurementJacobianGPS(obj, x);
            
            R = blkdiag(Rpos, Rvel);
            
            S = H*P*(H.') + R;
            K = P*(H.') / S;
            errorState = K*zminush;
            
            obj.StateCovariance = (eye(size(K*H)) - K*H)*P*((eye(size(K*H)) - K*H).') ...
                + K*R*(K.');
            
            injectError(obj, errorState);
            resetError(obj);
        end
        
        function reset(obj)
            %RESET Set state and state error covariance to default values 
            %   reset(FUSE) resets the State and StateCovariance to their 
            %   default values and resets the internal states of the 
            %   filter.
            
            obj.State = cast([1; zeros(15,1); 1], 'like', obj.State);
            obj.StateCovariance = ones(16, 'like', obj.StateCovariance);
        end
    end
    
    methods % Set methods
        function set.State(obj, val)
            validateattributes(val, {'double','single'}, ...
                {'real','finite','vector','numel',fusion.internal.ErrorStateIMUGPSFuserBase.NumStates}, ...
                '', ...
                'State');
            % Ensure it is a column vector.
            obj.State = val(:);
        end
        
        function set.StateCovariance(obj, val)
            validateattributes(val, {'double','single'}, ...
                {'finite','real','2d','square', ...
                'numel',(fusion.internal.ErrorStateIMUGPSFuserBase.NumErrorStates).^2, ...
                'nonempty','nonsparse'}, ...
                '', ...
                'StateCovariance');
            obj.StateCovariance = val;
        end
        
        function set.AccelerometerNoise(obj, val)
            validateattributes(val, {'double', 'single'}, ...
                {'finite', 'real', 'positive', '2d', ...
                'nonnan', 'nonempty', 'nonsparse'}, ...
                '', 'AccelerometerNoise');
           
            % Enforce scalar or 3-element vector inputs.
            n = numel(val);
            coder.internal.assert((n == 1) || (n == 3), ... 
                'shared_positioning:insfilter:OneorThreeElements', ...
                'AccelerometerNoise');

            obj.AccelerometerNoise(:) = val(:).';
        end
        
        function set.GyroscopeNoise(obj, val)
            validateattributes(val, {'double', 'single'}, ...
                {'finite', 'real', 'positive', '2d', ...
                'nonnan', 'nonempty', 'nonsparse'}, ...
                '', 'GyroscopeNoise');
           
            % Enforce scalar or 3-element vector inputs.
            n = numel(val);
            coder.internal.assert((n == 1) || (n == 3), ... 
                'shared_positioning:insfilter:OneorThreeElements', ...
                'GyroscopeNoise');

            obj.GyroscopeNoise(:) = val(:).';
        end
        
        function set.AccelerometerBiasNoise(obj, val)
            validateattributes(val, {'double', 'single'}, ...
                {'finite', 'real', 'positive', '2d', ...
                'nonnan', 'nonempty', 'nonsparse'}, ...
                '', 'AccelerometerBiasNoise');
           
            % Enforce scalar or 3-element vector inputs.
            n = numel(val);
            coder.internal.assert((n == 1) || (n == 3), ... 
                'shared_positioning:insfilter:OneorThreeElements', ...
                'AccelerometerBiasNoise');

            obj.AccelerometerBiasNoise(:) = val(:).';
        end
        
        function set.GyroscopeBiasNoise(obj, val)
            validateattributes(val, {'double', 'single'}, ...
                {'finite', 'real', 'positive', '2d', ...
                'nonnan', 'nonempty', 'nonsparse'}, ...
                '', 'GyroscopeBiasNoise');
           
            % Enforce scalar or 3-element vector inputs.
            n = numel(val);
            coder.internal.assert((n == 1) || (n == 3), ... 
                'shared_positioning:insfilter:OneorThreeElements', ...
                'GyroscopeBiasNoise');

            obj.GyroscopeBiasNoise(:) = val(:).';
        end
    end
    
    methods (Access = protected)
        function pos = getPosition(obj)
            pos = obj.State(5:7).';
        end
        
        function orient = getOrientation(obj)
            orient = quaternion(obj.State(1:4).');
        end
        
        function vel = getVelocity(obj)
            vel = obj.State(8:10).';
        end
        
        % Predict Helper Functions
        function x = stateTransition(obj, x, accelMeas, gyroMeas)
            quat = quaternion(x(1:4).');
            pos = x(5:7);
            vel = x(8:10);
            gyroBias = x(11:13);
            accelBias = x(14:16);
            scaleVO = x(17);
            
            dt = 1 ./ obj.IMUSampleRate;
            
            newQuat = quat * quaternion([1, 0.5*dt*(gyroMeas(:).'-gyroBias(:).')]); %quaternion((gyroMeas.'-gyroBias.')*dt, 'rotvec');
            newPos = pos + vel*dt;
            
            rf = rfconfig(obj.ReferenceFrame);
            grav = zeros(1,3, 'like', x);
            grav(rf.GravityIndex) = -rf.GravitySign*rf.GravityAxisSign*gravms2();
            newAcc = rotatepoint(quat, accelMeas(:).' - accelBias(:).') - grav(:).';
            newVel = vel + newAcc(:) * dt;
            newGyroBias = gyroBias;
            newAccelBias = accelBias;
            newScaleVO = scaleVO;
            x = [compact(normalize(newQuat)).'; newPos(:); newVel(:); newGyroBias(:); newAccelBias(:); newScaleVO(:)];
        end
        
        function F = stateTransitionJacobian(obj, x, accelMeas, ~) %gyroMeas)
            qw = x(1);
            qx = x(2);
            qy = x(3);
            qz = x(4);
            pn = x(5); %#ok<NASGU>
            pe = x(6); %#ok<NASGU>
            pd = x(7); %#ok<NASGU>
            vn = x(8); %#ok<NASGU>
            ve = x(9); %#ok<NASGU>
            vd = x(10); %#ok<NASGU>
            wbx = x(11); %#ok<NASGU>
            wby = x(12); %#ok<NASGU>
            wbz = x(13); %#ok<NASGU>
            abx = x(14);
            aby = x(15);
            abz = x(16);
            scaleVO = x(17); %#ok<NASGU>
            
            dt = 1 ./ obj.IMUSampleRate;
            
            amx = accelMeas(1);
            amy = accelMeas(2);
            amz = accelMeas(3);
            
            F = [ ...  
                1,                                                                                                                 0,                                                                                                                 0, 0, 0, 0,  0,  0,  0, -dt*(qw^2 + qx^2 - qy^2 - qz^2),          dt*(2*qw*qz - 2*qx*qy),         -dt*(2*qw*qy + 2*qx*qz),                               0,                               0,                               0, 0; ...
                0,                                                                                                                 1,                                                                                                                 0, 0, 0, 0,  0,  0,  0,         -dt*(2*qw*qz + 2*qx*qy), -dt*(qw^2 - qx^2 + qy^2 - qz^2),          dt*(2*qw*qx - 2*qy*qz),                               0,                               0,                               0, 0; ...
                0,                                                                                                                 0,                                                                                                                 1, 0, 0, 0,  0,  0,  0,          dt*(2*qw*qy - 2*qx*qz),         -dt*(2*qw*qx + 2*qy*qz), -dt*(qw^2 - qx^2 - qy^2 + qz^2),                               0,                               0,                               0, 0; ...
                0,                                                                                                                 0,                                                                                                                 0, 1, 0, 0, dt,  0,  0,                               0,                               0,                               0,                               0,                               0,                               0, 0; ...
                0,                                                                                                                 0,                                                                                                                 0, 0, 1, 0,  0, dt,  0,                               0,                               0,                               0,                               0,                               0,                               0, 0; ...
                0,                                                                                                                 0,                                                                                                                 0, 0, 0, 1,  0,  0, dt,                               0,                               0,                               0,                               0,                               0,                               0, 0; ...
                0, -dt*((abz - amz)*(qw^2 - qx^2 - qy^2 + qz^2) - (abx - amx)*(2*qw*qy - 2*qx*qz) + (aby - amy)*(2*qw*qx + 2*qy*qz)),  dt*((aby - amy)*(qw^2 - qx^2 + qy^2 - qz^2) + (abx - amx)*(2*qw*qz + 2*qx*qy) - (abz - amz)*(2*qw*qx - 2*qy*qz)), 0, 0, 0,  1,  0,  0,                               0,                               0,                               0, -dt*(qw^2 + qx^2 - qy^2 - qz^2),          dt*(2*qw*qz - 2*qx*qy),         -dt*(2*qw*qy + 2*qx*qz), 0; ...
                dt*((abz - amz)*(qw^2 - qx^2 - qy^2 + qz^2) - (abx - amx)*(2*qw*qy - 2*qx*qz) + (aby - amy)*(2*qw*qx + 2*qy*qz)),                                                                                                                 0, -dt*((abx - amx)*(qw^2 + qx^2 - qy^2 - qz^2) - (aby - amy)*(2*qw*qz - 2*qx*qy) + (abz - amz)*(2*qw*qy + 2*qx*qz)), 0, 0, 0,  0,  1,  0,                               0,                               0,                               0,         -dt*(2*qw*qz + 2*qx*qy), -dt*(qw^2 - qx^2 + qy^2 - qz^2),          dt*(2*qw*qx - 2*qy*qz), 0; ...
                -dt*((aby - amy)*(qw^2 - qx^2 + qy^2 - qz^2) + (abx - amx)*(2*qw*qz + 2*qx*qy) - (abz - amz)*(2*qw*qx - 2*qy*qz)),  dt*((abx - amx)*(qw^2 + qx^2 - qy^2 - qz^2) - (aby - amy)*(2*qw*qz - 2*qx*qy) + (abz - amz)*(2*qw*qy + 2*qx*qz)),                                                                                                                 0, 0, 0, 0,  0,  0,  1,                               0,                               0,                               0,          dt*(2*qw*qy - 2*qx*qz),         -dt*(2*qw*qx + 2*qy*qz), -dt*(qw^2 - qx^2 - qy^2 + qz^2), 0; ...
                0,                                                                                                                 0,                                                                                                                 0, 0, 0, 0,  0,  0,  0,                               1,                               0,                               0,                               0,                               0,                               0, 0; ...
                0,                                                                                                                 0,                                                                                                                 0, 0, 0, 0,  0,  0,  0,                               0,                               1,                               0,                               0,                               0,                               0, 0; ...
                0,                                                                                                                 0,                                                                                                                 0, 0, 0, 0,  0,  0,  0,                               0,                               0,                               1,                               0,                               0,                               0, 0; ...
                0,                                                                                                                 0,                                                                                                                 0, 0, 0, 0,  0,  0,  0,                               0,                               0,                               0,                               1,                               0,                               0, 0; ...
                0,                                                                                                                 0,                                                                                                                 0, 0, 0, 0,  0,  0,  0,                               0,                               0,                               0,                               0,                               1,                               0, 0; ...
                0,                                                                                                                 0,                                                                                                                 0, 0, 0, 0,  0,  0,  0,                               0,                               0,                               0,                               0,                               0,                               1, 0; ...
                0,                                                                                                                 0,                                                                                                                 0, 0, 0, 0,  0,  0,  0,                               0,                               0,                               0,                               0,                               0,                               0, 1; ...
                ];
        end
        
        function G = processNoiseJacobian(obj, x)
            qw = x(1);
            qx = x(2);
            qy = x(3);
            qz = x(4);
            pn = x(5); %#ok<NASGU>
            pe = x(6); %#ok<NASGU>
            pd = x(7); %#ok<NASGU>
            vn = x(8); %#ok<NASGU>
            ve = x(9); %#ok<NASGU>
            vd = x(10); %#ok<NASGU>
            wbx = x(11); %#ok<NASGU>
            wby = x(12); %#ok<NASGU>
            wbz = x(13); %#ok<NASGU>
            abx = x(14); %#ok<NASGU>
            aby = x(15); %#ok<NASGU>
            abz = x(16); %#ok<NASGU>
            scaleVO = x(17); %#ok<NASGU>
            
            dt = 1 ./ obj.IMUSampleRate;
            
            G = [ ...
                0,                              0,                              0, dt*(qw^2 + qx^2 - qy^2 - qz^2),        -dt*(2*qw*qz - 2*qx*qy),         dt*(2*qw*qy + 2*qx*qz),  0,  0,  0,  0,  0,  0; ...
                0,                              0,                              0,         dt*(2*qw*qz + 2*qx*qy), dt*(qw^2 - qx^2 + qy^2 - qz^2),        -dt*(2*qw*qx - 2*qy*qz),  0,  0,  0,  0,  0,  0; ...
                0,                              0,                              0,        -dt*(2*qw*qy - 2*qx*qz),         dt*(2*qw*qx + 2*qy*qz), dt*(qw^2 - qx^2 - qy^2 + qz^2),  0,  0,  0,  0,  0,  0; ...
                0,                              0,                              0,                              0,                              0,                              0,  0,  0,  0,  0,  0,  0; ...
                0,                              0,                              0,                              0,                              0,                              0,  0,  0,  0,  0,  0,  0; ...
                0,                              0,                              0,                              0,                              0,                              0,  0,  0,  0,  0,  0,  0; ...
                dt*(qw^2 + qx^2 - qy^2 - qz^2),        -dt*(2*qw*qz - 2*qx*qy),         dt*(2*qw*qy + 2*qx*qz),                              0,                              0,                              0,  0,  0,  0,  0,  0,  0; ...
                dt*(2*qw*qz + 2*qx*qy), dt*(qw^2 - qx^2 + qy^2 - qz^2),        -dt*(2*qw*qx - 2*qy*qz),                              0,                              0,                              0,  0,  0,  0,  0,  0,  0; ...
                -dt*(2*qw*qy - 2*qx*qz),         dt*(2*qw*qx + 2*qy*qz), dt*(qw^2 - qx^2 - qy^2 + qz^2),                              0,                              0,                              0,  0,  0,  0,  0,  0,  0; ...
                0,                              0,                              0,                              0,                              0,                              0,  0,  0,  0, dt,  0,  0; ...
                0,                              0,                              0,                              0,                              0,                              0,  0,  0,  0,  0, dt,  0; ...
                0,                              0,                              0,                              0,                              0,                              0,  0,  0,  0,  0,  0, dt; ...
                0,                              0,                              0,                              0,                              0,                              0, dt,  0,  0,  0,  0,  0; ...
                0,                              0,                              0,                              0,                              0,                              0,  0, dt,  0,  0,  0,  0; ...
                0,                              0,                              0,                              0,                              0,                              0,  0,  0, dt,  0,  0,  0; ...
                0,                              0,                              0,                              0,                              0,                              0,  0,  0,  0,  0,  0,  0; ...
                ];
        end
        
        function U = processNoiseCovariance(obj)
            
            U = obj.IMUSampleRate .* blkdiag(diag(obj.AccelerometerNoise), ...
                diag(obj.GyroscopeNoise), ...
                diag(obj.AccelerometerBiasNoise), ...
                diag(obj.GyroscopeBiasNoise));
        end
        
        % Correct Helper Functions
        function h = measurementGPS(~, x)
            quat = quaternion(x(1:4).'); %#ok<NASGU>
            pos = x(5:7);
            vel = x(8:10);
            gyroBias = x(11:13); %#ok<NASGU>
            accelBias = x(14:16); %#ok<NASGU>
            scaleVO = x(17); %#ok<NASGU>
            
            h = [pos; vel];
        end
        
        function H = measurementJacobianGPS(~, x)
            qw = x(1); %#ok<NASGU>
            qx = x(2); %#ok<NASGU>
            qy = x(3); %#ok<NASGU>
            qz = x(4); %#ok<NASGU>
            pn = x(5); %#ok<NASGU>
            pe = x(6); %#ok<NASGU>
            pd = x(7); %#ok<NASGU>
            vn = x(8); %#ok<NASGU>
            ve = x(9); %#ok<NASGU>
            vd = x(10); %#ok<NASGU>
            wbx = x(11); %#ok<NASGU>
            wby = x(12); %#ok<NASGU>
            wbz = x(13); %#ok<NASGU>
            abx = x(14); %#ok<NASGU>
            aby = x(15); %#ok<NASGU>
            abz = x(16); %#ok<NASGU>
            scale = x(17); %#ok<NASGU>
            
            H = [ ...
                0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0; ...
                0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0; ...
                0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0; ...
                0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0; ...
                0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0; ...
                0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0; ...
                ];
        end
        
        function h = measurementMVO(~, x)
            quat = quaternion(x(1:4).');
            pos = x(5:7);
            vel = x(8:10); %#ok<NASGU>
            gyroBias = x(11:13); %#ok<NASGU>
            accelBias = x(14:16); %#ok<NASGU>
            scaleVO = x(17);
            
            h = [pos.*scaleVO; compact(quat).'];
        end
        
        function H = measurementJacobianMVO(~, x)
            qw = x(1); %#ok<NASGU>
            qx = x(2); %#ok<NASGU>
            qy = x(3); %#ok<NASGU>
            qz = x(4); %#ok<NASGU>
            pn = x(5); 
            pe = x(6);
            pd = x(7);
            vn = x(8); %#ok<NASGU>
            ve = x(9); %#ok<NASGU>
            vd = x(10); %#ok<NASGU>
            wbx = x(11); %#ok<NASGU>
            wby = x(12); %#ok<NASGU>
            wbz = x(13); %#ok<NASGU>
            abx = x(14); %#ok<NASGU>
            aby = x(15); %#ok<NASGU>
            abz = x(16); %#ok<NASGU>
            scale = x(17);
            
            % The lower-left identity is an estimate of the change in
            % quaternion.
            H = [ ...
                0,           0,           0, scale,     0,     0, 0, 0, 0, 0, 0, 0, 0, 0, 0, pn; ...
                0,           0,           0,     0, scale,     0, 0, 0, 0, 0, 0, 0, 0, 0, 0, pe; ...
                0,           0,           0,     0,     0, scale, 0, 0, 0, 0, 0, 0, 0, 0, 0, pd; ...
                1,           0,           0,     0,     0,     0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  0; ...
                0,           1,           0,     0,     0,     0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  0; ...
                0,           0,           1,     0,     0,     0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  0; ...
                ];
        end
        
        function injectError(obj, deltaX)
            x = obj.State;
            
            quat = quaternion(x(1:4).');
            pos = x(5:7);
            vel = x(8:10);
            gyroBias = x(11:13);
            accelBias = x(14:16);
            scaleVO = x(17);
            
            quatErr = quaternion(deltaX(1:3).', 'rotvec');
            posErr = deltaX(4:6);
            velErr = deltaX(7:9);
            gyroBiasErr = deltaX(10:12);
            accelBiasErr = deltaX(13:15);
            scaleVOErr = deltaX(16);
            
            quat = quat .* quatErr;
            pos = pos + posErr;
            vel = vel + velErr;
            gyroBias = gyroBias + gyroBiasErr;
            accelBias = accelBias + accelBiasErr;
            scaleVO = scaleVO + scaleVOErr;
            
            obj.State = ...
                [compact(normalize(quat)).'; pos; vel; gyroBias; accelBias; scaleVO];
        end
        
        function resetError(obj)
            
            G = eye(obj.NumErrorStates);
            obj.StateCovariance = G*obj.StateCovariance*(G.');
        end
    end
end %classdef

%Helper Functions

function g = gravms2()
    g = fusion.internal.UnitConversions.geeToMetersPerSecondSquared(1);
end

function rf = rfconfig(refStr)
%RFCONFIG Return the reference frame configuration object based on the 
%   reference frame string.
rf = fusion.internal.frames.ReferenceFrame.getMathObject( ...
                refStr);
end
