classdef (Hidden) IMUSensorParameters
    %   Abstract class for fusion.internal.IMUSensorParameters.
    %
    %   This class is used to validate sensor parameters.
    %
    %   This class is for internal use only. It may be removed in the future.
    
    %   Copyright 2017-2019 The MathWorks, Inc.
    
    %#codegen
    
    properties
        % MeasurementRange Maximum sensor reading
        % Specify the maximum sensor reading as a real positive scalar. The
        % default value is Inf.
        MeasurementRange = Inf;
        % Resolution Resolution of sensor measurements
        % Specify the resolution as a real nonnegative scalar. The default
        % value is 0.
        Resolution = 0;
        % ConstantBias Constant sensor offset bias
        % Specify the constant bias as a real scalar or 3-element row
        % vector. The default value is [0 0 0].
        ConstantBias = [0 0 0];
        % AxesMisalignment Sensor axes skew (%)
        % Specify the axes misalignment as a real scalar or 3-element row
        % vector with values between 0 and 100, inclusive. The default
        % value is [0 0 0].
        AxesMisalignment = [0 0 0];
        % NoiseDensity Power spectral density of sensor noise
        % Specify the noise density as a real scalar or 3-element row
        % vector. The default value is [0 0 0].
        NoiseDensity = [0 0 0];
        % BiasInstability Instability of the bias offset
        % Specify the bias instability as a real scalar or 3-element row
        % vector. The default value is [0 0 0].
        BiasInstability = [0 0 0];
        % RandomWalk Integrated white noise of sensor
        % Specify the random walk as a real scalar or 3-element row vector.
        % The default value is [0 0 0];
        RandomWalk = [0 0 0];
        % TemperatureBias Sensor bias from temperature
        % Specify the temperature bias as a real scalar or 3-element row
        % vector. The default value is [0 0 0].
        TemperatureBias = [0 0 0];
        % TemperatureScaleFactor Scale factor error from temperature 
        % (%/degrees C)
        % Specify the temperature scale factor error as a real scalar or
        % 3-element row vector with values between 0 and 100, inclusive.
        % The default value is [0 0 0].
        TemperatureScaleFactor = [0 0 0];
    end
    
    properties (Hidden, Constant)
        AxesMisalignmentUnits = '%';
        TemperatureScaleFactorUnits = ['%/' char(176) 'C'];
    end

    properties (Hidden)
        MeasurementRangeUnits
        ResolutionUnits
        ConstantBiasUnits
        NoiseDensityUnits
        BiasInstabilityUnits
        RandomWalkUnits
        TemperatureBiasUnits
    end
    
    methods % Display Unit Methods
        function val = get.MeasurementRangeUnits(obj)
            val = sprintf(obj.getDisplayUnit);
        end
        function val = get.ResolutionUnits(obj)
            val = sprintf('(%s)/LSB',obj.getDisplayUnit);
        end
        function val = get.ConstantBiasUnits(obj)
            val = sprintf(obj.getDisplayUnit);
        end
        function val = get.NoiseDensityUnits(obj)
            val = sprintf(['(%s)/' char(8730) 'Hz'],obj.getDisplayUnit);
        end
        function val = get.RandomWalkUnits(obj)
            val = sprintf(['(%s)*' char(8730) 'Hz'],obj.getDisplayUnit);
        end
        function val = get.BiasInstabilityUnits(obj)
            val = sprintf(obj.getDisplayUnit);
        end
        function val = get.TemperatureBiasUnits(obj)
            val = sprintf(['(%s)/' char(176) 'C'],obj.getDisplayUnit);
        end
    end
    
    methods (Access = protected)
        function unit = getDisplayUnitImpl(~)
            unit = '';
        end
    end
    
    methods (Access = protected, Sealed)
        function unit = getDisplayUnit(obj)
            unit = getDisplayUnitImpl(obj);
        end
    end
    
    methods
        % Constructor
        function obj = IMUSensorParameters(varargin)
            obj = matlabshared.fusionutils.internal.setProperties(obj, nargin, varargin{:});
        end
        
%---------------------Set Methods Begin------------------------------------
        
        function obj = set.MeasurementRange(obj,val)
            validateattributes(val, {'single','double'}, {'scalar','real','positive','nonnan'}, '', 'MeasurementRange');
            obj.MeasurementRange = val;
        end
        
        function obj = set.Resolution(obj,val)
            validateattributes(val, {'single','double'}, {'scalar','real','nonnegative','finite'}, '', 'Resolution');
            obj.Resolution = val;
        end
        
        function obj = set.ConstantBias(obj,val)
            validateattributes(val, {'single','double'}, {'real','finite'}, '', 'ConstantBias');
            fusion.internal.IMUSensorParameters.validateSize(val, 'ConstantBias');
            obj.ConstantBias(:) = val;
        end
        
        function obj = set.AxesMisalignment(obj,val)
            validateattributes(val, {'single','double'}, {'real', '>=',0,'<=',100}, '', 'AxesMisalignment');
            fusion.internal.IMUSensorParameters.validateSize(val, 'AxesMisalignment');
            obj.AxesMisalignment(:) = val;
        end
        
        function obj = set.NoiseDensity(obj,val)
            validateattributes(val, {'single','double'}, {'real', 'nonnegative', 'finite'}, '', 'NoiseDensity');
            fusion.internal.IMUSensorParameters.validateSize(val, 'NoiseDensity');
            obj.NoiseDensity(:) = val;
        end
        
        function obj = set.BiasInstability(obj,val)
            validateattributes(val, {'single','double'}, {'real', 'nonnegative', 'finite'}, '', 'BiasInstability');
            fusion.internal.IMUSensorParameters.validateSize(val, 'BiasInstability');
            obj.BiasInstability(:) = val;
        end
        
        function obj = set.RandomWalk(obj,val)
            validateattributes(val, {'single','double'}, {'real', 'nonnegative', 'finite'}, '', 'RandomWalk');
            fusion.internal.IMUSensorParameters.validateSize(val, 'RandomWalk');
            obj.RandomWalk(:) = val;
        end
        
        function obj = set.TemperatureBias(obj,val)
            validateattributes(val, {'single','double'}, {'real', 'finite'}, '', 'TemperatureBias');
            fusion.internal.IMUSensorParameters.validateSize(val, 'TemperatureBias');
            obj.TemperatureBias(:) = val;
        end
        
        function obj = set.TemperatureScaleFactor(obj,val)
            validateattributes(val, {'single','double'}, {'real', '>=',0,'<=',100}, '', 'TemperatureScaleFactor');
            fusion.internal.IMUSensorParameters.validateSize(val, 'TemperatureScaleFactor');
            obj.TemperatureScaleFactor(:) = val;
        end
        
%---------------------Set Methods End--------------------------------------
        
    end
    
    methods (Abstract, Access = protected)
        sobj = createSystemObjectImpl(obj);
    end
    
    methods (Access = protected)
        function updateSystemObjectImpl(~, ~)
        end
    end
    
    methods (Hidden, Sealed)
        function sobj = createSystemObject(obj, varargin)
            sobj = createSystemObjectImpl(obj, varargin{:});
            updateSystemObject(obj, sobj);
        end
        
        function updateSystemObject(obj, sobj)
            updateSystemObjectImpl(obj, sobj);
            sobj.MeasurementRange       = obj.MeasurementRange;
            sobj.Resolution             = obj.Resolution;
            sobj.ConstantBias           = obj.ConstantBias;
            sobj.AxesMisalignment       = obj.AxesMisalignment;
            sobj.NoiseDensity           = obj.NoiseDensity;
            sobj.BiasInstability        = obj.BiasInstability;
            sobj.RandomWalk             = obj.RandomWalk;
            sobj.TemperatureBias        = obj.TemperatureBias;
            sobj.TemperatureScaleFactor = obj.TemperatureScaleFactor;            
        end
    end
    
    methods (Access = protected)
        function rowVectProp = getRowVector(obj, prop)
            if isscalar(obj.(prop))
                rowVectProp = repmat(obj.(prop), 1, 3);
            else
                rowVectProp = obj.(prop);
            end
        end
    end
    
    methods (Static, Access = protected)
        function validateSize(val, prop)
            % Check that val is a scalar or a 3-element row vector.
            cond = any([1 1] ~= size(val)) && any([1 3] ~= size(val));
            coder.internal.errorIf(cond, 'shared_positioning:internal:IMUSensorParameters:invalidSize', prop);
        end
    end
end
