classdef (Hidden, HandleCompatible) UnitDisplayer < matlab.mixin.CustomDisplay
    %   This class is used to display units.
    %
    %   This class is for internal use only. It may be removed in the future.
    
    %   Copyright 2017-2019 The MathWorks, Inc.
    
    
    methods (Abstract, Access = protected)
        groups = getPropertyGroups(obj);
    end

    methods (Access = protected)
        function displayScalarObjectWithUnits(obj)
            % Print the header
            hdr = getHeader(obj);
            fprintf('%s\n',hdr);   
            
            % For the body, get the groups, formatting and loop through
            % each group:
            
            groups = getPropertyGroups(obj);
            % groups.PropertyList can be a struct or cell array. Reformat:
            propGroups = parseGroups(groups);
            
            isLoose = strcmp(matlab.internal.display.formatSpacing, 'loose');
            
            % Loop through the groups and display each.
            for n = 1:numel(groups)
                printGroup(obj, propGroups(n));
                
                % If format is "LOOSE", add a return after each group
                if isLoose
                     fprintf('\n');
                end
            end
        end
    end

    methods (Access = private)
        function printGroup(obj, propGroup)
            % PRINTGROUP print a single property group propGroup
            %
            % propGroup is a struct with fields:
            %   Title  - the property group title
            %   Names  - a cell array of property names in this group.
           
            propNames = propGroup.Names;
            numProps = numel(propNames);
            
            namesToDisp = cell(numProps,1);
            valsToDisp  = cell(numProps,1);
            [unitsToDisp{1:numProps,1}] = deal('');
           
            % Loop through each property in this propGroup and gather
            % name, value and units.
            for propIdx = 1:numProps
                
                thisprop = propNames{propIdx};
                namesToDisp{propIdx,:} = thisprop;
                
                valueOfProp = obj.(thisprop);
                if ~ (isa(valueOfProp, 'char') || isa(valueOfProp, 'string'))
                    valsToDisp{propIdx,:} = numericVal2Str(valueOfProp);
                    unitsToDisp{propIdx,:} = propunits(obj, thisprop);
                else  % a character or string
                    valsToDisp{propIdx,:} = ['''' char(valueOfProp) ''''];
                end
            end
            
            % Convert cell arrays to justified display strings and concatenate.
            propInfo = formatPropGroup(namesToDisp, valsToDisp, unitsToDisp);
            
            % Print the section title, if it's not empty
            if ~isempty(propGroup.Title)
                fprintf('   %s\n',propGroup.Title);
            end
            
            % Print the properties, values and bodies for this group
            fprintf('    %s\n',string(propInfo));

        end
        
        function str = propunits(obj, propName)
            % PROPUNITS - get this units char array for property propName
            if isprop(obj,[propName 'Units'])
                str = obj.([propName 'Units']);
            else
                str = '';   % no units supplied
            end
        end        
    end
    
    methods (Static, Hidden)
        function s = squaredSym()
            s = char(178);
        end
        
        function s = uTSym()
            s = [char(181) 'T'];
        end
    end
    
end

function str = numericVal2Str(val)
%NUMERICVAL2STR convert val to a string, properly formatted.

fmt = matlabshared.rotations.internal.getFloatFormat(class(val));

if isscalar(val)
    if isnumeric(val)
        str = num2str(val, fmt);
    else
        str = strtrim(evalc('disp(val)'));
    end
elseif all( [1 3] == size(val) ) || all( [1 2] == size(val) ) %special case 1x3 and 1x2
    propStr = num2str(val, fmt);
    str = [ '[' strjoin(strsplit(propStr),' ') ']' ];
    
else  %too big to display. Just show size and type like a struct
    sz = size(val);
    szStr = "[" + sz(1) + join(compose("x%d",sz(2:end)),"");
    str = char(compose("%s %s]",szStr, class(val)));
    
end
end % numericVal2Str

function propInfo = formatPropGroup(propNamesCellStr, propValsCellStr, propUnitsCellStr)
%FORMATPROPGROUP - format the body of a property group
propNames = strjust(char(propNamesCellStr(:)));
propVals = char(propValsCellStr(:));
propUnits = char(propUnitsCellStr(:));
propInfo = [propNames, repmat(': ',size(propNames,1),1), propVals, ...
    repmat('    ',size(propNames,1),1), propUnits];
end

function propGroup = parseGroups(groups)
% PARSEGROUPS - Convert groups to a struct array (1 element per group)
%   'Title' - title of section
%   'Names' - cell array of names of properties in this group

numGroups = numel(groups);
titles = cell(1,numGroups);
names = cell(1, numGroups);
for ii=1:numGroups
    titles{ii} = groups(ii).Title;
    pl = groups(ii).PropertyList;
    if isstruct(pl)
        names{ii} = fieldnames(pl);
    else % cell array
        names{ii} = pl;
    end
end
propGroup = struct('Title', titles, 'Names', names);

end % formatPropGroup
