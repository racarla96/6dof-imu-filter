%This function is for internal use only. It may be removed in the future.

%WGS84MODELPARAMS World Geodetic System 1984 Model parameters
%   [a, f] = fusion.internal.frames.wgs84ModelParams() returns the semimajor axis 
%   a and the flattening f of the World Geodetic System 1984 Model.

%   Copyright 2017-2019 The MathWorks, Inc.

% internal function, no error checking is performed

%#codegen

function [a, f] = wgs84ModelParams(varargin)
dataType = 'double';
if numel(varargin) == 1
    dataType = varargin{1};
end

% flattening
f  = cast(1/298.257223563, dataType);

% semimajor axis (meters)
a =  cast(6378137, dataType);
end
