function [q, aVelocity, aAcceleration, aJerk] = getOrientationState(t, x, y, h, dtheta, e, w)
%FUSION.SCENARIO.INTERNAL.GETORIENTATIONSTATE interpolates a cubic quaternion spline
%  Inputs
%     t             desired output time
%     x             input vector of time values
%     y             input vector of quaternion values.
%     h             vector of time interval values.
%     dtheta        vector of rotation angles.
%     e             array of rotation axis vectors.
%     w             intermediate angular rate values.
%
%  Outputs
%     q             interpolated quaternion value.
%     aVelocity     interpolated angular rate (rad/sec).
%     aAcceleration interpolated angular acceleration (rad/sec^2).
%     aJerk         interpolated angular jerk (rad/sec^3).

% Adapted from:  James McEnnan, qspline
%          ( http://sourceforge.net/projects/qspline-cc0 )

%    Copyright 2017-2019 The MathWorks, Inc.

%#codegen

assert(isscalar(t));

% look up index into time table (x)
% i = discretize(t,x);
i = nnz(x(1:end-1) <= t);

% fetch work matrices
[A,B,C,D] = slew3_init(h(i),dtheta(i),e(i,:),w(i,:),w(i + 1,:));

% fetch orientation and angular measures
[q,aVelocity,aAcceleration,aJerk] = slew3(t-x(i),h(i),y(i,:),A,B,C,D);


function [aa,bb,cc,dd] = slew3_init(dt,dtheta,e,wi,wf)
% Subroutine slew3_init computes the coefficients for a third-order polynomial
% interpolation function describing a slew between the input initial and
% final states.
% 
% dt            i      slew time (sec).
% dtheta        i      slew angle (rad).
% e             i      unit vector along slew eigen-axis.
% wi            i      initial body angular rate (rad/sec).
% wf            i      final body angular rate (rad/sec).

aa = zeros(3,3);
bb = zeros(3,3);
cc = zeros(2,3);
dd = zeros(1,3);

if dt <= 0.0
    return
end

sa = sin(dtheta);
ca = cos(dtheta);

% final angular rate terms.
EPS = 1e-6;

if dtheta > EPS
    c1 = 0.5 * sa * dtheta / (1.0 - ca);
    c2 = 0.5 * dtheta;
    b0 = dot(e,wf);
    bvec2 = cross(e,wf);
    bvec1 = cross(bvec2,e);
    bvec = b0*e + c1*bvec1 + c2*bvec2;
else
    bvec = wf;
end
  
%  compute coefficients. 
bb(1,:) = wi;

aa(3,:) = e*dtheta;
bb(3,:) = bvec;

aa(1,:) = bb(1,:)*dt;
aa(2,:) = bb(3,:)*dt - 3*aa(3,:);

bb(2,:) = (2.0*aa(1,:) + 2.0*aa(2,:))/dt;
cc(1,:) = (2.0*bb(1,:) +     bb(2,:))/dt;
cc(2,:) = (    bb(2,:) + 2.0*bb(3,:))/dt;

dd(:)   = (    cc(1,:) +     cc(2,:))/dt;


function [q,angVel,angAcc,angJerk] = slew3(t,dt,qi,a,b,c,d)
% Subroutine slew3 computes the quaternion, body angular rate, acceleration and
% jerk as a function of time corresponding to a third-order polynomial
% interpolation function describing a slew between initial and final states.
% 
% t             i      current time (seconds from start).
% dt            i      slew time (sec).
% qi            i      initial attitude quaternion.
% q             o      current attitude quaternion.
% angVel        o      current body angular rate (rad/sec).
% angAcc        o      current body angular acceleration (rad/sec^2).
% angJerk       o      current body angular jerk (rad/sec^3).

EPS = 1e-6;

q = quaternion.zeros;
angVel = zeros(1,3);
angAcc = zeros(1,3);
angJerk = zeros(1,3);
    
if dt <= 0.0
  return
end

x = t/dt;

x1 = zeros(1,2);
x1(1) = x - 1.0;
x1(2) = x1(1)*x1(1);

th0 = ((x*a(3,:) + x1(1)*a(2,:))*x + x1(2)*a(1,:))*x;
th1 = (x*b(3,:) + x1(1)*b(2,:))*x + x1(2)*b(1,:);
th2 = x*c(2,:) + x1(1)*c(1,:);
th3 = d(1,:);

deltaQ = quaternion(th0, 'rotvec');
q = qi * deltaQ;

[ang,u] = unvec(th0);

ca = cos(ang);
sa = sin(ang);

if ang > EPS
    % compute angular rate vector.

    temp1 = cross(u,th1);

    w = temp1/ang;

    udot = cross(w,u);

    thd1 = dot(u,th1);

    angVel =  thd1*u + sa*udot - (1.0 - ca)*w;

    % compute angular acceleration vector.

    thd2 = udot(1)*th1(1) + udot(2)*th1(2) + udot(3)*th1(3) + ...
              u(1)*th2(1) +    u(2)*th2(2) +    u(3)*th2(3);

    temp1 = cross(u,th2);

    wd1 = (temp1 - 2.0*thd1*w)./ang;

    wd1xu = cross(wd1,u);

    temp0 = thd1*u - w;
    temp1 = cross(angVel,temp0);

    angAcc = thd2*u + sa*wd1xu - (1.0 - ca)*wd1 + thd1*udot + temp1;

    % compute angular jerk vector.

    w2 = w(1)*w(1) + w(2)*w(2) + w(3)*w(3);

    thd3 = wd1xu(1)*th1(1) + wd1xu(2)*th1(2) + wd1xu(3)*th1(3) - ...
           w2*(u(1)*th1(1) + u(2)*th1(2) + u(3)*th1(3)) + ...
           2.0*(udot(1)*th2(1) + udot(2)*th2(2) + udot(3)*th2(3)) + ...
           u(1)*th3(1) + u(2)*th3(2) + u(3)*th3(3);

    temp1 = cross(th1,th2) ./ ang;
    temp2 = cross(u,th3);

    td2 = (th1(1)*th1(1) + th1(2)*th1(2) + th1(3)*th1(3))/ang;

    ut2 = u(1)*th2(1) + u(2)*th2(2) + u(3)*th2(3);

    wwd = w(1)*wd1(1) + w(2)*wd1(2) + w(3)*wd1(3);

    wd2 = (temp1 + temp2 - 2.0*(td2 + ut2)*w - 4.0*thd1*wd1)/ang;

    wd2xu = cross(wd2,u);

    temp2 = thd2*u + thd1*udot - wd1;

    temp1 = cross(angVel,temp2);
    temp2 = cross(angAcc,temp0);

    angJerk = thd3*u + sa*wd2xu - (1.0 - ca)*wd2 + 2.0*thd2*udot ...
                     + thd1*((1.0 + ca)*wd1xu - w2*u - sa*wd1) ...
                     - wwd*sa*u + temp1 + temp2;
else
    temp1 = cross(th1,th2);
    angVel = th1;
    angAcc = th2;
    angJerk = th3 - 0.5*temp1;
end

function [amag,au] = unvec(a)
%UNVEC unitizes a vector, a, and computes its magnitude.

validateattributes(a,{'double'},{'size',[1 3]});
amag = norm(a);

if amag > 0.0
    au = a ./ amag;
else
    au = zeros(1,3);
end
