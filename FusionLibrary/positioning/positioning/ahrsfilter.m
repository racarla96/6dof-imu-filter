classdef ahrsfilter <  fusion.internal.AHRSFilterBase & fusion.internal.UnitDisplayer
%AHRSFILTER Orientation from accelerometer, magnetometer, and gyroscope
%
%   FUSE = AHRSFILTER returns an indirect Kalman filter System object,
%   FUSE, for fusion of accelerometer, gyroscope, and magnetometer
%   data to estimate device orientation. The filter uses a 12 element
%   state vector to track the error in the device orientation estimate
%   (as a rotation vector), the error in the gyroscope bias estimate, the
%   error in the linear acceleration estimate, and the magnetic
%   disturbance error.
%
%   FUSE = AHRSFILTER('ReferenceFrame', RF) returns an AHRSFILTER System 
%   object that fuses accelerometer, gyroscope, and magnetometer data to 
%   estimate device orientation relative to the reference frame RF. Specify
%   the reference frame as 'NED' (North-East-Down) or 'ENU' 
%   (East-North-Up). The default value is 'NED'.
%
%   FUSE = AHRSFILTER('PropertyName', PropertyValue, ...) returns
%   an indirect Kalman sensor fusion filter, FUSE, with each specified
%   property set to a specified value.
%
%   Step method syntax:
%
%   [ORNT, AV] = step(FUSE, ACC, GYRO, MAG) fuses the accelerometer data
%   ACC, gyroscope data GYRO, and magnetometer data MAG,  to compute device
%   orientation ORNT and angular velocity AV. The inputs are:
%       ACC     - N-by-3 array of accelerometer readings in m/s^2
%       GYRO    - N-by-3 array of gyroscope readings in rad/s
%       MAG     - N-by-3 array of magnetometer readings in uT
%   where N is the number of samples. The three columns of each input
%   array represent the [X Y Z] measurements. The outputs are:
%       ORNT    - an M-by-1 array of orientation quaternions that can be
%                 used to rotate quantities in the global frame of
%                 reference to the sensor frame of reference.
%       AV      - an M-by-3 array of angular velocity measurements in rad/s
%                 in the sensor's frame of reference, with the gyroscope
%                 bias removed.
%   M is determined by N and the DecimationFactor property.
%
%   System objects may be called directly like a function instead of
%   using the step method. For example, y = step(obj, a, g, m) and y =
%   obj(a, g, m) are equivalent.
%
%   AHRSFILTER methods:
%
%   step                - See above description for use of this method
%   release             - Allow changes to non-tunable properties
%                         values and input characteristics
%   clone               - Create an AHRSFILTER object with the same
%                         property values and internal states
%   isLocked            - Locked status (logical)
%   reset               - Reset the internal states to initial
%                         conditions
%
%   AHRSFILTER properties:
%   
%   SampleRate                     - Sample rate of data from sensor
%   DecimationFactor               - Decimation factor
%   AccelerometerNoise             - Noise in accelerometer signal
%   MagnetometerNoise              - Noise in magnetometer signal
%   GyroscopeNoise                 - Noise in the gyroscope signal
%   GyroscopeDriftNoise            - Gyroscope bias drift noise
%   LinearAccelerationNoise        - Linear acceleration noise variance
%   LinearAccelerationDecayFactor  - Linear acceleration noise decay factor 
%   MagneticDisturbanceNoise       - Magnetic disturbance noise
%   MagneticDisturbanceDecayFactor - Magnetic Disturbance noise decay factor 
%   InitialProcessNoise            - Initial process covariance matrix
%   ExpectedMagneticFieldStrength  - Earth's magnetic field strength 
%   OrientationFormat              - Quaternion or rotation matrix
%
%   % EXAMPLE: Estimate orientation from recorded IMU data.
%   
%   %  The data in rpy_9axis.mat is recorded accelerometer, gyroscope
%   %  and magnetometer sensor data from a device oscillating in pitch
%   %  (around y-axis) then yaw (around z-axis) then roll (around
%   %  x-axis). The device's x-axis was pointing southward when
%   %  recorded. The IMUFILTER fusion ignores the MagneticField data
%   %  in the sensorData struct, and only uses the gyroscope and
%   %  accelerometer data in its fusion algorithm. The IMUFILTER
%   %  fusion algorithm correctly estimates the motion of the device
%   %  in roll and pitch. However, the algorithm assumes the device is
%   %  initially pointing northward (because it does not use the
%   %  magnetometer) and correctly computes relative change in yaw
%   %  from the starting yaw. To correctly estimate the orientation
%   %  with absolute yaw use the AHRSFILTER, which requires
%   %  magnetometer data.
%
%   ld = load('rpy_9axis.mat');    
%   accel = ld.sensorData.Acceleration;
%   gyro = ld.sensorData.AngularVelocity;
%   mag = ld.sensorData.MagneticField;
%
%   Fs  = ld.Fs;  % Hz
%   decim = 2;    % Decimate by 2 to lower computational cost
%   fuse = ahrsfilter('SampleRate', Fs, 'DecimationFactor', decim);
%
%   % Fuse accelerometer, gyroscope, and magnetometer
%   q = fuse(accel, gyro, mag);
%       
%   % Plot Euler angles in degrees
%   plot( eulerd( q, 'ZYX', 'frame'));
%   title('Orientation Estimate');
%   legend('Z-rotation', 'Y-rotation', 'X-rotation');
%   ylabel('Degrees');
%
%   See also IMUFILTER, ECOMPASS, QUATERNION

%   Copyright 2017-2019 The MathWorks, Inc.   
    

    properties (Constant, Hidden)
        SampleRateUnits = 'Hz'
        AccelerometerNoiseUnits = ['(m/s' sqSym ')' sqSym];
        GyroscopeNoiseUnits = ['(rad/s)' sqSym];
        GyroscopeDriftNoiseUnits = ['(rad/s)' sqSym];
        LinearAccelerationNoiseUnits = ['(m/s' sqSym ')' sqSym];     
        MagneticDisturbanceNoiseUnits = [ '(' uTSym ')' sqSym];
        ExpectedMagneticFieldStrengthUnits = uTSym;
    end    

    methods
        function obj = ahrsfilter(varargin)
            setProperties(obj,nargin,varargin{:});
        end
    end 
    
    methods (Access = protected)
        function displayScalarObject(obj)
            displayScalarObjectWithUnits(obj);
        end
        function groups = getPropertyGroups(obj)
            list.SampleRate                     = obj.SampleRate;
            list.DecimationFactor               = obj.DecimationFactor;
            list.AccelerometerNoise             = obj.AccelerometerNoise;
            list.GyroscopeNoise                 = obj.GyroscopeNoise;
            list.MagnetometerNoise              = obj.MagnetometerNoise;
            list.GyroscopeDriftNoise            = obj.GyroscopeDriftNoise;
            list.LinearAccelerationNoise        = obj.LinearAccelerationNoise;
            list.MagneticDisturbanceNoise       = obj.MagneticDisturbanceNoise;
            list.LinearAccelerationDecayFactor  = obj.LinearAccelerationDecayFactor;
            list.MagneticDisturbanceDecayFactor = obj.MagneticDisturbanceDecayFactor;
            list.ExpectedMagneticFieldStrength  = obj.ExpectedMagneticFieldStrength;
            list.InitialProcessNoise            = obj.InitialProcessNoise;
            list.OrientationFormat              = obj.OrientationFormat;
            groups = matlab.mixin.util.PropertyGroup(list);
        end
    end
    methods (Hidden, Static)
        function name = matlabCodegenRedirect(~)
            name = 'fusion.internal.coder.ahrsfiltercg';
        end
        function flag = isAllowedInSystemBlock
            flag = false;
        end
    end
end

function s = sqSym()
    s = char(178);
end

function s = uTSym()
    s = [char(181) 'T'];
end
