classdef insSensor < fusion.internal.INSSENSORBase & fusion.internal.UnitDisplayer
%INSSENSOR INS/GPS position, velocity, and orientation emulator
%   INS = INSSENSOR returns a System object, INS, that models an inertial 
%   navigation and global position system reading based on an inertial 
%   input signal.
%
%   INS = INSSENSOR('Name', Value, ...) returns an INSSENSOR System object 
%   with each specified property name set to the specified value. You can 
%   specify additional name-value pair arguments in any order as 
%   (Name1,Value1,...,NameN, ValueN).
%   
%   Step method syntax:
%
%   INSMEAS = step(INS, MOTION) models an inertial navigation and global
%   positioning system reading, INSMEAS, from an inertial input MOTION. 
%
%   MOTION is a struct with the following fields:
% 
%        Position      Position of the INS in the local NED coordinate 
%                      system specified as a real finite N-by-3 array in 
%                      meters. N is the number of samples in the current 
%                      frame.
% 
%        Velocity      Velocity of the INS in the local NED coordinate 
%                      system specified as a real finite N-by-3 array in 
%                      meters per second. N is the number of samples in the
%                      current frame.
%
%       Orientation    Orientation of the INS with respect to the local NED
%                      coordinate system specified as a quaternion 
%                      N-element column vector or a single or double 
%                      3-3-N-element rotation matrix. Each quaternion or 
%                      rotation matrix is a frame rotation from the local 
%                      NED coordinate system to the current INS body 
%                      coordinate system. N is the number of samples in the
%                      current frame. 
%
%   INSMEAS is a struct with the following fields: 
%
%        Position      Position of the INS in the local NED coordinate 
%                      system specified as a real finite N-by-3 array in 
%                      meters. N is the number of samples in the current 
%                      frame.
% 
%        Velocity      Velocity of the INS in the local NED coordinate 
%                      system specified as a real finite N-by-3 array in 
%                      meters per second. N is the number of samples in the
%                      current frame.
%
%       Orientation    Orientation of the INS with respect to the local NED
%                      coordinate system specified as a quaternion 
%                      N-element column vector or a single or double 
%                      3-3-N-element rotation matrix. Each quaternion or 
%                      rotation matrix is a frame rotation from the local 
%                      NED coordinate system to the current INS body 
%                      coordinate system. N is the number of samples in the
%                      current frame. 
%
%   Either single or double datatypes are supported for the fields of the
%   input MOTION. The fields of the output INSMEAS have the same datatype
%   as the input.
%
%   System objects may be called directly like a function instead of using
%   the step method. For example, y = step(obj, x) and y = obj(x) are
%   equivalent.
%
%   INSSENSOR methods:
%
%   step        - See above description for use of this method
%   release     - Allow property value and input characteristics to change,
%                 and release INSSENSOR resources
%   clone       - Create INSSENSOR object with same property values
%   isLocked    - Display locked status (logical)
%   reset       - Reset the states of the INSSENSOR
%
%   INSSENSOR properties:
%
%   RollAccuracy        - Roll accuracy (deg)
%   PitchAccuracy       - Pitch accuracy (deg)
%   YawAccuracy         - Yaw accuracy (deg)
%   PositionAccuracy    - Position accuracy (m)
%   VelocityAccuracy    - Velocity accuracy (m/s)
%   RandomStream        - Source of random number stream 
%   Seed                - Initial seed of mt19937ar random number 
%   
%   % EXAMPLE 1: Generate INS measurements from stationary input.
%
%   Fs = 100;
%   numSamples = 1000;
%   t = 0:1/Fs:(numSamples-1)/Fs;
%
%   ins = insSensor;
%
%   motion = struct( ...
%       'Position', [0 0 0], ...
%       'Velocity', [0 0 0], ...
%       'Orientation', quaternion(1, 0, 0, 0));
%
%   loggedData = struct( ...
%       'Position', zeros(numSamples, 3), ...
%       'Velocity', zeros(numSamples, 3), ...
%       'Orientation', quaternion.zeros(numSamples, 1));
%
%   for i = 1:numSamples
%       insMeas = ins(motion);
%       loggedData.Position(i, :) = insMeas.Position;
%       loggedData.Velocity(i, :) = insMeas.Velocity;
%       loggedData.Orientation(i, :) = insMeas.Orientation;
%   end
%
%   subplot(3, 1, 1)
%   plot(t, loggedData.Position)
%   title('Position')
%   xlabel('s')
%   ylabel('m')
%   legend('N', 'E', 'D')
%
%   subplot(3, 1, 2)
%   plot(t, loggedData.Velocity)
%   title('Velocity')
%   xlabel('s')
%   ylabel('m/s')
%   legend('N', 'E', 'D')
%
%   subplot(3, 1, 3)
%   eulerAngs = eulerd(loggedData.Orientation, 'ZYX', 'frame');
%   plot(t, eulerAngs)
%   title('Orientation')
%   xlabel('s')
%   ylabel('degrees')
%   legend('Roll', 'Pitch', 'Yaw')
%
%   See also IMUSENSOR, GPSSENSOR

%   Copyright 2017-2019 The MathWorks, Inc.

%#codegen
    
    properties (Constant, Hidden)
        RollAccuracyUnits     = 'deg';
        PitchAccuracyUnits    = 'deg';
        YawAccuracyUnits      = 'deg';
        PositionAccuracyUnits = 'm';
        VelocityAccuracyUnits = 'm/s';     
    end 
    
    methods
        function obj = insSensor(varargin)
            setProperties(obj, nargin, varargin{:});
        end
    end
    methods (Access = protected)
        function displayScalarObject(obj)
            displayScalarObjectWithUnits(obj);
        end
        function groups = getPropertyGroups(obj)
            list.RollAccuracy = obj.RollAccuracy;
            list.PitchAccuracy = obj.PitchAccuracy;
            list.YawAccuracy = obj.YawAccuracy;
            list.PositionAccuracy = obj.PositionAccuracy;
            list.VelocityAccuracy = obj.VelocityAccuracy;
            list.RandomStream = obj.RandomStream;
            if ~isInactiveProperty(obj, 'Seed')
                list.Seed = obj.Seed;
            end
            groups = matlab.mixin.util.PropertyGroup(list);
        end
    end
    methods (Hidden, Static)
        function name = matlabCodegenRedirect(~)
            name = 'fusion.internal.coder.insSensorCG';
        end
        function flag = isAllowedInSystemBlock
            flag = false;
        end
    end
end

