classdef insfilterErrorState < fusion.internal.ErrorStateIMUGPSFuserBase & fusion.internal.UnitDisplayer
%INSFILTERERRORSTATE Estimate pose using error state filter
%   FUSE = INSFILTERERRORSTATE returns an object, FUSE, that estimates pose
%   in the navigation reference frame from IMU, GPS, and monocular visual
%   odometry (MVO) data. The filter uses a 17-element state vector to track
%   the orientation quaternion, position, velocity, the IMU sensor biases,
%   and the MVO scaling factor. The FUSE object uses an error-state Kalman
%   filter to estimate these quantities.
%
%   FUSE = INSFILTERERRORSTATE('ReferenceFrame', RF) returns an inertial
%   navigation filter that estimates pose relative to the reference frame
%   RF. Specify the reference frame as 'NED' (North-East-Down) or 'ENU'
%   (East-North-Up). The default value is 'NED'.
%
%   INSFILTERERRORSTATE methods:
%
%   predict      - Update states using accelerometer and gyroscope 
%                  measurements
%   fusegps      - Correct states using GPS
%   fusemvo      - Correct states using monocular visual odometry
%   correct      - Correct states using direct state measurements
%   pose         - Current position, orientation, and velocity estimate
%   reset        - Set state and state estimation error covariance to 
%                  default values
%   stateinfo    - Display state vector information
%
%   INSFILTERERRORSTATE properties:
%
%   IMUSampleRate             - IMU sampling rate (Hz)
%   ReferenceLocation         - Origin of local NED reference frame
%   State                     - State vector
%   StateCovariance           - State estimation error covariance
%   GyroscopeNoise            - Noise in the gyroscope signal (rad/s)^2
%   AccelerometerNoise        - Noise in the accelerometer signal (m/s^2)^2
%   GyroscopeBiasNoise        - Gyroscope bias drift noise (rad/s)^2
%   AccelerometerBiasNoise    - Accelerometer bias drift noise (m/s^2)^2
%
%   Example:
%   % Estimate the pose of a ground vehicle.
%   
%   % Load logged data of a ground vehicle following a circular trajectory.
%   % The .mat file contains IMU and GPS sensor measurements and ground 
%   % truth orientation and position.
%   load('loggedGroundVehicleCircle.mat', ...
%       'imuFs', 'localOrigin', ...
%       'initialStateCovariance', ...
%       'accelData', 'gyroData', ...
%       'gpsFs', 'gpsLLA', 'Rpos', 'gpsVel', 'Rvel', ...
%       'trueOrient', 'truePos');
%   
%   % Initialize filter.
%   initialState = [compact(trueOrient(1)), truePos(1,:), ...
%       -6.8e-3, 2.5002, 0, zeros(1,6), 1].';
%   filt = insfilterErrorState('IMUSampleRate', imuFs, ...
%       'ReferenceLocation', localOrigin, 'State', initialState, ...
%       'StateCovariance', initialStateCovariance);
%   
%   imuSamplesPerGPS = imuFs / gpsFs;
%   
%   % Log data for final metric computation.
%   numIMUSamples = size(accelData, 1);
%   estOrient = quaternion.ones(numIMUSamples, 1);
%   estPos = zeros(numIMUSamples, 3);
%   
%   gpsIdx = 1;
%   for idx = 1:numIMUSamples
%       % Use the predict method to estimate the filter state based on the
%       % accelData and gyroData arrays.
%       predict(filt, accelData(idx,:), gyroData(idx,:));
%       
%       if (mod(idx, imuSamplesPerGPS) == 0)
%           % Correct the filter states based on the GPS data.
%           fusegps(filt, gpsLLA(gpsIdx,:), Rpos, gpsVel(gpsIdx,:), Rvel);
%           gpsIdx = gpsIdx + 1;
%       end
%       % Log estimated pose.
%       [estPos(idx,:), estOrient(idx,:)] = pose(filt);
%   end
%   
%   % Calculate RMS errors.
%   posd = estPos - truePos;
%   quatd = rad2deg(dist(estOrient, trueOrient));
%   
%   % Display RMS errors.
%   fprintf('Position RMS Error\n');
%   msep = sqrt(mean(posd.^2));
%   fprintf('\tX: %.2f , Y: %.2f, Z: %.2f (meters)\n\n', msep(1), ...
%       msep(2), msep(3));
%   
%   fprintf('Quaternion Distance RMS Error\n');
%   fprintf('\t%.2f (degrees)\n\n', sqrt(mean(quatd.^2)));
%
%   See also INSFILTERMARG, INSFILTERASYNC, INSFILTERNONHOLONOMIC

%   Copyright 2018-2019 The MathWorks, Inc.

%#codegen

    properties (Constant, Hidden)
        IMUSampleRateUnits = 'Hz';
        ReferenceLocationUnits = '[deg deg m]';
        AccelerometerNoiseUnits = ['(m/s', squared, ')', squared];
        AccelerometerBiasNoiseUnits = ['(m/s', squared, ')', squared];
        GyroscopeNoiseUnits = ['(rad/s)', squared];
        GyroscopeBiasNoiseUnits = ['(rad/s)', squared];
    end

    methods
        function obj = insfilterErrorState(varargin)
            obj@fusion.internal.ErrorStateIMUGPSFuserBase(varargin{:});
        end
    end
    
    methods
        function stateinfo(~)
        %STATEINFO Display state vector information
        %   STATEINFO(FUSE) displays the meaning of each index of the State 
        %   property and the associated units. 
        
            stateCellArr = {'States', 'Orientation (quaternion parts)', ...
                'Position (NAV)', ...
                'Velocity (NAV)', ...
                'Gyroscope Bias (XYZ)', 'Accelerometer Bias (XYZ)', ...
                'Visual Odometry Scale'};
            unitCellArr = {'Units', '', 'm', 'm/s', 'rad/s', acceleration, ''};
            indexCellArr = {'Index', '1:4', '5:7', '8:10', '11:13', '14:16', '17'};
            
            states = char(stateCellArr(:));
            units = char(unitCellArr(:));
            indices = char(indexCellArr(:));
            spaces = repmat('    ',size(states, 1), 1);
            infoStr = [states, spaces, units, spaces, indices];
            
            fprintf('\n');
            for i = 1:size(infoStr, 1)
                fprintf(infoStr(i,:));
                fprintf('\n');
            end
            fprintf('\n');
        end
    end
    
    methods (Access = protected)
        function displayScalarObject(obj)
            displayScalarObjectWithUnits(obj);
        end
        
        function groups = getPropertyGroups(obj)
            if ~isscalar(obj)
                groups = getPropertyGroups@matlab.mixin.CustomDisplay(obj);
            else
                basicList.IMUSampleRate = obj.IMUSampleRate;
                basicList.ReferenceLocation = obj.ReferenceLocation;
                basicList.State = obj.State;
                basicList.StateCovariance = obj.StateCovariance;
                
                procNoiseList.GyroscopeNoise = obj.GyroscopeNoise;
                procNoiseList.AccelerometerNoise = obj.AccelerometerNoise;
                procNoiseList.GyroscopeBiasNoise = obj.GyroscopeBiasNoise;
                procNoiseList.AccelerometerBiasNoise = obj.AccelerometerBiasNoise;
                
                basicGroup = matlab.mixin.util.PropertyGroup(basicList);
                procNoiseGroup = matlab.mixin.util.PropertyGroup( ...
                    procNoiseList, 'Process Noise Variances');
                
                groups = [basicGroup procNoiseGroup];
            end
        end
    end
    
    methods (Hidden, Static)
        function name = matlabCodegenRedirect(~)
            name = 'fusion.internal.coder.insfilterErrorStateCG';
        end
    end
end

function str = squared
str = char(178);
end

function s = acceleration
s = ['m/s', squared];
end
