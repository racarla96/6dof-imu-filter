classdef insfilterNonholonomic < ...
        fusion.internal.NHConstrainedIMUGPSFuserBase ...
        & fusion.internal.UnitDisplayer
%INSFILTERNONHOLONOMIC Pose estimation with nonholonomic constraints
%   FUSE = INSFILTERNONHOLONOMIC returns an object, FUSE, that estimates
%   pose in the navigation reference frame from IMU and GPS data. The
%   filter uses a 16-element state vector to track the orientation
%   quaternion, position, velocity, and IMU sensor biases. The FUSE object
%   uses an extended Kalman filter to estimate these quantities.
%
%   FUSE = INSFILTERNONHOLONOMIC('ReferenceFrame', RF) returns an inertial
%   navigation filter that estimates pose relative to the reference frame
%   RF. Specify the reference frame as 'NED' (North-East-Down) or 'ENU'
%   (East-North-Up). The default value is 'NED'.
%
%   INSFILTERNONHOLONOMIC methods:
%
%   predict      - Update states using accelerometer and gyroscope 
%                  measurements
%   fusegps      - Correct states using GPS
%   correct      - Correct states using direct state measurements
%   pose         - Current position, orientation, and velocity estimate
%   reset        - Set state and state estimation error covariance to 
%                  default values
%   stateinfo    - Display state vector information
%
%   INSFILTERNONHOLONOMIC properties:
%
%   IMUSampleRate                   - IMU sampling rate (Hz)
%   ReferenceLocation               - Origin of local NED reference frame
%   DecimationFactor                - Decimation factor
%   State                           - State vector
%   StateCovariance                 - State estimation error covariance
%   GyroscopeNoise                  - Noise in the gyroscope signal 
%                                     (rad/s)^2
%   AccelerometerNoise              - Noise in the accelerometer signal 
%                                     (m/s^2)^2
%   GyroscopeBiasNoise              - Gyroscope bias drift noise (rad/s)^2
%   GyroscopeBiasDecayFactor        - Gyroscope bias noise decay factor
%   AccelerometerBiasNoise          - Accelerometer bias drift noise
%                                     (m/s^2)^2
%   AccelerometerBiasDecayFactor    - Accelerometer bias noise decay factor
%   ZeroVelocityConstraintNoise     - Velocity constraints noise (m/s)^2
%
%   % EXAMPLE: Estimate the pose of a ground vehicle.
%   
%   % Load logged data of a ground vehicle following a circular trajectory.
%   % The .mat file contains IMU and GPS sensor measurements and ground 
%   % truth orientation and position.
%   load('loggedGroundVehicleCircle.mat', ...
%       'imuFs', 'localOrigin', ...
%       'initialState', 'initialStateCovariance', ...
%       'accelData', 'gyroData', ...
%       'gpsFs', 'gpsLLA', 'Rpos', 'gpsVel', 'Rvel', ...
%       'trueOrient', 'truePos');
%   
%   % Initialize filter.
%   filt = insfilterNonholonomic('IMUSampleRate', imuFs, ...
%       'ReferenceLocation', localOrigin, 'State', initialState, ...
%       'StateCovariance', initialStateCovariance);
%   
%   imuSamplesPerGPS = imuFs / gpsFs;
%   
%   % Log data for final metric computation.
%   numIMUSamples = size(accelData, 1);
%   estOrient = quaternion.ones(numIMUSamples, 1);
%   estPos = zeros(numIMUSamples, 3);
%   
%   gpsIdx = 1;
%   for idx = 1:numIMUSamples
%       % Use the predict method to estimate the filter state based on the
%       % accelData and gyroData arrays.
%       predict(filt, accelData(idx,:), gyroData(idx,:));
%       
%       if (mod(idx, imuSamplesPerGPS) == 0)
%           % Correct the filter states based on the GPS data.
%           fusegps(filt, gpsLLA(gpsIdx,:), Rpos, gpsVel(gpsIdx,:), Rvel);
%           gpsIdx = gpsIdx + 1;
%       end
%       % Log estimated pose.
%       [estPos(idx,:), estOrient(idx,:)] = pose(filt);
%   end
%   
%   % Calculate RMS errors.
%   posd = estPos - truePos;
%   quatd = rad2deg(dist(estOrient, trueOrient));
%   
%   % Display RMS errors.
%   fprintf('Position RMS Error\n');
%   msep = sqrt(mean(posd.^2));
%   fprintf('\tX: %.2f , Y: %.2f, Z: %.2f (meters)\n\n', msep(1), ...
%       msep(2), msep(3));
%   
%   fprintf('Quaternion Distance RMS Error\n');
%   fprintf('\t%.2f (degrees)\n\n', sqrt(mean(quatd.^2)));
%
%   See also INSFILTERMARG, INSFILTERASYNC, INSFILTERERRORSTATE

%   Copyright 2018-2019 The MathWorks, Inc.

%#codegen
    
    properties (Constant, Hidden)
        IMUSampleRateUnits = 'Hz';
        ReferenceLocationUnits = '[deg deg m]';
        GyroscopeNoiseUnits = ['(rad/s)', squared];
        AccelerometerNoiseUnits = ['(', acceleration, ')', squared];
        GyroscopeBiasNoiseUnits = ['(rad/s)', squared];
        AccelerometerBiasNoiseUnits = ['(', acceleration, ')', squared];
        ZeroVelocityConstraintNoiseUnits = ['(m/s)', squared];
    end
    
    methods
        function obj = insfilterNonholonomic(varargin)
            obj@fusion.internal.NHConstrainedIMUGPSFuserBase(varargin{:});
        end
        
        function stateinfo(~)
        %STATEINFO Display state vector information
        %   STATEINFO(FUSE) displays the meaning of each index of the State 
        %   property and the associated units. 

            stateCellArr = {'States', 'Orientation (quaternion parts)', ...
                'Gyroscope Bias (XYZ)', 'Position (NAV)', ...
                'Velocity (NAV)', 'Accelerometer Bias (XYZ)'};
            unitCellArr = {'Units', '', 'rad/s', 'm', 'm/s', acceleration};
            indexCellArr = {'Index', '1:4', '5:7', '8:10', '11:13', '14:16'};
            
            states = char(stateCellArr(:));
            units = char(unitCellArr(:));
            indices = char(indexCellArr(:));
            spaces = repmat('    ',size(states, 1), 1);
            infoStr = [states, spaces, units, spaces, indices];
            
            fprintf('\n');
            for i = 1:size(infoStr, 1)
                fprintf(infoStr(i,:));
                fprintf('\n');
            end
            fprintf('\n');
        end
    end
    
    methods (Access = protected)
        function displayScalarObject(obj)
            displayScalarObjectWithUnits(obj);
        end
        
        function groups = getPropertyGroups(obj)
            % Add section titles to property display.
            if ~isscalar(obj)
                groups = getPropertyGroups@matlab.mixin.CustomDisplay(obj);
            else
                basicList.IMUSampleRate = obj.IMUSampleRate;
                basicList.ReferenceLocation = obj.ReferenceLocation;
                basicList.DecimationFactor = obj.DecimationFactor;
                
                ekfValueList.State = obj.State;
                ekfValueList.StateCovariance = obj.StateCovariance;

                processNoiseList.GyroscopeNoise = obj.GyroscopeNoise;
                processNoiseList.AccelerometerNoise = obj.AccelerometerNoise;
                processNoiseList.GyroscopeBiasNoise = obj.GyroscopeBiasNoise;
                processNoiseList.GyroscopeBiasDecayFactor = obj.GyroscopeBiasDecayFactor;
                processNoiseList.AccelerometerBiasNoise = obj.AccelerometerBiasNoise;
                processNoiseList.AccelerometerBiasDecayFactor = obj.AccelerometerBiasDecayFactor;
                
                measNoiseList.ZeroVelocityConstraintNoise = obj.ZeroVelocityConstraintNoise;
                
                basicGroup = matlab.mixin.util.PropertyGroup(basicList);
                ekfValueGroup = matlab.mixin.util.PropertyGroup( ...
                    ekfValueList, 'Extended Kalman Filter Values');
                processNoiseGroup = matlab.mixin.util.PropertyGroup( ...
                    processNoiseList, 'Process Noise Variances');
                measNoiseGroup = matlab.mixin.util.PropertyGroup( ...
                    measNoiseList, 'Measurement Noise Variances');
                
                groups = [basicGroup, ekfValueGroup, processNoiseGroup, ...
                    measNoiseGroup];
            end
        end
    end
    
    methods (Hidden, Static)
        function name = matlabCodegenRedirect(~)
            name = 'fusion.internal.coder.insfilterNonholonomicCG';
        end
    end
end %classdef


function s = squared
s = char(178);
end

function s = acceleration
s = ['m/s', squared];
end
