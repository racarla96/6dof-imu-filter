classdef imufilter < fusion.internal.IMUFilterBase & fusion.internal.UnitDisplayer 
%IMUFILTER Orientation from accelerometer and gyroscope readings 
%
%   FUSE = IMUFILTER returns an indirect Kalman filter System object FUSE
%   for sensor fusion of accelerometer and gyroscope data to estimate
%   device orientation. The filter uses a 9 element state vector tracking
%   the error in the device orientation estimate (as a rotation vector),
%   the error in the gyroscope bias estimate, and the error in the
%   linear acceleration estimate. 
%
%   FUSE = IMUFILTER('ReferenceFrame', RF) returns an IMUFILTER System 
%   object that fuses accelerometer and gyroscope data to estimate device 
%   orientation relative to the reference frame RF. Specify the reference 
%   frame as 'NED' (North-East-Down) or 'ENU' (East-North-Up). The default 
%   value is 'NED'.
%
%   FUSE = IMUFILTER('PropertyName', PropertyValue, ...) returns
%   an indirect Kalman sensor fusion filter, FUSE, with each specified
%   property set to a specified value.
%
%   Step method syntax:
%
%   [ORNT, AV] = step(FUSE, ACC, GYRO) fuses the accelerometer data
%   ACC and gyroscope data GYRO, to compute device orientation ORNT and
%   angular velocity AV. The inputs are:
%       ACC     - N-by-3 array of accelerometer readings in m/s^2
%       GYRO    - N-by-3 array of gyroscope readings in rad/s
%   where N is the number of samples. The three columns of each input
%   array represent the [X Y Z] measurements. The outputs are:
%       ORNT    - an M-by-1 array of orientation quaternions that can be
%                 used to rotate quantities in the global frame of
%                 reference to the sensor frame of reference.
%       AV      - an M-by-3 array of angular velocity measurements in rad/s
%                 in the sensor's frame of reference, with the gyroscope
%                 bias removed.
%   M is determined by N and the DecimationFactor property.
%
%   System objects may be called directly like a function instead of
%   using the step method. For example, y = step(obj, a, g) and 
%   y = obj(a, g) are equivalent.
%
%   IMUFILTER methods:
%
%   step                - See above description for use of this method
%   release             - Allow changes to non-tunable property
%                         values and input characteristics
%   clone               - Create a IMUFILTER object with the same
%                         property values and internal states
%   isLocked            - Locked status (logical)
%   reset               - Reset the internal states to initial
%                         conditions
%
%   IMUFILTER properties:
%   
%   SampleRate                      - Input sample rate of the sensor 
%                                     data in Hz
%   DecimationFactor                - Decimation factor
%   AccelerometerNoise              - Accelerometer noise variance in
%                                     (m/s^2)^2
%   GyroscopeNoise                  - Gyroscope noise variance in
%                                     (rad/s)^2
%   GyroscopeDriftNoise             - Gyroscope bias noise variance in 
%                                     (rad/s)^2
%   LinearAccelerationNoise         - Linear acceleration noise 
%                                     variance in (m/s^2)^2
%   LinearAccelerationDecayFactor   - Linear acceleration noise decay 
%                                     factor 
%   InitialProcessNoise             - Initial process noise covariance 
%                                     matrix
%   OrientationFormat               - quaternion or rotation matrix
%
%   % EXAMPLE: Estimate orientation from recorded IMU data.
%
%   % The data in rpy_9axis.mat is recorded accelerometer, gyroscope
%   % and magnetometer sensor data from a device oscillating in pitch
%   % (around y-axis) then yaw (around z-axis) then roll (around
%   % x-axis). The device's x-axis was pointing southward when
%   % recorded. The AHRSFILTER fusion correctly estimates the
%   % orientation in the NED coordinate system. IMUFILTER fusion correctly
%   % estimates the change in orientation from an assumed north-facing initial
%   % orientation
%
%   ld = load('rpy_9axis.mat');    
%   accel = ld.sensorData.Acceleration;
%   gyro = ld.sensorData.AngularVelocity;
%
%   Fs  = ld.Fs;  % Hz
%   decim = 2;    % Decimate by 2 to lower computational cost
%   fuse = imufilter('SampleRate', Fs, 'DecimationFactor', decim);
%
%   % Fuse accelerometer and gyroscope
%   q = fuse(accel, gyro);
%       
%   % Plot Euler angles in degrees
%   plot( euler( q, 'ZYX', 'frame'));
%   title('Orientation Estimate');
%   legend('Z-rotation', 'Y-rotation', 'X-rotation');
%   ylabel('Degrees');
%
%   See also AHRSFILTER, ECOMPASS, QUATERNION

%   Copyright 2017-2019 The MathWorks, Inc.   

    properties (Constant, Hidden)
        SampleRateUnits = 'Hz'
        AccelerometerNoiseUnits = ['(m/s' char(178) ')' char(178)];
        GyroscopeNoiseUnits = ['(rad/s)' char(178)];
        GyroscopeDriftNoiseUnits = ['(rad/s)' char(178)];
        LinearAccelerationNoiseUnits = ['(m/s' char(178) ')' char(178)];     
    end    

    methods
        function obj = imufilter(varargin)
            setProperties(obj,nargin,varargin{:});
        end
    end 
    methods (Access = protected)
        function displayScalarObject(obj)
            displayScalarObjectWithUnits(obj);
        end
        function groups = getPropertyGroups(obj)
            list.SampleRate                         = obj.SampleRate;
            list.DecimationFactor                   = obj.DecimationFactor;
            list.AccelerometerNoise                 = obj.AccelerometerNoise;
            list.GyroscopeNoise                     = obj.GyroscopeNoise;
            list.GyroscopeDriftNoise                = obj.GyroscopeDriftNoise;
            list.LinearAccelerationNoise            = obj.LinearAccelerationNoise;
            list.LinearAccelerationDecayFactor      = obj.LinearAccelerationDecayFactor;
            list.InitialProcessNoise                = obj.InitialProcessNoise;
            list.OrientationFormat                  = obj.OrientationFormat;
            groups = matlab.mixin.util.PropertyGroup(list);
        end
    end
    methods (Hidden, Static)
        function name = matlabCodegenRedirect(~)
            name = 'fusion.internal.coder.imufiltercg';
        end
        function flag = isAllowedInSystemBlock
            flag = false;
        end
    end
end

