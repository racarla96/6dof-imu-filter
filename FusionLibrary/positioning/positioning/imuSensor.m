classdef imuSensor < fusion.internal.PositioningSystemBase
%IMUSENSOR IMU measurements of accelerometer, gyroscope, and magnetometer
%   IMU = IMUSENSOR returns a System object, IMU, that computes an inertial 
%   measurement unit reading based on an inertial input signal. The 
%   IMUSENSOR System object has an ideal accelerometer and gyroscope.
%
%   IMU = IMUSENSOR(TYPE) returns an IMUSENSOR System object with the
%   IMUType property set to TYPE.
%
%   IMU = IMUSENSOR('accel-gyro') returns an IMUSENSOR System object with 
%   an ideal accelerometer and gyroscope.
%
%   IMU = IMUSENSOR('accel-mag') returns an IMUSENSOR System object with an 
%   ideal accelerometer and magnetometer.
%   
%   IMU = IMUSENSOR('accel-gyro-mag') returns an IMUSENSOR System object 
%   with an ideal accelerometer, gyroscope, and magnetometer.
%
%   IMU = IMUSENSOR(..., 'ReferenceFrame', RF) returns an IMUSENSOR System
%   object that computes an inertial measurement unit reading relative to
%   the reference frame RF. Specify the reference frame as 'NED'
%   (North-East-Down) or 'ENU' (East-North-Up). The default value is 'NED'.
%
%   IMU = IMUSENSOR(..., 'Name', Value, ...) returns an IMUSENSOR System 
%   object with each specified property name set to the specified value. 
%   You can specify additional name-value pair arguments in any order as 
%   (Name1,Value1,...,NameN, ValueN).
%   
%   Step method syntax:
%
%   [ACCEL, GYRO] = step(IMU, ACC, ANGVEL) computes accelerometer and
%   gyroscope readings from the acceleration (ACC) and angular velocity
%   (ANGVEL) inputs. This syntax is only valid if IMUType is set to 
%   'accel-gyro' or 'accel-gyro-mag'.
%
%   [ACCEL, GYRO] = step(IMU, ACC, ANGVEL, ORIENTATION) computes
%   accelerometer and gyroscope readings from the acceleration (ACC),
%   angular velocity (ANGVEL), and orientation (ORIENTATION) inputs. This 
%   syntax is only valid if IMUType is set to 'accel-gyro' or 
%   'accel-gyro-mag'.
%
%   [ACCEL, MAG] = step(IMU, ACC, ANGVEL) computes accelerometer and
%   magnetometer readings from the acceleration (ACC) and angular velocity
%   (ANGVEL) inputs. This syntax is only valid if IMUType is set to
%   'accel-mag'.
%
%   [ACCEL, MAG] = step(IMU, ACC, ANGVEL, ORIENTATION) computes
%   accelerometer and magnetometer readings from the acceleration (ACC),
%   angular velocity (ANGVEL), and orientation (ORIENTATION) inputs. This
%   syntax is only valid if IMUType is set to 'accel-mag'.
%
%   [ACCEL, GYRO, MAG] = step(IMU, ACC, ANGVEL) computes accelerometer,
%   gyroscope, and magnetometer readings from the acceleration (ACC) and
%   angular velocity (ANGVEL) inputs. This syntax is only valid if IMUType
%   is set to 'accel-gyro-mag'.
%
%   [ACCEL, GYRO, MAG] = step(IMU, ACC, ANGVEL, ORIENTATION) computes
%   accelerometer, gyroscope, and magnetometer readings from the
%   acceleration (ACC), angular velocity (ANGVEL), and orientation
%   (ORIENTATION) inputs. This syntax is only valid if IMUType is set to
%   'accel-gyro-mag'.
%
%   The inputs to IMUSENSOR are defined as follows: 
%
%       ACC            Acceleration of the IMU in the local navigation 
%                      coordinate system specified as a real finite N-by-3
%                      array in meters per second squared. N is the number
%                      of samples in the current frame.
%
%       ANGVEL         Angular velocity of the IMU in the local navigation 
%                      coordinate system specified as a real finite N-by-3 
%                      array in radians per second. N is the number of 
%                      samples in the current frame.
%
%       ORIENTATION    Orientation of the IMU with respect to the local
%                      navigation coordinate system specified as a
%                      quaternion N-element column vector or a single or
%                      double 3-3-N-element rotation matrix. Each
%                      quaternion or rotation matrix is a frame rotation
%                      from the local navigation coordinate system to the
%                      current IMU body coordinate system. N is the number
%                      of samples in the current frame.
%
%   The outputs of IMUSENSOR are defined as follows: 
%
%       ACCEL          Accelerometer measurement of the IMU in the local 
%                      sensor body coordinate system specified as a real 
%                      finite N-by-3 array in meters per second squared. N 
%                      is the number of samples in the current frame. 
%
%       GYRO           Gyroscope measurement of the IMU in the local sensor
%                      body coordinate system specified as a real finite 
%                      N-by-3 array in radians per second. N is the number 
%                      of samples in the current frame. 
%
%       MAG            Magnetometer measurement of the IMU in the local 
%                      sensor body coordinate system specified as a real 
%                      finite N-by-3 array in microteslas. N is the number 
%                      of samples in the current frame. 
%
%   Either single or double datatypes are supported for the inputs to 
%   IMUSENSOR. Outputs have the same datatype as the input.
%
%   System objects may be called directly like a function instead of using
%   the step method. For example, y = step(obj, x) and y = obj(x) are
%   equivalent.
%
%   IMUSENSOR methods:
%
%   step             - See above description for use of this method
%   release          - Allow property value and input characteristics to 
%                      change, and release IMUSENSOR resources
%   clone            - Create IMUSENSOR object with same property values
%   isLocked         - Display locked status (logical)
%   reset            - Reset the states of the IMUSENSOR
%
%   IMUSENSOR properties:
%
%   IMUType          - Type of inertial measurement unit
%   SampleRate       - Sample rate of sensor (Hz)
%   Temperature      - Temperature of imu (degrees C)
%   MagneticField    - Magnetic field vector in the navigation frame (uT)
%   Accelerometer    - Accelerometer sensor parameters
%   Gyroscope        - Gyroscope sensor parameters
%   Magnetometer     - Magnetometer sensor parameters
%   RandomStream     - Source of random number stream 
%   Seed             - Initial seed of mt19937ar random number 
%   
%   % EXAMPLE 1: Generate ideal IMU data from stationary input. 
% 
%   Fs = 100;
%   numSamples = 1000;
%   t = 0:1/Fs:(numSamples-1)/Fs;
% 
%   imu = imuSensor('accel-gyro-mag', 'SampleRate', Fs);
%   
%   acc = zeros(numSamples, 3);
%   angvel = zeros(numSamples, 3);
%   
%   [accelMeas, gyroMeas, magMeas] = imu(acc, angvel);
% 
%   subplot(3, 1, 1)
%   plot(t, accelMeas)
%   title('Accelerometer')
%   xlabel('s')
%   ylabel('m/s^2')
%   legend('x','y','z')
%   
%   subplot(3, 1, 2)
%   plot(t, gyroMeas)
%   title('Gyroscope')
%   xlabel('s')
%   ylabel('rad/s')
%   legend('x','y','z')
%   
%   subplot(3, 1, 3)
%   plot(t, magMeas)
%   title('Magnetometer')
%   xlabel('s')
%   ylabel('uT')
%   legend('x','y','z')
% 
%   % EXAMPLE 2: Generate noisy IMU data from a spinning trajectory.
% 
%   % To determine if an orientation filter is affected by gimbal lock, 
%   % first create a spinning trajectory that passes through the 
%   % singularity and then generate noisy IMU data from it. 
% 
%   Fs = 100;
%   numSamples = 1000;
%   t = 0:1/Fs:(numSamples-1)/Fs;
%   
%   orientation = quaternion.zeros(numSamples, 1);
%   acc = zeros(numSamples, 3);
%   angvel = deg2rad([0 20 0]) .* ones(numSamples, 3);
% 
%   q = quaternion(1, 0, 0, 0);
%   for i = 1:numSamples
%       orientation(i) = q;
%       dq = quaternion(angvel(i,:) ./ Fs, 'rotvec');
%       q = q .* dq;
%   end
% 
%   imu = imuSensor('accel-gyro-mag', 'SampleRate', Fs);
% 
%   % Typical noise values for MEMS sensors. 
%   imu.Accelerometer.MeasurementRange = 156.96;
%   imu.Accelerometer.Resolution = 0.0048;
%   imu.Accelerometer.ConstantBias = 0.5886;
%   imu.Accelerometer.AxesMisalignment = 2;
%   imu.Accelerometer.NoiseDensity = 0.0029;
%   imu.Accelerometer.TemperatureBias = 0.0147;
%   imu.Accelerometer.TemperatureScaleFactor = 0.026;
% 
%   imu.Gyroscope.MeasurementRange = deg2rad(2000);
%   imu.Gyroscope.Resolution = deg2rad(1/16.4);
%   imu.Gyroscope.ConstantBias = deg2rad(5);
%   imu.Gyroscope.AxesMisalignment = 2;
%   imu.Gyroscope.NoiseDensity = deg2rad(0.01);
%   imu.Gyroscope.TemperatureBias = deg2rad(30/125);
%   imu.Gyroscope.TemperatureScaleFactor = 4/125;
% 
%   imu.Magnetometer.MeasurementRange = 4800;
%   imu.Magnetometer.Resolution = 0.6;
%   imu.Magnetometer.ConstantBias = 500*0.6;
%   
%   accelMeas = zeros(numSamples, 3);
%   gyroMeas = zeros(numSamples, 3);
%   magMeas = zeros(numSamples, 3);
% 
%   for i = 1:numSamples
%       [accelMeas(i,:), gyroMeas(i,:), magMeas(i,:)] ...
%           = imu(acc(i,:), angvel(i,:), orientation(i,:));
%   end
% 
%   subplot(3, 1, 1)
%   plot(t, accelMeas)
%   title('Accelerometer')
%   xlabel('s')
%   ylabel('m/s^2')
%   legend('x','y','z')
% 
%   subplot(3, 1, 2)
%   plot(t, gyroMeas)
%   title('Gyroscope')
%   xlabel('s')
%   ylabel('rad/s')
%   legend('x','y','z')
% 
%   subplot(3, 1, 3)
%   plot(t, magMeas)
%   title('Magnetometer')
%   xlabel('s')
%   ylabel('uT')
%   legend('x','y','z')
%
%   See also ACCELPARAMS, GYROPARAMS, MAGPARAMS, GPSSENSOR, INSSENSOR

%   Copyright 2017-2019 The MathWorks, Inc.

%#codegen

    properties (Nontunable)
        % IMUType Type of inertial measurement unit
        % Specify the IMU type as one of 'accel-gyro' | 'accel-mag' | 
        % 'accel-gyro-mag'. The default value is 'accel-gyro'. 
        IMUType = 'accel-gyro';    
        % SampleRate Sampling rate (Hz)
        % Specify the sampling frequency of the IMU as a positive scalar. 
        % The default value is 100. 
        SampleRate = 100;
    end
    
    properties
        % Temperature Temperature of IMU (degrees C)
        % Specify the operating temperature of the IMU as a real scalar. 
        % This property is tunable. The default value is 25.
        Temperature = 25;

        % MagneticField Magnetic field vector (uT)
        % Specify the magnetic field as a real 3-element row vector in the
        % navigation frame. This property is tunable. The default value is 
        % [27.5550 -2.4169 -16.0849]. 
        MagneticField; % = [ 27.5550 -2.4169 -16.0849];
    end
    
    properties 
        % Accelerometer Accelerometer sensor parameters
        % accelparams object containing accelerometer parameters
        % This property is tunable. 
        Accelerometer;

        % Gyroscope Gyroscope sensor parameters
        % gyroparams object containing gyroscope parameters.
        % This property is tunable. 
        Gyroscope; 
        
        % Magnetometer Magnetometer sensor parameters
        % magparams object containing magnetometer parameters.
        % This property is tunable. 
        Magnetometer;
    end
    
    properties (Nontunable)
        % RandomStream Random number source
        % Specify the source of the random number stream as one of the
        % following: 
        %
        % 'Global stream' - Random numbers are generated using the current 
        % global random number stream. 
        % 'mt19937ar with seed' - Random numbers are generated using the 
        % mt19937ar algorithm with the seed specified by the Seed property.
        %
        % The default value is 'Global stream'.
        RandomStream = 'Global stream';
        
        % Seed Initial seed
        % Specify the initial seed of an mt19937ar random number generator
        % algorithm as a real, nonnegative integer scalar. This property 
        % applies when you set the RandomStream property to 
        % 'mt19937ar with seed'. The default value is 67.
        Seed = uint32(67);
    end

    properties (Constant, Hidden)
        RandomStreamSet = matlab.system.StringSet({...
            'Global stream', ...
            'mt19937ar with seed'});
        IMUTypeSet = matlab.system.StringSet({...
            'accel-gyro', ...
            'accel-mag', ...
            'accel-gyro-mag'});
        ReferenceFrameSet = matlab.system.StringSet( ...
            fusion.internal.frames.ReferenceFrame.getOptions);
    end
    
    properties (Nontunable, Hidden)
        ReferenceFrame = fusion.internal.frames.ReferenceFrame.getDefault;
    end
    
    properties (Nontunable, Access = private)
        % Cached reference frame.
        pRefFrame;
    end

    properties (Access = private)
        % Random stream object (used in 'mt19937ar with seed' mode). 
        pStream;
        % Random number generator state. 
        pStreamState;
    end

    properties (Constant, Access = private)
        pNumRandomChannelsPerSensor = 9;
    end

    properties (Nontunable, Access = private)
        % Cached number of random channels needed. 
        pNumRandomChannels;
    end    

    properties (Access = private)
        % Internal System objects to execute each sensor model. 
        pAccel;
        pGyro;
        pMag;
    end
    
    properties (Nontunable, Dependent, Access = private)
        % Cached flags for existence of each sensor, dependent upon the
        % value of IMUType. 
        pHasGyro = true;
        pHasMag  = false;
    end   

    % Set methods
    methods
        function set.SampleRate(obj, val)
            validateattributes(val,{'single','double'}, ...
                {'real','scalar','positive','finite'}, ...
                '', ...
                'SampleRate');
            obj.SampleRate = val;
        end
        
        function set.Temperature(obj, val)
            validateattributes(val,{'single','double'}, ...
                {'real','scalar','finite'}, ...
                '', ...
                'Temperature');
            obj.Temperature = val;
        end
        
        function set.MagneticField(obj, val)
            validateattributes(val,{'single','double'}, ...
                {'real','size',[1,3],'finite'}, ...
                '', ...
                'MagneticField');
            obj.MagneticField = val;
        end
        
        function set.Seed(obj, val)
            validateattributes(val,{'numeric'}, ...
                {'real','scalar','integer','>=',0,'<',2^32}, ...
                '', ...
                'Seed');
            obj.Seed = uint32(val);
        end
        
        function set.Accelerometer(obj, val)
            coder.internal.errorIf(~isa(val, 'accelparams'), ...
                'shared_positioning:imuSensor:invalidType', ... 
                'Accelerometer', 'accelparams');
            coder.internal.errorIf(~isscalar(val), ...
            	'shared_positioning:imuSensor:expectedScalar', ...
                'Accelerometer');
            obj.Accelerometer = val;
        end
        
        function set.Gyroscope(obj, val)
            coder.internal.errorIf(~isa(val, 'gyroparams'), ...
            	'shared_positioning:imuSensor:invalidType', ...
                'Gyroscope', 'gyroparams');
            coder.internal.errorIf(~isscalar(val), ...
            	'shared_positioning:imuSensor:expectedScalar', ...
                'Gyroscope');
            obj.Gyroscope = val;
        end
        
        function set.Magnetometer(obj, val)
            coder.internal.errorIf(~isa(val, 'magparams'), ...
            	'shared_positioning:imuSensor:invalidType', ...
                'Magnetometer', 'magparams');
            coder.internal.errorIf(~isscalar(val), ...
            	'shared_positioning:imuSensor:expectedScalar', ...
                'Magnetometer');
            obj.Magnetometer = val;
        end
    end
    
    % Get methods
    methods
        function val = get.pHasGyro(obj)
            val = ~strcmp(obj.IMUType, 'accel-mag');
        end
        
        function val = get.pHasMag(obj)
            val = ~strcmp(obj.IMUType, 'accel-gyro');
        end
    end
    
    methods
        % Constructor
        function obj = imuSensor(varargin)
            setProperties(obj, nargin, varargin{:}, 'IMUType');
            isAccelSet = false;
            isGyroSet = false;
            isMagSet = false;
            isMagFieldSet = false;
            for i = 1:nargin
                if strcmp('Accelerometer', varargin{i})
                    isAccelSet = true;
                end
                if strcmp('Gyroscope', varargin{i})
                    isGyroSet = true;
                end
                if strcmp('Magnetometer', varargin{i})
                    isMagSet = true;
                end
                if strcmp('MagneticField', varargin{i})
                    isMagFieldSet = true;
                end
            end
            
            if ~isAccelSet
                obj.Accelerometer = accelparams();
            end
            if ~isGyroSet
                obj.Gyroscope = gyroparams();
            end
            if ~isMagSet
                obj.Magnetometer = magparams();
            end
            
            if isempty(coder.target) && ~isMagFieldSet
                initializeMagneticField(obj);
            end
        end
    end

    methods (Access = protected)
        function setupImpl(obj, ~, ~, ~) 
            
            isMagFieldSet = coder.internal.is_defined(obj.MagneticField);
            if ~isempty(coder.target) && ~isMagFieldSet
                initializeMagneticField(obj);
            end
            
            setupRandomStream(obj);
            
            obj.pRefFrame = ...
                fusion.internal.frames.ReferenceFrame.getMathObject( ...
                obj.ReferenceFrame);
            
            obj.pAccel = createSystemObject(obj.Accelerometer, ...
                'ReferenceFrame', obj.ReferenceFrame);
            obj.pAccel.SampleRate = obj.SampleRate;
            obj.pAccel.Temperature = obj.Temperature;

            if obj.pHasGyro
                obj.pGyro = createSystemObject(obj.Gyroscope);
                obj.pGyro.SampleRate = obj.SampleRate;
                obj.pGyro.Temperature = obj.Temperature;
            end

            if obj.pHasMag
                obj.pMag = createSystemObject(obj.Magnetometer);
                obj.pMag.SampleRate = obj.SampleRate;
                obj.pMag.Temperature = obj.Temperature;
            end
        end

        function setupRandomStream(obj)
            numSensors = 1 + obj.pHasGyro + obj.pHasMag;
            obj.pNumRandomChannels = numSensors ...
                * obj.pNumRandomChannelsPerSensor;

            if strcmp(obj.RandomStream,  'mt19937ar with seed') ...
                && isempty(coder.target)
                % Setup Random Stream object if required. 
                obj.pStream = RandStream('mt19937ar', 'seed', obj.Seed);
            end
        end

        function varargout = stepImpl(obj, acceleration, angularvelocity, orientation)
            if (nargin < 4)
                R = repmat(eye(3), 1, 1, size(acceleration, 1));
            elseif isa(orientation, 'quaternion')
                    R = rotmat(orientation, 'frame');
            else
                R = orientation;
            end
            
            numSamples = size(R, 3);
            numRandomChannelsPerSensor = obj.pNumRandomChannelsPerSensor;
            sensorIdx = 1:numRandomChannelsPerSensor;
            randNums = stepRandomStream(obj, numSamples);
            
            argoutIdx = 1;
            varargout{argoutIdx} = step(obj.pAccel, acceleration, R, randNums(:,sensorIdx));
            argoutIdx = argoutIdx + 1;
            sensorIdx = sensorIdx + numRandomChannelsPerSensor;
            
            if obj.pHasGyro
                varargout{argoutIdx} = step(obj.pGyro, angularvelocity, acceleration, R, randNums(:,sensorIdx));
                argoutIdx = argoutIdx + 1;
                sensorIdx = sensorIdx + numRandomChannelsPerSensor;
            end
            if obj.pHasMag
                if isa(R, 'single') || isa(acceleration, 'single') ...
                        || isa(angularvelocity, 'single')
                    dataType = 'single';
                else
                    dataType = 'double';
                end
                magneticfield = cast(repmat(obj.MagneticField,numSamples,1),dataType);
                varargout{argoutIdx} = step(obj.pMag, magneticfield, R, randNums(:,sensorIdx));
            end
        end        
        
        function whiteNoise = stepRandomStream(obj, numSamples)
            % Noise (random number) generation. 
            if strcmp(obj.RandomStream, 'Global stream')
                whiteNoise = randn(numSamples, obj.pNumRandomChannels);
            elseif isempty(coder.target)
                whiteNoise = randn(obj.pStream, numSamples,...
                                   obj.pNumRandomChannels);
            else
                allRandData = coder.nullcopy(zeros(numSamples,...
                                   obj.pNumRandomChannels));
                state = obj.pStreamState;
                for colIdx = 1:obj.pNumRandomChannels
                    % Noise is generated column-wisely
                    for rowIdx = 1:numSamples
                        [state, allRandData(rowIdx, colIdx)] = ...
                            eml_rand_mt19937ar('generate_normal', state);
                    end
                end
                obj.pStreamState = state;

                whiteNoise = allRandData;
            end
        end
        
        function processTunedPropertiesImpl(obj)
            if isChangedProperty(obj, 'Temperature')
                obj.pAccel.Temperature = obj.Temperature;
                updateSystemObject(obj.Accelerometer, obj.pAccel);
                if obj.pHasGyro
                    obj.pGyro.Temperature = obj.Temperature;
                    updateSystemObject(obj.Gyroscope, obj.pGyro);
                end
                if obj.pHasMag
                    obj.pMag.Temperature = obj.Temperature;
                    updateSystemObject(obj.Magnetometer, obj.pMag);
                end
            end
            if isChangedProperty(obj, 'Accelerometer')
                updateSystemObject(obj.Accelerometer, obj.pAccel);
            end
            if obj.pHasGyro && isChangedProperty(obj, 'Gyroscope')
                updateSystemObject(obj.Gyroscope, obj.pGyro);
            end
            if obj.pHasMag && isChangedProperty(obj, 'Magnetometer')
                updateSystemObject(obj.Magnetometer, obj.pMag);
            end
        end

        function resetImpl(obj)
            resetRandomStream(obj);

            if isLocked(obj)
                reset(obj.pAccel);
                
                if obj.pHasGyro
                    reset(obj.pGyro);
                end
                if obj.pHasMag
                    reset(obj.pMag);
                end
            end
        end

        function resetRandomStream(obj)
            if ~isempty(coder.target)
                obj.pStreamState = eml_rand_mt19937ar('preallocate_state');
                if strcmp(obj.RandomStream, 'mt19937ar with seed')
                    obj.pStreamState = ...
                        eml_rand_mt19937ar('seed_to_state', ...
                        obj.pStreamState, obj.Seed);
                end
            elseif strcmp(obj.RandomStream, 'mt19937ar with seed')
                obj.pStream.reset;
            end
        end
        
        function num = getNumOutputsImpl(obj)
            if strcmp(obj.IMUType, 'accel-gyro-mag')
                num = 3;
            else
                num = 2;
            end
        end

        function validateInputsImpl(~, acceleration, angularvelocity, orientation)
            validateattributes(acceleration, {'single', 'double'}, ...
                {'real', 'finite', '2d', 'ncols', 3});
            expectedDataType = class(acceleration);
            numSamples = size(acceleration, 1);
            validateattributes(angularvelocity, {expectedDataType}, ...
                {'real', 'finite', '2d', 'nrows', numSamples, 'ncols', 3});
            if (nargin == 4)
                if isa(orientation, 'quaternion')
                    quatDataType = classUnderlying(orientation);
                    coder.internal.errorIf(~strcmp(expectedDataType, quatDataType), ...
                        'shared_positioning:imuSensor:invalidUnderlyingType', expectedDataType, quatDataType);
                    validateattributes(orientation, {'quaternion'}, ...
                        {'nrows', numSamples, 'ncols', 1, '2d', 'finite'});
                else
                    validateattributes(orientation, {expectedDataType}, ...
                        {'real', 'finite', '3d', 'size', [3 3 numSamples]});
                end
            end
        end
        
        function flag = isInputComplexityMutableImpl(~, ~)
            flag = false;
        end

        function s = saveObjectImpl(obj)
            % Save public properties.
            s = saveObjectImpl@matlab.System(obj);

            % Save private properties. 
            if isLocked(obj)
                s.pNumRandomChannels = obj.pNumRandomChannels;
                s.pRefFrame = obj.pRefFrame;
                
                s.pAccel = matlab.System.saveObject(obj.pAccel);
                if obj.pHasGyro
                    s.pGyro  = matlab.System.saveObject(obj.pGyro);
                end
                if obj.pHasMag
                    s.pMag   = matlab.System.saveObject(obj.pMag);
                end

                if strcmp(obj.RandomStream, 'mt19937ar with seed')
                    if ~isempty(obj.pStream)
                        s.pStreamState = obj.pStream.State;
                    end
                end
            end
        end

        function loadObjectImpl(obj, s, wasLocked)
            % Load public properties. 
            loadObjectImpl@matlab.System(obj, s, wasLocked);

            % Load private properties.
            if wasLocked
                obj.pNumRandomChannels = s.pNumRandomChannels;
                obj.pRefFrame = s.pRefFrame;
                
                obj.pAccel = matlab.System.loadObject(s.pAccel);
                if obj.pHasGyro
                    obj.pGyro = matlab.System.loadObject(s.pGyro);
                end
                if obj.pHasMag
                    obj.pMag = matlab.System.loadObject(s.pMag);
                end

                if strcmp(s.RandomStream, 'mt19937ar with seed')
                    obj.pStream = RandStream('mt19937ar', ...
                        'seed', obj.Seed);
                    if ~isempty(s.pStreamState)
                        obj.pStream.State = s.pStreamState;
                    end
                end
            end
        end

        function flag = isInactivePropertyImpl(obj, prop)
            flag = false;
            if strcmp(prop, 'Seed')
                if strcmp(obj.RandomStream, 'Global stream')
                    flag = true;
                end
            end
            if strcmp(prop, 'Gyroscope')
                if ~obj.pHasGyro
                    flag = true;
                end
            end
            if strcmp(prop, 'Magnetometer')
                if ~obj.pHasMag
                    flag = true;
                end
            end
            if strcmp(prop, 'MagneticField')
                if ~obj.pHasMag
                    flag = true;
                end
            end
        end
        
        function initializeMagneticField(obj)
            %INITIALIZEMAGNETICFIELD Set the MagneticField based on the
            %   reference frame. 
            
            magFieldNED = defaultMagFieldNED;
            magFieldNorth = magFieldNED(1);
            magFieldEast = magFieldNED(2);
            magFieldDown = magFieldNED(3);
            
            refFrame = fusion.internal.frames.ReferenceFrame.getMathObject( ...
                obj.ReferenceFrame);
            magField = zeros(1,3);
            magField(refFrame.NorthIndex) = magFieldNorth;
            magField(refFrame.EastIndex) = magFieldEast;
            magField(3) = -refFrame.ZAxisUpSign * magFieldDown;
            
            obj.MagneticField = magField;
        end
    end
    
    methods (Hidden)
        function sensorsim = getSensorSimulator(obj, name)
            %This method is for internal use only. It may be removed in the
            % future.
            
            % Extract the underlying System object for an individual
            % sensor to verify tunable parameters have been updated. 
            validateattributes(name, {'char'}, {'row'});
            sensorsim = [];
            if isLocked(obj)
                processTunedPropertiesImpl(obj);
                switch lower(name)
                    case 'accelerometer'
                        sensorsim = obj.pAccel;
                    case 'gyroscope'
                        sensorsim = obj.pGyro;
                    case 'magnetometer'
                        sensorsim = obj.pMag;  
                end
            end
        end
    end
    methods (Hidden, Static)
        function flag = isAllowedInSystemBlock
            flag = false;
        end
    end
end

function mfNED = defaultMagFieldNED
mfNED = fusion.internal.UnitConversions.MagneticFieldNED;
end
