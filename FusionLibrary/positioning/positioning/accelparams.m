classdef accelparams < fusion.internal.IMUSensorParameters & fusion.internal.UnitDisplayerValue
%   ACCELPARAMS Accelerometer sensor parameters
%   params = ACCELPARAMS returns an accelerometer parameter object with
%   default values assigned to each property.
%
%   params = ACCELPARAMS('Name', Value) returns an accelerometer parameter
%   object with each specified property name set to the specified value.
%   You can specify additional name-value pair arguments in any order as
%   (Name1, Value1, ...,NameN, ValueN).
%
%   ACCELPARAMS properties:
%
%   MeasurementRange          - Maximum sensor reading (m/s^2)
%   Resolution                - Resolution of sensor measurements
%                               (m/s^2/LSB)
%   ConstantBias              - Constant sensor offset bias (m/s^2)
%   AxesMisalignment          - Sensor axes skew (%)
%   NoiseDensity              - Power spectral density of sensor noise
%                               (m/s^2/sqrt(Hz))
%   BiasInstability           - Instability of the bias offset (m/s^2)
%   RandomWalk                - Integrated white noise of sensor
%                               ((m/s^2)*sqrt(Hz))
%   TemperatureBias           - Sensor bias from temperature
%                               (m/s^2/degrees C)
%   TemperatureScaleFactor    - Scale factor error from temperature 
%                               (%/degrees C)
%
%   EXAMPLE: Generate ideal accelerometer data from stationary input.
%
%   Fs = 100;
%   numSamples = 1000;
%   t = 0:1/Fs:(numSamples-1)/Fs;
%
%   params = accelparams;
%   imu = imuSensor('SampleRate', Fs, 'Accelerometer', params);
%
%   acc = zeros(numSamples, 3);
%   angvel = zeros(numSamples, 3);
%
%   accelData = imu(acc, angvel);
%
%   plot(t, accelData)
%   title('Accelerometer')
%   xlabel('s')
%   ylabel('m/s^2')
%
%   See also GYROPARAMS, MAGPARAMS, IMUSENSOR

%   Copyright 2017-2019 The MathWorks, Inc.

%#codegen
    
    methods
        function obj = accelparams(varargin)
            obj = obj@fusion.internal.IMUSensorParameters(varargin{:});
        end
    end
    
    methods (Access = protected)
        function sobj = createSystemObjectImpl(~, varargin)
            sobj = fusion.internal.AccelerometerSimulator(varargin{:});
        end
        
        function unit = getDisplayUnitImpl(~)
            unit = ['m/s' char(178)];
        end
        
        function groups = getPropertyGroups(obj)
            if ~isscalar(obj)
                groups = getPropertyGroups@matlab.mixin.CustomDisplay(obj);
            else
                basicList.MeasurementRange     = obj.MeasurementRange;
                basicList.Resolution           = obj.Resolution;
                basicList.ConstantBias         = obj.ConstantBias;
                basicList.AxesMisalignment     = obj.AxesMisalignment;
                
                noiseList.NoiseDensity         = obj.NoiseDensity;
                noiseList.BiasInstability      = obj.BiasInstability;
                noiseList.RandomWalk           = obj.RandomWalk;
                
                envList.TemperatureBias        = obj.TemperatureBias;
                envList.TemperatureScaleFactor = obj.TemperatureScaleFactor;
                    
                basicGroup = matlab.mixin.util.PropertyGroup(basicList);
                noiseGroup = matlab.mixin.util.PropertyGroup(noiseList);
                envGroup   = matlab.mixin.util.PropertyGroup(envList);

                groups = [basicGroup noiseGroup envGroup];
            end
        end
    end
    
    methods (Hidden, Static)
        function name = matlabCodegenRedirect(~)
            name = 'fusion.internal.coder.accelparamscg';
        end
    end
    
end
