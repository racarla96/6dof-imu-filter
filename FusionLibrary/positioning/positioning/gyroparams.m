classdef gyroparams < fusion.internal.IMUSensorParameters & fusion.internal.UnitDisplayerValue
%   GYROPARAMS Gyroscope sensor parameters
%   params = GYROPARAMS returns a gyroscope parameter object with  default
%   values assigned to each property.
%
%   params = GYROPARAMS('Name', Value) returns a gyroscope parameter object
%   with each specified property name set to the specified value. You can
%   specify additional name-value pair arguments in any order as
%   (Name1, Value1, ...,NameN, ValueN).
%
%   GYROPARAMS properties:
%
%   MeasurementRange          - Maximum sensor reading (rad/s)
%   Resolution                - Resolution of sensor measurements
%                               (rad/s/LSB)
%   ConstantBias              - Constant sensor offset bias (rad/s)
%   AxesMisalignment          - Sensor axes skew (%)
%   NoiseDensity              - Power spectral density of sensor noise
%                               (rad/s/sqrt(Hz))
%   BiasInstability           - Instability of the bias offset (rad/s)
%   RandomWalk                - Integrated white noise of sensor
%                               ((rad/s)*sqrt(Hz))
%   TemperatureBias           - Sensor bias from temperature
%                               (rad/s/degrees C)
%   TemperatureScaleFactor    - Scale factor error from temperature 
%                               (%/degrees C)
%   AccelerationBias          - Sensor bias from linear acceleration
%                               ((rad/s) / (m/s^2))
%
%   EXAMPLE: Generate ideal gyroscope data from stationary input.
%
%   Fs = 100;
%   numSamples = 1000;
%   t = 0:1/Fs:(numSamples-1)/Fs;
%
%   params = gyroparams;
%   imu = imuSensor('accel-gyro', 'SampleRate', Fs, 'Gyroscope', params);
%
%   acc = zeros(numSamples, 3);
%   angvel = zeros(numSamples, 3);
%
%   [~, gyroData] = imu(acc, angvel);
%
%   plot(t, gyroData)
%   title('Gyroscope')
%   xlabel('s')
%   ylabel('rad/s')
%
%   See also ACCELPARAMS, MAGPARAMS, IMUSENSOR

%   Copyright 2017-2019 The MathWorks, Inc.

%#codegen
    
    properties
        % AccelerationBias Sensor bias from acceleration ((rad/s)/(m/s^2))
        % Specify the acceleration bias as a real scalar or 3-element row
        % vector. The default value is [0 0 0].
        AccelerationBias = [0 0 0];
    end
    
    properties (Hidden)
        AccelerationBiasUnits;
    end
    
    methods
        function obj = gyroparams(varargin)
            obj = obj@fusion.internal.IMUSensorParameters(varargin{:});
        end
    end
    
    methods (Access = protected)
        function sobj = createSystemObjectImpl(~)
            sobj = fusion.internal.GyroscopeSimulator();
        end
        
        function updateSystemObjectImpl(obj, sobj)
            sobj.AccelerationBias = getRowVector(obj, 'AccelerationBias');
        end

        function unit = getDisplayUnitImpl(~)
            unit = 'rad/s';
        end
    end
    
    methods
        function obj = set.AccelerationBias(obj,val)
            validateattributes(val, {'single','double'}, {'real','finite'}, '', 'AccelerationBias');
            fusion.internal.IMUSensorParameters.validateSize(val, 'AccelerationBias');
            obj.AccelerationBias = val .* ones(1,3);
        end
        
        function val = get.AccelerationBiasUnits(obj)
            val = sprintf(['(%s)/(m/s' char(178) ')'],obj.getDisplayUnit);
        end
    end
    
    methods (Access = protected)
        function groups = getPropertyGroups(obj)
            if ~isscalar(obj)
                groups = getPropertyGroups@matlab.mixin.CustomDisplay(obj);
            else
                basicList.MeasurementRange     = obj.MeasurementRange;
                basicList.Resolution           = obj.Resolution;
                basicList.ConstantBias         = obj.ConstantBias;
                basicList.AxesMisalignment     = obj.AxesMisalignment;
                
                noiseList.NoiseDensity         = obj.NoiseDensity;
                noiseList.BiasInstability      = obj.BiasInstability;
                noiseList.RandomWalk           = obj.RandomWalk;
                
                envList.TemperatureBias        = obj.TemperatureBias;
                envList.TemperatureScaleFactor = obj.TemperatureScaleFactor;
                envList.AccelerationBias       = obj.AccelerationBias;
                    
                basicGroup = matlab.mixin.util.PropertyGroup(basicList);
                noiseGroup = matlab.mixin.util.PropertyGroup(noiseList);
                envGroup   = matlab.mixin.util.PropertyGroup(envList);

                groups = [basicGroup noiseGroup envGroup];
            end
        end
    end

    methods (Hidden, Static)
        function name = matlabCodegenRedirect(~)
            name = 'fusion.internal.coder.gyroparamscg';
        end
    end
    
end
