classdef ahrs10filter < fusion.internal.AHRS10FilterBase & ...
    fusion.internal.UnitDisplayer 
    %AHRS10FILTER  Height and orientation from MARG and altimeter
    %
    %   FILT = AHRS10FILTER returns a filter that estimates height and
    %   orientation based on altimeter, accelerometer, gyroscope, and
    %   magnetometer measurements. 
    %
    %   FILT = AHRS10FILTER(..., 'ReferenceFrame', RF) returns a filter
    %   that estimates height and orientation relative to the reference
    %   frame RF. Specify the reference frame as 'NED' (North-East-Down) or
    %   'ENU' (East-North-Up). The default value is 'NED'.
    %
    %   FILT = AHRS10FILTER(..., 'Name', Value, ...) returns a filter with
    %   each specified property name set to the specified value. You can
    %   specify additional name-value pair arguments in any order as
    %   (Name1,Value1,...,NameN,ValueN).
    %
    %   AHRS10FILTER implements sensor fusion of MARG and altimeter
    %   data to estimate height and orientation in the navigation reference
    %   frame. MARG (magnetic-angular rate-gravity) data is typically
    %   derived from magnetometer, gyroscope and accelerometer data,
    %   respectively. The filter uses a 18-element state vector to track
    %   the orientation quaternion, vertical velocity, vertical position,
    %   MARG sensor biases, and geomagnetic vector. The AHRS10FILTER class
    %   uses an extended Kalman filter to estimate these quantities.
    %
    %   AHRS10FILTER Methods: 
    %
    %       predict       - Update states using accelerometer and gyroscope
    %       fusemag       - Correct states using magnetometer data
    %       fusealtimeter - Correct states using altimeter data
    %       correct       - Correct states with direct state measurements
    %       pose          - Estimated altitude and orientation 
    %       reset         - Reinitialize internal states
    %       stateinfo     - Definition of State property vector
    %   
    %   AHRS10FILTER Properties:
    %
    %       IMUSampleRate          - Sample rate of the IMU (Hz)
    %       GyroscopeNoise         - Gyroscope process noise variance
    %                                (rad/s)^2
    %       AccelerometerNoise     - Accelerometer process noise variance
    %                                (m/s^2)^2
    %       GyroscopeBiasNoise     - Gyroscope bias process noise variance
    %                                (rad/s)^2
    %       AccelerometerBiasNoise - Accelerometer bias process noise 
    %                                variance (m/s^2)^2
    %       GeomagneticVectorNoise - Geomagnetic vector process noise 
    %                                variance (uT^2)
    %       MagnetometerBiasNoise  - Magnetometer bias process noise 
    %                                variance (uT^2)
    %       State                  - State vector of extended Kalman Filter
    %       StateCovariance        - State error covariance for 
    %                                extended Kalman Filter
    %   
    %   Example : Estimate orientation and height
    %   
    %   % Load logged sensor data and ground truth pose
    %   ld = load('fuse10ex.mat');
    %   imuFs = ld.imuFs;
    %   accel = ld.accel;
    %   gyro = ld.gyro;
    %   mag = ld.mag;
    %   alt = ld.alt;
    %   imuSamplesPerAlt = fix(imuFs/ld.altFs);
    %   imuSamplesPerMag = fix(imuFs/ld.magFs);
    %
    %   % Setup the fusion filter
    %   f = ahrs10filter;
    %   f.IMUSampleRate = imuFs;
    %   f.AccelerometerNoise = 0.1;
    %   f.StateCovariance = ld.initcov;
    %   f.State = ld.initstate;
    %
    %   Ralt = 0.24;
    %   Rmag = 0.9;
    %
    %   N = size(accel,1);
    %   p = zeros(N,1);
    %   q = zeros(N,1, 'quaternion');
    %
    %   % Fuse accelerometer, gyroscope, magnetometer and altimeter
    %   for ii=1:size(accel,1)
    %       % Fuse IMU
    %       f.predict(accel(ii,:), gyro(ii,:));
    %
    %       % Fuse magnetometer
    %       if ~mod(ii, imuSamplesPerMag)
    %           f.fusemag(mag(ii,:), Rmag);
    %       end
    %
    %       % Fuse altimeter
    %       if ~mod(ii, imuSamplesPerAlt)
    %           f.fusealtimeter(alt(ii), Ralt);
    %       end
    %
    %       [p(ii),q(ii)] = pose(f);
    %   end
    %
    %   % RMS errors
    %   posErr = ld.expectedAlt - p;
    %   qErr = rad2deg(dist(ld.expectedOrient,q));
    %   pRMS = sqrt(mean(posErr.^2));
    %   qRMS = sqrt(mean(qErr.^2));
    %   fprintf('Altitude RMS Error\n');
    %   fprintf('\t%.2f (meters)\n\n', pRMS);
    %   fprintf('Quaternion Distance RMS Error\n');
    %   fprintf('\t%.2f (degrees)\n\n', qRMS);
    %
    %   See also insfilter, ahrsfilter
    
    %   Copyright 2018-2019 The MathWorks, Inc.

    %#codegen

    properties (Constant, Hidden)
        IMUSampleRateUnits          = 'Hz';
        GyroscopeNoiseUnits        = ['(rad/s)' char(178)];
        AccelerometerNoiseUnits     = ['(m/s' char(178) ')' char(178)];
        GyroscopeBiasNoiseUnits    = ['(rad/s)' char(178)];
        AccelerometerBiasNoiseUnits = ['(m/s' char(178) ')' char(178)];
        GeomagneticVectorNoiseUnits = ['uT' char(178)];
        MagnetometerBiasNoiseUnits  = ['uT' char(178)];
    end    

    methods (Hidden)
        function obj = ahrs10filter(varargin) 
            obj@fusion.internal.AHRS10FilterBase(varargin{:});
        end
    end

    methods (Access = protected)

        function displayScalarObject(obj)
            displayScalarObjectWithUnits(obj);
        end
        function groups = getPropertyGroups(~)
            % Add section titles to property display
            basic = {'IMUSampleRate'};
            multprocNoise = {'GyroscopeNoise', 'AccelerometerNoise', ...
                'GyroscopeBiasNoise', 'AccelerometerBiasNoise'};

            addprocNoise = {'GeomagneticVectorNoise', ...
                'MagnetometerBiasNoise'};
            
            initVals = {'State', 'StateCovariance'};
            
            g1 = matlab.mixin.util.PropertyGroup(cat(2, basic, initVals));
            g2 = matlab.mixin.util.PropertyGroup(multprocNoise, ...
                'Multiplicative Process Noise Variances');
            g3 = matlab.mixin.util.PropertyGroup(addprocNoise, ...
                'Additive Process Noise Variances');
            groups = [g1,g2,g3];        
        end
    end
    
    methods (Access=public, Hidden, Static)
        function name = matlabCodegenRedirect(~)
            name = 'fusion.internal.coder.ahrs10filtercg';
        end
    end
end % classdef

