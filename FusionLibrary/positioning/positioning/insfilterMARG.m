classdef insfilterMARG < fusion.internal.MARGGPSFuserBase & ...
    fusion.internal.UnitDisplayer 
    %INSFILTERMARG Estimate pose from MARG and GPS data 
    %
    %   FILT = INSFILTERMARG implements sensor fusion of MARG and GPS data
    %   to estimate pose in the navigation reference frame. MARG
    %   (magnetic-angular rate-gravity) data is typically derived from
    %   magnetometer, gyroscope and accelerometer data, respectively. The
    %   filter uses a 22-element state vector to track the orientation
    %   quaternion, velocity, position, MARG sensor biases, and geomagnetic
    %   vector. The insfilterMARG class uses an extended Kalman filter to
    %   estimate these quantities.
    %
    %   FILT = INSFILTERMARG('ReferenceFrame', RF) returns an inertial
    %   navigation filter that estimates pose relative to the reference
    %   frame RF. Specify the reference frame as 'NED' (North-East-Down) or
    %   'ENU' (East-North-Up). The default value is 'NED'.
    % 
    %   INSFILTERMARG Methods: 
    %
    %       predict   - Update states using accelerometer and gyroscope data
    %       fusemag   - Correct states using magnetometer data
    %       fusegps   - Correct states using GPS data
    %       correct   - Correct states with direct state measurements
    %       pose      - Current position, orientation, and velocity estimate
    %       reset     - Reinitialize internal states
    %       stateinfo - Definition of each element of State property vector
    %   
    %   INSFILTERMARG Properties:
    %
    %       IMUSampleRate          - Sample rate of the IMU (Hz)
    %       ReferenceLocation      - Reference location (deg, deg, meters)
    %       GyroscopeNoise         - Gyroscope process noise variance
    %                                (rad/s)^2
    %       AccelerometerNoise     - Accelerometer process noise variance
    %                                (m/s^2)^2
    %       GyroscopeBiasNoise     - Gyroscope bias process noise variance
    %                                (rad/s)^2
    %       AccelerometerBiasNoise - Accelerometer bias process noise 
    %                                variance (m/s^2)^2
    %       GeomagneticVectorNoise - Geomagnetic vector process noise 
    %                                variance (uT^2)
    %       MagnetometerBiasNoise  - Magnetometer bias process noise 
    %                                variance (uT^2)
    %       State                  - State vector of extended Kalman Filter
    %       StateCovariance        - State error covariance for 
    %                                extended Kalman Filter
    %   
    %   Example : Estimate the pose of a UAV
    %   
    %   % Load logged sensor data and ground truth pose
    %   load uavshort.mat
    %
    %   % Setup the fusion filter
    %   f = insfilterMARG('IMUSampleRate', imuFs, 'ReferenceLocation', ...
    %       refloc, 'AccelerometerBiasNoise', 2e-4, ...
    %       'AccelerometerNoise', 2, 'GyroscopeBiasNoise', 1e-16, ...
    %       'GyroscopeNoise', 1e-5, 'MagnetometerBiasNoise', 1e-10, ...
    %       'GeomagneticVectorNoise', 1e-12, 'StateCovariance', ...
    %       1e-9*ones(22), 'State', initstate);
    %
    %   gpsidx = 1;
    %   N = size(accel,1);
    %   p = zeros(N,3);
    %   q = zeros(N,1, 'quaternion');
    %
    %   % Fuse accelerometer, gyroscope, magnetometer and GPS
    %   for ii=1:size(accel,1)
    %       % Fuse IMU
    %       f.predict(accel(ii,:), gyro(ii,:));
    %
    %       % Fuse magnetometer at 1/2 the IMU rate
    %       if ~mod(ii, fix(imuFs/2))
    %           f.fusemag(mag(ii,:), Rmag);
    %       end
    %
    %       % Fuse GPS once per second
    %       if ~mod(ii, imuFs)
    %           f.fusegps(lla(gpsidx,:), Rpos, gpsvel(gpsidx,:), Rvel);
    %           gpsidx = gpsidx  + 1;
    %       end
    %
    %       [p(ii,:),q(ii)] = pose(f);
    %   end
    %
    %   % RMS errors
    %   posErr = truePos - p;
    %   qErr = rad2deg(dist(trueOrient,q));
    %   pRMS = sqrt(mean(posErr.^2));
    %   qRMS = sqrt(mean(qErr.^2));
    %   fprintf('Position RMS Error\n');
    %   fprintf('\tX: %.2f , Y: %.2f, Z: %.2f (meters)\n\n', pRMS(1), ...
    %       pRMS(2), pRMS(3));
    %   
    %   fprintf('Quaternion Distance RMS Error\n');
    %   fprintf('\t%.2f (degrees)\n\n', qRMS);
    %
    %
    %   See also INSFILTERERRORSTATE, INSFILTERASYNC, INSFILTERNONHOLONOMIC
    
    %   Copyright 2018-2019 The MathWorks, Inc.

    %#codegen

    properties (Constant, Hidden)
        IMUSampleRateUnits          = 'Hz';
        ReferenceLocationUnits      = '[deg deg m]';
        GyroscopeNoiseUnits        = ['(rad/s)' char(178)];
        AccelerometerNoiseUnits     = ['(m/s' char(178) ')' char(178)];
        GyroscopeBiasNoiseUnits    = ['(rad/s)' char(178)];
        AccelerometerBiasNoiseUnits = ['(m/s' char(178) ')' char(178)];
        GeomagneticVectorNoiseUnits = ['uT' char(178)];
        MagnetometerBiasNoiseUnits  = ['uT' char(178)];
    end    

    methods
        function obj = insfilterMARG(varargin)
            obj@fusion.internal.MARGGPSFuserBase(varargin{:});
        end
    end

    methods (Access = protected)

        function displayScalarObject(obj)
            displayScalarObjectWithUnits(obj);
        end
        function groups = getPropertyGroups(~)
            % Add section titles to property display
            basic = {'IMUSampleRate', 'ReferenceLocation'};
            multprocNoise = {'GyroscopeNoise', 'AccelerometerNoise', ...
                'GyroscopeBiasNoise', 'AccelerometerBiasNoise'};

            addprocNoise = {'GeomagneticVectorNoise', ...
                'MagnetometerBiasNoise'};
            
            initVals = {'State', 'StateCovariance'};
            
            g1 = matlab.mixin.util.PropertyGroup(cat(2, basic, initVals));
            g2 = matlab.mixin.util.PropertyGroup(multprocNoise, ...
                'Multiplicative Process Noise Variances');
            g3 = matlab.mixin.util.PropertyGroup(addprocNoise, ...
                'Additive Process Noise Variances');
            groups = [g1,g2,g3];        
        end
    end
    
    methods (Access=public, Hidden, Static)
        function name = matlabCodegenRedirect(~)
            name = 'fusion.internal.coder.insfilterMARGCG';
        end
    end
end % classdef

