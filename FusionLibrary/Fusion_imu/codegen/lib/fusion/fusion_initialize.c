/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: fusion_initialize.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 17-Mar-2020 00:12:08
 */

/* Include Files */
#include "fusion_initialize.h"
#include "fusion.h"
#include "fusion_data.h"
#include "rt_nonfinite.h"

/* Function Definitions */

/*
 * Arguments    : void
 * Return Type  : void
 */
void fusion_initialize(void)
{
  rt_InitInfAndNaN();
  isInitialized_fusion = true;
}

/*
 * File trailer for fusion_initialize.c
 *
 * [EOF]
 */
