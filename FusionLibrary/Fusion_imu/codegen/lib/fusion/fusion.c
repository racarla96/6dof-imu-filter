/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: fusion.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 17-Mar-2020 00:12:08
 */

/* Include Files */
#include "fusion.h"
#include "IMUFilterBase.h"
#include "fusion_data.h"
#include "fusion_initialize.h"
#include "rt_nonfinite.h"
#include <string.h>

/* Function Definitions */

/*
 * Arguments    : const float acc[3]
 *                const float gyro[3]
 *                c_matlabshared_rotations_intern *q
 *                float gyroRate[3]
 * Return Type  : void
 */
void fusion(const float acc[3], const float gyro[3],
            c_matlabshared_rotations_intern *q, float gyroRate[3])
{
  c_fusion_internal_coder_imufilt FUSE;
  int i;
  signed char b_I[9];
  static const float fv[81] = { 6.09234849E-6F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F,
    0.0F, 0.0F, 0.0F, 0.0F, 6.09234849E-6F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F,
    0.0F, 0.0F, 0.0F, 6.09234849E-6F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F,
    0.0F, 0.0F, 7.61543561E-5F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F,
    0.0F, 7.61543561E-5F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F,
    7.61543561E-5F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F,
    0.00962361F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F,
    0.00962361F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F,
    0.00962361F };

  if (isInitialized_fusion == false) {
    fusion_initialize();
  }

  FUSE.AccelerometerNoise = 0.0001924722;
  FUSE.GyroscopeNoise = 9.1385E-5;
  FUSE.GyroscopeDriftNoise = 3.0462E-13;
  FUSE.LinearAccelerationNoise = 0.0096236100000000012;
  FUSE.LinearAccelerationDecayFactor = 0.5;
  FUSE.isInitialized = 1;
  FUSE.pInputPrototype[0] = acc[0];
  FUSE.pInputPrototype[1] = acc[1];
  FUSE.pInputPrototype[2] = acc[2];
  FUSE.pSensorPeriod = 0.0025;
  FUSE.pKalmanPeriod = 0.0025;
  FUSE.TunablePropsChanged = false;
  FUSE.pOrientPost.a = 1.0F;
  FUSE.pOrientPost.b = 0.0F;
  FUSE.pOrientPost.c = 0.0F;
  FUSE.pOrientPost.d = 0.0F;
  FUSE.pGyroOffset[0] = 0.0F;
  FUSE.pGyroOffset[1] = 0.0F;
  FUSE.pGyroOffset[2] = 0.0F;
  for (i = 0; i < 9; i++) {
    b_I[i] = 0;
  }

  b_I[0] = 1;
  b_I[4] = 1;
  b_I[8] = 1;
  for (i = 0; i < 9; i++) {
    FUSE.pQv[i] = 0.0098160827711562537 * (double)b_I[i];
  }

  memcpy(&FUSE.pQw[0], &fv[0], 81U * sizeof(float));
  FUSE.pLinAccelPost[0] = 0.0F;
  FUSE.pLinAccelPost[1] = 0.0F;
  FUSE.pLinAccelPost[2] = 0.0F;
  FUSE.pFirstTime = true;
  IMUFilterBase_stepImpl(&FUSE, acc, gyro, &q->a, &q->b, &q->c, &q->d, gyroRate);
}

/*
 * File trailer for fusion.c
 *
 * [EOF]
 */
