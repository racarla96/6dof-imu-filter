/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: fusion_data.h
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 17-Mar-2020 00:12:08
 */

#ifndef FUSION_DATA_H
#define FUSION_DATA_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "fusion_types.h"

/* Variable Declarations */
extern boolean_T isInitialized_fusion;

#endif

/*
 * File trailer for fusion_data.h
 *
 * [EOF]
 */
