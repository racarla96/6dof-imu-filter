/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: IMUFilterBase.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 17-Mar-2020 00:12:08
 */

/* Include Files */
#include "IMUFilterBase.h"
#include "fusion.h"
#include "quaternioncg.h"
#include "rt_nonfinite.h"
#include <math.h>
#include <string.h>

/* Function Definitions */

/*
 * Arguments    : c_fusion_internal_coder_imufilt *obj
 *                const float accelIn[3]
 *                const float gyroIn[3]
 *                float *orientOut_a
 *                float *orientOut_b
 *                float *orientOut_c
 *                float *orientOut_d
 *                float av[3]
 * Return Type  : void
 */
void IMUFilterBase_stepImpl(c_fusion_internal_coder_imufilt *obj, const float
  accelIn[3], const float gyroIn[3], float *orientOut_a, float *orientOut_b,
  float *orientOut_c, float *orientOut_d, float av[3])
{
  c_matlabshared_rotations_intern qerr;
  float Reast[3];
  float R[9];
  float n;
  float a21;
  float deltaq_c;
  float deltaq_d;
  float xa;
  float xb;
  float xc;
  int k;
  int rtemp;
  float h1[9];
  int xpageoffset;
  double a;
  boolean_T y[3];
  boolean_T b[9];
  boolean_T exitg1;
  boolean_T nanPageIdx;
  float H[27];
  float Qw[81];
  int H_tmp;
  static const signed char iv[9] = { 1, 0, 0, 0, 1, 0, 0, 0, 1 };

  int r1;
  int r2;
  int r3;
  float tmp_tmp[27];
  float b_tmp[27];
  float b_y[27];
  double b_a;
  double c_a;
  float Ppost[81];
  av[0] = gyroIn[0] - obj->pGyroOffset[0];
  av[1] = gyroIn[1] - obj->pGyroOffset[1];
  av[2] = gyroIn[2] - obj->pGyroOffset[2];
  if (obj->pFirstTime) {
    obj->pFirstTime = false;
    Reast[0] = accelIn[1] * 0.0F - accelIn[2] * 0.0F;
    Reast[1] = accelIn[2] - accelIn[0] * 0.0F;
    Reast[2] = accelIn[0] * 0.0F - accelIn[1];
    R[6] = accelIn[0];
    R[3] = Reast[0];
    R[7] = accelIn[1];
    R[4] = Reast[1];
    R[8] = accelIn[2];
    R[5] = Reast[2];
    R[0] = Reast[1] * accelIn[2] - Reast[2] * accelIn[1];
    R[1] = Reast[2] * accelIn[0] - Reast[0] * accelIn[2];
    R[2] = Reast[0] * accelIn[1] - Reast[1] * accelIn[0];
    for (k = 0; k < 9; k++) {
      h1[k] = R[k] * R[k];
    }

    for (rtemp = 0; rtemp < 3; rtemp++) {
      xpageoffset = rtemp * 3;
      Reast[rtemp] = sqrtf((h1[xpageoffset] + h1[xpageoffset + 1]) +
                           h1[xpageoffset + 2]);
    }

    for (k = 0; k < 9; k++) {
      h1[k] = R[k];
    }

    for (k = 0; k < 3; k++) {
      R[3 * k] = h1[3 * k] / Reast[k];
      rtemp = 3 * k + 1;
      R[rtemp] = h1[rtemp] / Reast[k];
      rtemp = 3 * k + 2;
      R[rtemp] = h1[rtemp] / Reast[k];
    }

    for (k = 0; k < 9; k++) {
      b[k] = rtIsNaNF(R[k]);
    }

    y[0] = false;
    y[1] = false;
    y[2] = false;
    rtemp = 1;
    exitg1 = false;
    while ((!exitg1) && (rtemp <= 3)) {
      if (b[rtemp - 1]) {
        y[0] = true;
        exitg1 = true;
      } else {
        rtemp++;
      }
    }

    rtemp = 4;
    exitg1 = false;
    while ((!exitg1) && (rtemp <= 6)) {
      if (b[rtemp - 1]) {
        y[1] = true;
        exitg1 = true;
      } else {
        rtemp++;
      }
    }

    rtemp = 7;
    exitg1 = false;
    while ((!exitg1) && (rtemp <= 9)) {
      if (b[rtemp - 1]) {
        y[2] = true;
        exitg1 = true;
      } else {
        rtemp++;
      }
    }

    nanPageIdx = false;
    k = 0;
    exitg1 = false;
    while ((!exitg1) && (k < 3)) {
      if (y[k]) {
        nanPageIdx = true;
        exitg1 = true;
      } else {
        k++;
      }
    }

    if (nanPageIdx) {
      for (k = 0; k < 9; k++) {
        R[k] = 0.0F;
      }

      R[0] = 1.0F;
      R[4] = 1.0F;
      R[8] = 1.0F;
    }

    quaternioncg_quaternioncg(R, &obj->pOrientPost.a, &obj->pOrientPost.b,
      &obj->pOrientPost.c, &obj->pOrientPost.d);
  }

  qerr = obj->pOrientPost;
  Reast[0] = (gyroIn[0] - obj->pGyroOffset[0]) * (float)obj->pSensorPeriod;
  Reast[1] = (gyroIn[1] - obj->pGyroOffset[1]) * (float)obj->pSensorPeriod;
  Reast[2] = (gyroIn[2] - obj->pGyroOffset[2]) * (float)obj->pSensorPeriod;
  b_quaternioncg_quaternioncg(Reast, &n, &a21, &deltaq_c, &deltaq_d);
  xa = qerr.a;
  xb = qerr.b;
  xc = qerr.c;
  qerr.a = ((qerr.a * n - qerr.b * a21) - qerr.c * deltaq_c) - qerr.d * deltaq_d;
  qerr.b = ((xa * a21 + qerr.b * n) + qerr.c * deltaq_d) - qerr.d * deltaq_c;
  qerr.c = ((xa * deltaq_c - xb * deltaq_d) + qerr.c * n) + qerr.d * a21;
  qerr.d = ((xa * deltaq_d + xb * deltaq_c) - xc * a21) + qerr.d * n;
  if (qerr.a < 0.0F) {
    qerr.a = -qerr.a;
    qerr.b = -qerr.b;
    qerr.c = -qerr.c;
    qerr.d = -qerr.d;
  }

  obj->pOrientPrior = qerr;
  qerr = obj->pOrientPrior;
  n = sqrtf(((qerr.a * qerr.a + qerr.b * qerr.b) + qerr.c * qerr.c) + qerr.d *
            qerr.d);
  qerr.a /= n;
  qerr.b /= n;
  qerr.c /= n;
  qerr.d /= n;
  a = obj->LinearAccelerationDecayFactor;
  obj->pLinAccelPrior[0] = (float)a * obj->pLinAccelPost[0];
  obj->pLinAccelPrior[1] = (float)a * obj->pLinAccelPost[1];
  obj->pLinAccelPrior[2] = (float)a * obj->pLinAccelPost[2];
  for (k = 0; k < 9; k++) {
    h1[k] = 0.0F;
  }

  deltaq_c = (qerr.a * qerr.a * 2.0F - 1.0F) + qerr.d * qerr.d * 2.0F;
  h1[3] = deltaq_c;
  deltaq_d = qerr.c * qerr.d * 2.0F + qerr.a * qerr.b * 2.0F;
  h1[6] = -deltaq_d;
  xa = qerr.b * qerr.d * 2.0F - qerr.a * qerr.c * 2.0F;
  h1[7] = xa;
  for (k = 0; k < 3; k++) {
    R[3 * k] = h1[3 * k];
    rtemp = 3 * k + 1;
    R[rtemp] = h1[rtemp] - h1[k + 3];
    rtemp = 3 * k + 2;
    R[rtemp] = h1[rtemp] - h1[k + 6];
  }

  for (k = 0; k < 9; k++) {
    h1[k] = R[k];
  }

  for (k = 0; k < 3; k++) {
    a21 = h1[3 * k];
    H[3 * k] = a21;
    rtemp = 3 * (k + 3);
    H[rtemp] = -a21 * (float)obj->pKalmanPeriod;
    H_tmp = 3 * (k + 6);
    H[H_tmp] = iv[3 * k];
    xpageoffset = 3 * k + 1;
    H[xpageoffset] = h1[xpageoffset];
    H[rtemp + 1] = -h1[xpageoffset] * (float)obj->pKalmanPeriod;
    H[H_tmp + 1] = iv[xpageoffset];
    xpageoffset = 3 * k + 2;
    H[xpageoffset] = h1[xpageoffset];
    H[rtemp + 2] = -h1[xpageoffset] * (float)obj->pKalmanPeriod;
    H[H_tmp + 2] = iv[xpageoffset];
  }

  for (k = 0; k < 81; k++) {
    Qw[k] = obj->pQw[k];
  }

  for (k = 0; k < 3; k++) {
    for (xpageoffset = 0; xpageoffset < 9; xpageoffset++) {
      a21 = 0.0F;
      for (rtemp = 0; rtemp < 9; rtemp++) {
        a21 += H[k + 3 * rtemp] * Qw[rtemp + 9 * xpageoffset];
      }

      rtemp = k + 3 * xpageoffset;
      tmp_tmp[rtemp] = a21;
      b_tmp[xpageoffset + 9 * k] = H[rtemp];
    }
  }

  for (k = 0; k < 3; k++) {
    for (xpageoffset = 0; xpageoffset < 3; xpageoffset++) {
      a21 = 0.0F;
      for (rtemp = 0; rtemp < 9; rtemp++) {
        a21 += tmp_tmp[xpageoffset + 3 * rtemp] * b_tmp[rtemp + 9 * k];
      }

      h1[k + 3 * xpageoffset] = a21 + (float)obj->pQv[xpageoffset + 3 * k];
    }
  }

  for (k = 0; k < 9; k++) {
    for (xpageoffset = 0; xpageoffset < 3; xpageoffset++) {
      a21 = 0.0F;
      for (rtemp = 0; rtemp < 9; rtemp++) {
        a21 += Qw[k + 9 * rtemp] * b_tmp[rtemp + 9 * xpageoffset];
      }

      b_y[k + 9 * xpageoffset] = a21;
    }
  }

  r1 = 0;
  r2 = 1;
  r3 = 2;
  n = fabsf(h1[0]);
  a21 = fabsf(h1[1]);
  if (a21 > n) {
    n = a21;
    r1 = 1;
    r2 = 0;
  }

  if (fabsf(h1[2]) > n) {
    r1 = 2;
    r2 = 1;
    r3 = 0;
  }

  h1[r2] /= h1[r1];
  h1[r3] /= h1[r1];
  h1[r2 + 3] -= h1[r2] * h1[r1 + 3];
  h1[r3 + 3] -= h1[r3] * h1[r1 + 3];
  h1[r2 + 6] -= h1[r2] * h1[r1 + 6];
  h1[r3 + 6] -= h1[r3] * h1[r1 + 6];
  if (fabsf(h1[r3 + 3]) > fabsf(h1[r2 + 3])) {
    rtemp = r2;
    r2 = r3;
    r3 = rtemp;
  }

  h1[r3 + 3] /= h1[r2 + 3];
  h1[r3 + 6] -= h1[r3 + 3] * h1[r2 + 6];
  for (k = 0; k < 9; k++) {
    rtemp = k + 9 * r1;
    H[rtemp] = b_y[k] / h1[r1];
    H_tmp = k + 9 * r2;
    H[H_tmp] = b_y[k + 9] - H[rtemp] * h1[r1 + 3];
    xpageoffset = k + 9 * r3;
    H[xpageoffset] = b_y[k + 18] - H[rtemp] * h1[r1 + 6];
    H[H_tmp] /= h1[r2 + 3];
    H[xpageoffset] -= H[H_tmp] * h1[r2 + 6];
    H[xpageoffset] /= h1[r3 + 6];
    H[H_tmp] -= H[xpageoffset] * h1[r3 + 3];
    H[rtemp] -= H[xpageoffset] * h1[r3];
    H[rtemp] -= H[H_tmp] * h1[r2];
  }

  Reast[0] = (accelIn[0] + obj->pLinAccelPrior[0]) - xa;
  Reast[1] = (accelIn[1] + obj->pLinAccelPrior[1]) - deltaq_d;
  Reast[2] = (accelIn[2] + obj->pLinAccelPrior[2]) - deltaq_c;
  for (k = 0; k < 9; k++) {
    h1[k] = (H[k] * Reast[0] + H[k + 9] * Reast[1]) + H[k + 18] * Reast[2];
  }

  b_quaternioncg_quaternioncg(*(float (*)[3])&h1[0], &qerr.a, &qerr.b, &qerr.c,
    &qerr.d);
  qerr.b = -qerr.b;
  qerr.c = -qerr.c;
  qerr.d = -qerr.d;
  n = obj->pOrientPrior.a;
  a21 = obj->pOrientPrior.b;
  deltaq_c = obj->pOrientPrior.c;
  deltaq_d = obj->pOrientPrior.d;
  obj->pOrientPost.a = ((n * qerr.a - a21 * qerr.b) - deltaq_c * qerr.c) -
    deltaq_d * qerr.d;
  obj->pOrientPost.b = ((n * qerr.b + a21 * qerr.a) + deltaq_c * qerr.d) -
    deltaq_d * qerr.c;
  obj->pOrientPost.c = ((n * qerr.c - a21 * qerr.d) + deltaq_c * qerr.a) +
    deltaq_d * qerr.b;
  obj->pOrientPost.d = ((n * qerr.d + a21 * qerr.c) - deltaq_c * qerr.b) +
    deltaq_d * qerr.a;
  if (obj->pOrientPost.a < 0.0F) {
    qerr = obj->pOrientPost;
    qerr.a = -qerr.a;
    qerr.b = -qerr.b;
    qerr.c = -qerr.c;
    qerr.d = -qerr.d;
    obj->pOrientPost = qerr;
  }

  qerr = obj->pOrientPost;
  n = sqrtf(((qerr.a * qerr.a + qerr.b * qerr.b) + qerr.c * qerr.c) + qerr.d *
            qerr.d);
  qerr.a /= n;
  qerr.b /= n;
  qerr.c /= n;
  qerr.d /= n;
  obj->pOrientPost = qerr;
  obj->pGyroOffset[0] -= h1[3];
  obj->pLinAccelPost[0] = obj->pLinAccelPrior[0] - h1[6];
  obj->pGyroOffset[1] -= h1[4];
  obj->pLinAccelPost[1] = obj->pLinAccelPrior[1] - h1[7];
  obj->pGyroOffset[2] -= h1[5];
  obj->pLinAccelPost[2] = obj->pLinAccelPrior[2] - h1[8];
  for (k = 0; k < 9; k++) {
    a21 = H[k + 9];
    n = H[k + 18];
    for (xpageoffset = 0; xpageoffset < 9; xpageoffset++) {
      rtemp = k + 9 * xpageoffset;
      Ppost[rtemp] = Qw[rtemp] - ((H[k] * tmp_tmp[3 * xpageoffset] + a21 *
        tmp_tmp[3 * xpageoffset + 1]) + n * tmp_tmp[3 * xpageoffset + 2]);
    }
  }

  memset(&Qw[0], 0, 81U * sizeof(float));
  a = obj->pKalmanPeriod;
  a *= a;
  n = (float)(obj->GyroscopeDriftNoise + obj->GyroscopeNoise);
  b_a = -obj->pKalmanPeriod;
  c_a = obj->LinearAccelerationDecayFactor;
  c_a *= c_a;
  Qw[0] = Ppost[0] + (float)a * (Ppost[30] + n);
  a21 = Ppost[30] + (float)obj->GyroscopeDriftNoise;
  Qw[30] = a21;
  a21 *= (float)b_a;
  Qw[3] = a21;
  Qw[27] = a21;
  Qw[60] = (float)c_a * Ppost[60] + (float)obj->LinearAccelerationNoise;
  Qw[10] = Ppost[10] + (float)a * (Ppost[40] + n);
  a21 = Ppost[40] + (float)obj->GyroscopeDriftNoise;
  Qw[40] = a21;
  a21 *= (float)b_a;
  Qw[13] = a21;
  Qw[37] = a21;
  Qw[70] = (float)c_a * Ppost[70] + (float)obj->LinearAccelerationNoise;
  Qw[20] = Ppost[20] + (float)a * (Ppost[50] + n);
  a21 = Ppost[50] + (float)obj->GyroscopeDriftNoise;
  Qw[50] = a21;
  a21 *= (float)b_a;
  Qw[23] = a21;
  Qw[47] = a21;
  Qw[80] = (float)c_a * Ppost[80] + (float)obj->LinearAccelerationNoise;
  memcpy(&obj->pQw[0], &Qw[0], 81U * sizeof(float));
  qerr = obj->pOrientPost;
  *orientOut_a = qerr.a;
  *orientOut_b = qerr.b;
  *orientOut_c = qerr.c;
  *orientOut_d = qerr.d;
}

/*
 * File trailer for IMUFilterBase.c
 *
 * [EOF]
 */
