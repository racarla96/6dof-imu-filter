/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: fusion_types.h
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 17-Mar-2020 00:12:08
 */

#ifndef FUSION_TYPES_H
#define FUSION_TYPES_H

/* Include Files */
#include "rtwtypes.h"

/* Type Definitions */
#ifndef typedef_c_matlabshared_rotations_intern
#define typedef_c_matlabshared_rotations_intern

typedef struct {
  float a;
  float b;
  float c;
  float d;
} c_matlabshared_rotations_intern;

#endif                                 /*typedef_c_matlabshared_rotations_intern*/

#ifndef typedef_cell_wrap_3
#define typedef_cell_wrap_3

typedef struct {
  unsigned int f1[8];
} cell_wrap_3;

#endif                                 /*typedef_cell_wrap_3*/

#ifndef typedef_c_fusion_internal_coder_imufilt
#define typedef_c_fusion_internal_coder_imufilt

typedef struct {
  int isInitialized;
  boolean_T TunablePropsChanged;
  cell_wrap_3 inputVarSize[2];
  double AccelerometerNoise;
  double GyroscopeNoise;
  double GyroscopeDriftNoise;
  double LinearAccelerationNoise;
  double LinearAccelerationDecayFactor;
  float pQw[81];
  double pQv[9];
  c_matlabshared_rotations_intern pOrientPost;
  c_matlabshared_rotations_intern pOrientPrior;
  boolean_T pFirstTime;
  double pSensorPeriod;
  double pKalmanPeriod;
  float pGyroOffset[3];
  float pLinAccelPrior[3];
  float pLinAccelPost[3];
  float pInputPrototype[3];
} c_fusion_internal_coder_imufilt;

#endif                                 /*typedef_c_fusion_internal_coder_imufilt*/
#endif

/*
 * File trailer for fusion_types.h
 *
 * [EOF]
 */
