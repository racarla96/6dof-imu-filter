/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: _coder_fusion_api.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 17-Mar-2020 00:12:08
 */

/* Include Files */
#include "_coder_fusion_api.h"
#include "_coder_fusion_mex.h"

/* Variable Definitions */
emlrtCTX emlrtRootTLSGlobal = NULL;
emlrtContext emlrtContextGlobal = { true,/* bFirstTime */
  false,                               /* bInitialized */
  131483U,                             /* fVersionInfo */
  NULL,                                /* fErrorFunction */
  "fusion",                            /* fFunctionName */
  NULL,                                /* fRTCallStack */
  false,                               /* bDebugMode */
  { 2045744189U, 2170104910U, 2743257031U, 4284093946U },/* fSigWrd */
  NULL                                 /* fSigMem */
};

/* Function Declarations */
static real32_T (*b_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u,
  const emlrtMsgIdentifier *parentId))[3];
static const mxArray *b_emlrt_marshallOut(const real32_T u[3]);
static real32_T (*c_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src,
  const emlrtMsgIdentifier *msgId))[3];
static real32_T (*emlrt_marshallIn(const emlrtStack *sp, const mxArray *acc,
  const char_T *identifier))[3];
static const mxArray *emlrt_marshallOut(const emlrtStack *sp, const
  c_matlabshared_rotations_intern u);

/* Function Definitions */

/*
 * Arguments    : const emlrtStack *sp
 *                const mxArray *u
 *                const emlrtMsgIdentifier *parentId
 * Return Type  : real32_T (*)[3]
 */
static real32_T (*b_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u,
  const emlrtMsgIdentifier *parentId))[3]
{
  real32_T (*y)[3];
  y = c_emlrt_marshallIn(sp, emlrtAlias(u), parentId);
  emlrtDestroyArray(&u);
  return y;
}
/*
 * Arguments    : const real32_T u[3]
 * Return Type  : const mxArray *
 */
  static const mxArray *b_emlrt_marshallOut(const real32_T u[3])
{
  const mxArray *y;
  const mxArray *m;
  static const int32_T iv[2] = { 0, 0 };

  static const int32_T iv1[2] = { 1, 3 };

  y = NULL;
  m = emlrtCreateNumericArray(2, iv, mxSINGLE_CLASS, mxREAL);
  emlrtMxSetData((mxArray *)m, (void *)&u[0]);
  emlrtSetDimensions((mxArray *)m, iv1, 2);
  emlrtAssign(&y, m);
  return y;
}

/*
 * Arguments    : const emlrtStack *sp
 *                const mxArray *src
 *                const emlrtMsgIdentifier *msgId
 * Return Type  : real32_T (*)[3]
 */
static real32_T (*c_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src,
  const emlrtMsgIdentifier *msgId))[3]
{
  real32_T (*ret)[3];
  static const int32_T dims[2] = { 1, 3 };

  emlrtCheckBuiltInR2012b(sp, msgId, src, "single", false, 2U, dims);
  ret = (real32_T (*)[3])emlrtMxGetData(src);
  emlrtDestroyArray(&src);
  return ret;
}
/*
 * Arguments    : const emlrtStack *sp
 *                const mxArray *acc
 *                const char_T *identifier
 * Return Type  : real32_T (*)[3]
 */
  static real32_T (*emlrt_marshallIn(const emlrtStack *sp, const mxArray *acc,
  const char_T *identifier))[3]
{
  real32_T (*y)[3];
  emlrtMsgIdentifier thisId;
  thisId.fIdentifier = (const char *)identifier;
  thisId.fParent = NULL;
  thisId.bParentIsCell = false;
  y = b_emlrt_marshallIn(sp, emlrtAlias(acc), &thisId);
  emlrtDestroyArray(&acc);
  return y;
}

/*
 * Arguments    : const emlrtStack *sp
 *                const c_matlabshared_rotations_intern u
 * Return Type  : const mxArray *
 */
static const mxArray *emlrt_marshallOut(const emlrtStack *sp, const
  c_matlabshared_rotations_intern u)
{
  const mxArray *y;
  const mxArray *m;
  const mxArray *b_y;
  const mxArray *m1;
  const mxArray *propValues[4];
  const char * propNames[4] = { "a", "b", "c", "d" };

  const char * propClasses[4] = {
    "matlabshared.rotations.internal.quaternionBase",
    "matlabshared.rotations.internal.quaternionBase",
    "matlabshared.rotations.internal.quaternionBase",
    "matlabshared.rotations.internal.quaternionBase" };

  y = NULL;
  emlrtAssign(&y, emlrtCreateClassInstance(
    "matlabshared.rotations.internal.coder.quaternioncg"));
  m = NULL;
  b_y = NULL;
  m1 = emlrtCreateNumericMatrix(1, 1, mxSINGLE_CLASS, mxREAL);
  *(real32_T *)emlrtMxGetData(m1) = u.a;
  emlrtAssign(&b_y, m1);
  emlrtAssign(&m, b_y);
  propValues[0] = m;
  m = NULL;
  b_y = NULL;
  m1 = emlrtCreateNumericMatrix(1, 1, mxSINGLE_CLASS, mxREAL);
  *(real32_T *)emlrtMxGetData(m1) = u.b;
  emlrtAssign(&b_y, m1);
  emlrtAssign(&m, b_y);
  propValues[1] = m;
  m = NULL;
  b_y = NULL;
  m1 = emlrtCreateNumericMatrix(1, 1, mxSINGLE_CLASS, mxREAL);
  *(real32_T *)emlrtMxGetData(m1) = u.c;
  emlrtAssign(&b_y, m1);
  emlrtAssign(&m, b_y);
  propValues[2] = m;
  m = NULL;
  b_y = NULL;
  m1 = emlrtCreateNumericMatrix(1, 1, mxSINGLE_CLASS, mxREAL);
  *(real32_T *)emlrtMxGetData(m1) = u.d;
  emlrtAssign(&b_y, m1);
  emlrtAssign(&m, b_y);
  propValues[3] = m;
  emlrtSetAllProperties(sp, &y, 0, 4, propNames, propClasses, propValues);
  emlrtAssign(&y, emlrtConvertInstanceToRedirectSource(sp, y, 0,
    "matlabshared.rotations.internal.coder.quaternioncg"));
  return y;
}

/*
 * Arguments    : const mxArray * const prhs[2]
 *                int32_T nlhs
 *                const mxArray *plhs[2]
 * Return Type  : void
 */
void fusion_api(const mxArray * const prhs[2], int32_T nlhs, const mxArray *
                plhs[2])
{
  real32_T (*gyroRate)[3];
  real32_T (*acc)[3];
  real32_T (*gyro)[3];
  c_matlabshared_rotations_intern q;
  emlrtStack st = { NULL,              /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  st.tls = emlrtRootTLSGlobal;
  gyroRate = (real32_T (*)[3])mxMalloc(sizeof(real32_T [3]));

  /* Marshall function inputs */
  acc = emlrt_marshallIn(&st, emlrtAlias(prhs[0]), "acc");
  gyro = emlrt_marshallIn(&st, emlrtAlias(prhs[1]), "gyro");

  /* Invoke the target function */
  fusion(*acc, *gyro, &q, *gyroRate);

  /* Marshall function outputs */
  plhs[0] = emlrt_marshallOut(&st, q);
  if (nlhs > 1) {
    plhs[1] = b_emlrt_marshallOut(*gyroRate);
  }
}

/*
 * Arguments    : void
 * Return Type  : void
 */
void fusion_atexit(void)
{
  emlrtStack st = { NULL,              /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  mexFunctionCreateRootTLS();
  st.tls = emlrtRootTLSGlobal;
  emlrtEnterRtStackR2012b(&st);
  emlrtLeaveRtStackR2012b(&st);
  emlrtDestroyRootTLS(&emlrtRootTLSGlobal);
  fusion_xil_terminate();
  fusion_xil_shutdown();
  emlrtExitTimeCleanup(&emlrtContextGlobal);
}

/*
 * Arguments    : void
 * Return Type  : void
 */
void fusion_initialize(void)
{
  emlrtStack st = { NULL,              /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  mexFunctionCreateRootTLS();
  st.tls = emlrtRootTLSGlobal;
  emlrtClearAllocCountR2012b(&st, false, 0U, 0);
  emlrtEnterRtStackR2012b(&st);
  emlrtFirstTimeR2012b(emlrtRootTLSGlobal);
}

/*
 * Arguments    : void
 * Return Type  : void
 */
void fusion_terminate(void)
{
  emlrtStack st = { NULL,              /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  st.tls = emlrtRootTLSGlobal;
  emlrtLeaveRtStackR2012b(&st);
  emlrtDestroyRootTLS(&emlrtRootTLSGlobal);
}

/*
 * File trailer for _coder_fusion_api.c
 *
 * [EOF]
 */
