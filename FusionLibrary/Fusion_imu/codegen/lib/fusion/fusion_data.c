/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: fusion_data.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 17-Mar-2020 00:12:08
 */

/* Include Files */
#include "fusion_data.h"
#include "fusion.h"
#include "rt_nonfinite.h"

/* Variable Definitions */
boolean_T isInitialized_fusion = false;

/*
 * File trailer for fusion_data.c
 *
 * [EOF]
 */
