function [q,gyroRate] = fusion(acc,gyro)
FUSE = imufilter('SampleRate',400);
[q,gyroRate] = FUSE(acc,gyro);
end

