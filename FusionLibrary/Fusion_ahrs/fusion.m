function [euler,gyroRate] = fusion(acc,gyro,mag)
FUSE = ahrsfilter('SampleRate',400);
[q,gyroRate] = FUSE(acc,gyro,mag);
euler = eulerd(q,'XYZ','frame');
end

