/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: AHRSFilterBase.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 13-Mar-2020 00:10:46
 */

/* Include Files */
#include "AHRSFilterBase.h"
#include "fusion.h"
#include "fusion_data.h"
#include "fusion_rtwutil.h"
#include "mrdivide_helper.h"
#include "quaternioncg.h"
#include "rotmat.h"
#include "rt_nonfinite.h"
#include <math.h>
#include <string.h>

/* Function Definitions */

/*
 * Arguments    : c_fusion_internal_coder_ahrsfil *obj
 *                const float accelIn[3]
 *                const float gyroIn[3]
 *                const float magIn[3]
 *                float *orientOut_a
 *                float *orientOut_b
 *                float *orientOut_c
 *                float *orientOut_d
 *                float av[3]
 * Return Type  : void
 */
void AHRSFilterBase_stepImpl(c_fusion_internal_coder_ahrsfil *obj, const float
  accelIn[3], const float gyroIn[3], const float magIn[3], float *orientOut_a,
  float *orientOut_b, float *orientOut_c, float *orientOut_d, float av[3])
{
  c_matlabshared_rotations_intern qerr;
  float Reast[3];
  float gyroOffsetErr[3];
  float h1[9];
  float absxk;
  float t;
  float deltaq_c;
  float scale;
  float xa;
  float xb;
  float xc;
  int i;
  int xi;
  float Rprior[9];
  double a;
  int xpageoffset;
  boolean_T y[3];
  boolean_T b[9];
  boolean_T exitg1;
  float linAccelErr[3];
  float h2[9];
  boolean_T nanPageIdx;
  float b_h2[9];
  float H[72];
  float Qw[144];
  int i1;
  int H_tmp;
  float tmp_tmp[36];
  float b_tmp_tmp[72];
  float ze[6];
  float b_tmp[72];
  static const signed char b_iv[9] = { -1, 0, 0, 0, -1, 0, 0, 0, -1 };

  float xe_post[12];
  double b_a;
  double c_a;
  float Ppost[144];
  double d_a;
  av[0] = gyroIn[0] - obj->pGyroOffset[0];
  av[1] = gyroIn[1] - obj->pGyroOffset[1];
  av[2] = gyroIn[2] - obj->pGyroOffset[2];
  if (obj->pFirstTime) {
    obj->pFirstTime = false;
    Reast[0] = accelIn[1] * magIn[2] - accelIn[2] * magIn[1];
    Reast[1] = accelIn[2] * magIn[0] - accelIn[0] * magIn[2];
    Reast[2] = accelIn[0] * magIn[1] - accelIn[1] * magIn[0];
    h1[6] = accelIn[0];
    h1[3] = Reast[0];
    h1[7] = accelIn[1];
    h1[4] = Reast[1];
    h1[8] = accelIn[2];
    h1[5] = Reast[2];
    h1[0] = Reast[1] * accelIn[2] - Reast[2] * accelIn[1];
    h1[1] = Reast[2] * accelIn[0] - Reast[0] * accelIn[2];
    h1[2] = Reast[0] * accelIn[1] - Reast[1] * accelIn[0];
    for (i = 0; i < 9; i++) {
      Rprior[i] = h1[i] * h1[i];
    }

    for (xi = 0; xi < 3; xi++) {
      xpageoffset = xi * 3;
      Reast[xi] = sqrtf((Rprior[xpageoffset] + Rprior[xpageoffset + 1]) +
                        Rprior[xpageoffset + 2]);
    }

    for (i = 0; i < 9; i++) {
      Rprior[i] = h1[i];
    }

    for (xpageoffset = 0; xpageoffset < 3; xpageoffset++) {
      h1[3 * xpageoffset] = Rprior[3 * xpageoffset] / Reast[xpageoffset];
      xi = 3 * xpageoffset + 1;
      h1[xi] = Rprior[xi] / Reast[xpageoffset];
      xi = 3 * xpageoffset + 2;
      h1[xi] = Rprior[xi] / Reast[xpageoffset];
    }

    for (i = 0; i < 9; i++) {
      b[i] = rtIsNaNF(h1[i]);
    }

    y[0] = false;
    y[1] = false;
    y[2] = false;
    xi = 1;
    exitg1 = false;
    while ((!exitg1) && (xi <= 3)) {
      if (b[xi - 1]) {
        y[0] = true;
        exitg1 = true;
      } else {
        xi++;
      }
    }

    xi = 4;
    exitg1 = false;
    while ((!exitg1) && (xi <= 6)) {
      if (b[xi - 1]) {
        y[1] = true;
        exitg1 = true;
      } else {
        xi++;
      }
    }

    xi = 7;
    exitg1 = false;
    while ((!exitg1) && (xi <= 9)) {
      if (b[xi - 1]) {
        y[2] = true;
        exitg1 = true;
      } else {
        xi++;
      }
    }

    nanPageIdx = false;
    xpageoffset = 0;
    exitg1 = false;
    while ((!exitg1) && (xpageoffset < 3)) {
      if (y[xpageoffset]) {
        nanPageIdx = true;
        exitg1 = true;
      } else {
        xpageoffset++;
      }
    }

    if (nanPageIdx) {
      for (i = 0; i < 9; i++) {
        h1[i] = 0.0F;
      }

      h1[0] = 1.0F;
      h1[4] = 1.0F;
      h1[8] = 1.0F;
    }

    quaternioncg_quaternioncg(h1, &obj->pOrientPost.a, &obj->pOrientPost.b,
      &obj->pOrientPost.c, &obj->pOrientPost.d);
  }

  qerr = obj->pOrientPost;
  gyroOffsetErr[0] = (gyroIn[0] - obj->pGyroOffset[0]) * (float)
    obj->pSensorPeriod;
  gyroOffsetErr[1] = (gyroIn[1] - obj->pGyroOffset[1]) * (float)
    obj->pSensorPeriod;
  gyroOffsetErr[2] = (gyroIn[2] - obj->pGyroOffset[2]) * (float)
    obj->pSensorPeriod;
  b_quaternioncg_quaternioncg(gyroOffsetErr, &absxk, &t, &deltaq_c, &scale);
  xa = qerr.a;
  xb = qerr.b;
  xc = qerr.c;
  qerr.a = ((qerr.a * absxk - qerr.b * t) - qerr.c * deltaq_c) - qerr.d * scale;
  qerr.b = ((xa * t + qerr.b * absxk) + qerr.c * scale) - qerr.d * deltaq_c;
  qerr.c = ((xa * deltaq_c - xb * scale) + qerr.c * absxk) + qerr.d * t;
  qerr.d = ((xa * scale + xb * deltaq_c) - xc * t) + qerr.d * absxk;
  if (qerr.a < 0.0F) {
    qerr.a = -qerr.a;
    qerr.b = -qerr.b;
    qerr.c = -qerr.c;
    qerr.d = -qerr.d;
  }

  obj->pOrientPrior = qerr;
  quaternionBase_rotmat(obj->pOrientPrior.a, obj->pOrientPrior.b,
                        obj->pOrientPrior.c, obj->pOrientPrior.d, Rprior);
  a = obj->LinearAccelerationDecayFactor;
  obj->pLinAccelPrior[0] = (float)a * obj->pLinAccelPost[0];
  Reast[0] = (accelIn[0] + obj->pLinAccelPrior[0]) - Rprior[6];
  gyroOffsetErr[0] = obj->pMagVec[0];
  obj->pLinAccelPrior[1] = (float)a * obj->pLinAccelPost[1];
  Reast[1] = (accelIn[1] + obj->pLinAccelPrior[1]) - Rprior[7];
  gyroOffsetErr[1] = obj->pMagVec[1];
  obj->pLinAccelPrior[2] = (float)a * obj->pLinAccelPost[2];
  Reast[2] = (accelIn[2] + obj->pLinAccelPrior[2]) - Rprior[8];
  gyroOffsetErr[2] = obj->pMagVec[2];
  for (i = 0; i < 3; i++) {
    linAccelErr[i] = (Rprior[i] * gyroOffsetErr[0] + Rprior[i + 3] *
                      gyroOffsetErr[1]) + Rprior[i + 6] * gyroOffsetErr[2];
  }

  for (i = 0; i < 9; i++) {
    h1[i] = 0.0F;
  }

  h1[3] = Rprior[8];
  h1[6] = -Rprior[7];
  h1[7] = Rprior[6];
  for (i = 0; i < 3; i++) {
    h2[3 * i] = h1[3 * i];
    xi = 3 * i + 1;
    h2[xi] = h1[xi] - h1[i + 3];
    xi = 3 * i + 2;
    h2[xi] = h1[xi] - h1[i + 6];
  }

  for (i = 0; i < 9; i++) {
    h1[i] = h2[i];
    h2[i] = 0.0F;
  }

  h2[3] = linAccelErr[2];
  h2[6] = -linAccelErr[1];
  h2[7] = linAccelErr[0];
  for (i = 0; i < 3; i++) {
    b_h2[3 * i] = h2[3 * i] - h2[i];
    xi = 3 * i + 1;
    b_h2[xi] = h2[xi] - h2[i + 3];
    xi = 3 * i + 2;
    b_h2[xi] = h2[xi] - h2[i + 6];
  }

  for (i = 0; i < 9; i++) {
    h2[i] = b_h2[i];
  }

  for (i = 0; i < 3; i++) {
    t = h1[3 * i];
    H[6 * i] = t;
    xi = 6 * (i + 3);
    H[xi] = -t * (float)obj->pKalmanPeriod;
    xpageoffset = 6 * (i + 6);
    H[xpageoffset] = iv[3 * i];
    H_tmp = 6 * (i + 9);
    H[H_tmp] = 0.0F;
    t = h2[3 * i];
    H[6 * i + 3] = t;
    H[xi + 3] = -t * (float)obj->pKalmanPeriod;
    H[xpageoffset + 3] = 0.0F;
    H[H_tmp + 3] = b_iv[3 * i];
    i1 = 3 * i + 1;
    H[6 * i + 1] = h1[i1];
    H[xi + 1] = -h1[i1] * (float)obj->pKalmanPeriod;
    H[xpageoffset + 1] = iv[i1];
    H[H_tmp + 1] = 0.0F;
    H[6 * i + 4] = h2[i1];
    H[xi + 4] = -h2[i1] * (float)obj->pKalmanPeriod;
    H[xpageoffset + 4] = 0.0F;
    H[H_tmp + 4] = b_iv[i1];
    i1 = 3 * i + 2;
    H[6 * i + 2] = h1[i1];
    H[xi + 2] = -h1[i1] * (float)obj->pKalmanPeriod;
    H[xpageoffset + 2] = iv[i1];
    H[H_tmp + 2] = 0.0F;
    H[6 * i + 5] = h2[i1];
    H[xi + 5] = -h2[i1] * (float)obj->pKalmanPeriod;
    H[xpageoffset + 5] = 0.0F;
    H[H_tmp + 5] = b_iv[i1];
  }

  for (i = 0; i < 144; i++) {
    Qw[i] = obj->pQw[i];
  }

  for (i = 0; i < 6; i++) {
    for (i1 = 0; i1 < 12; i1++) {
      t = 0.0F;
      for (xi = 0; xi < 12; xi++) {
        t += H[i + 6 * xi] * Qw[xi + 12 * i1];
      }

      xi = i + 6 * i1;
      b_tmp_tmp[xi] = t;
      b_tmp[i1 + 12 * i] = H[xi];
    }
  }

  for (i = 0; i < 12; i++) {
    for (i1 = 0; i1 < 6; i1++) {
      t = 0.0F;
      for (xi = 0; xi < 12; xi++) {
        t += Qw[i + 12 * xi] * b_tmp[xi + 12 * i1];
      }

      H[i + 12 * i1] = t;
    }
  }

  for (i = 0; i < 6; i++) {
    for (i1 = 0; i1 < 6; i1++) {
      t = 0.0F;
      for (xi = 0; xi < 12; xi++) {
        t += b_tmp_tmp[i1 + 6 * xi] * b_tmp[xi + 12 * i];
      }

      tmp_tmp[i + 6 * i1] = t + (float)obj->pQv[i1 + 6 * i];
    }
  }

  mrdiv(H, tmp_tmp);
  for (i = 0; i < 3; i++) {
    ze[i] = Reast[i];
    ze[i + 3] = magIn[i] - ((Rprior[i] * gyroOffsetErr[0] + Rprior[i + 3] *
      gyroOffsetErr[1]) + Rprior[i + 6] * gyroOffsetErr[2]);
  }

  deltaq_c = 0.0F;
  scale = 1.29246971E-26F;
  for (xpageoffset = 0; xpageoffset < 3; xpageoffset++) {
    t = 0.0F;
    for (i = 0; i < 6; i++) {
      t += H[(xpageoffset + 12 * i) + 9] * ze[i];
    }

    absxk = fabsf(t);
    if (absxk > scale) {
      t = scale / absxk;
      deltaq_c = deltaq_c * t * t + 1.0F;
      scale = absxk;
    } else {
      t = absxk / scale;
      deltaq_c += t * t;
    }
  }

  deltaq_c = scale * sqrtf(deltaq_c);
  a = obj->ExpectedMagneticFieldStrength;
  nanPageIdx = (deltaq_c * deltaq_c > 4.0 * (a * a));
  if (nanPageIdx) {
    for (i = 0; i < 9; i++) {
      Rprior[i] = (H[i] * Reast[0] + H[i + 12] * Reast[1]) + H[i + 24] * Reast[2];
    }

    Reast[0] = Rprior[0];
    gyroOffsetErr[0] = Rprior[3];
    linAccelErr[0] = Rprior[6];
    Reast[1] = Rprior[1];
    gyroOffsetErr[1] = Rprior[4];
    linAccelErr[1] = Rprior[7];
    Reast[2] = Rprior[2];
    gyroOffsetErr[2] = Rprior[5];
    linAccelErr[2] = Rprior[8];
  } else {
    for (i = 0; i < 12; i++) {
      t = 0.0F;
      for (i1 = 0; i1 < 6; i1++) {
        t += H[i + 12 * i1] * ze[i1];
      }

      xe_post[i] = t;
    }

    Reast[0] = xe_post[0];
    gyroOffsetErr[0] = xe_post[3];
    linAccelErr[0] = xe_post[6];
    Reast[1] = xe_post[1];
    gyroOffsetErr[1] = xe_post[4];
    linAccelErr[1] = xe_post[7];
    Reast[2] = xe_post[2];
    gyroOffsetErr[2] = xe_post[5];
    linAccelErr[2] = xe_post[8];
  }

  b_quaternioncg_quaternioncg(Reast, &qerr.a, &qerr.b, &qerr.c, &qerr.d);
  qerr.b = -qerr.b;
  qerr.c = -qerr.c;
  qerr.d = -qerr.d;
  absxk = obj->pOrientPrior.a;
  t = obj->pOrientPrior.b;
  deltaq_c = obj->pOrientPrior.c;
  scale = obj->pOrientPrior.d;
  obj->pOrientPost.a = ((absxk * qerr.a - t * qerr.b) - deltaq_c * qerr.c) -
    scale * qerr.d;
  obj->pOrientPost.b = ((absxk * qerr.b + t * qerr.a) + deltaq_c * qerr.d) -
    scale * qerr.c;
  obj->pOrientPost.c = ((absxk * qerr.c - t * qerr.d) + deltaq_c * qerr.a) +
    scale * qerr.b;
  obj->pOrientPost.d = ((absxk * qerr.d + t * qerr.c) - deltaq_c * qerr.b) +
    scale * qerr.a;
  if (obj->pOrientPost.a < 0.0F) {
    qerr = obj->pOrientPost;
    qerr.a = -qerr.a;
    qerr.b = -qerr.b;
    qerr.c = -qerr.c;
    qerr.d = -qerr.d;
    obj->pOrientPost = qerr;
  }

  qerr = obj->pOrientPost;
  absxk = sqrtf(((qerr.a * qerr.a + qerr.b * qerr.b) + qerr.c * qerr.c) + qerr.d
                * qerr.d);
  qerr.a /= absxk;
  qerr.b /= absxk;
  qerr.c /= absxk;
  qerr.d /= absxk;
  obj->pOrientPost = qerr;
  quaternionBase_rotmat(obj->pOrientPost.a, obj->pOrientPost.b,
                        obj->pOrientPost.c, obj->pOrientPost.d, Rprior);
  obj->pGyroOffset[0] -= gyroOffsetErr[0];
  obj->pLinAccelPost[0] = obj->pLinAccelPrior[0] - linAccelErr[0];
  obj->pGyroOffset[1] -= gyroOffsetErr[1];
  obj->pLinAccelPost[1] = obj->pLinAccelPrior[1] - linAccelErr[1];
  obj->pGyroOffset[2] -= gyroOffsetErr[2];
  obj->pLinAccelPost[2] = obj->pLinAccelPrior[2] - linAccelErr[2];
  if (!nanPageIdx) {
    for (i = 0; i < 3; i++) {
      t = 0.0F;
      for (i1 = 0; i1 < 6; i1++) {
        t += H[(i + 12 * i1) + 9] * ze[i1];
      }

      gyroOffsetErr[i] = t;
    }

    for (i = 0; i < 3; i++) {
      Reast[i] = obj->pMagVec[i] - ((Rprior[3 * i] * gyroOffsetErr[0] + Rprior[3
        * i + 1] * gyroOffsetErr[1]) + Rprior[3 * i + 2] * gyroOffsetErr[2]);
    }

    absxk = rt_atan2f_snf(Reast[2], Reast[0]);
    if (absxk < -1.5707963267948966) {
      absxk = -1.57079637F;
    }

    if (absxk > 1.5707963267948966) {
      absxk = 1.57079637F;
    }

    obj->pMagVec[0] = 0.0F;
    obj->pMagVec[1] = 0.0F;
    obj->pMagVec[2] = 0.0F;
    obj->pMagVec[0] = cosf(absxk);
    obj->pMagVec[2] = sinf(absxk);
    obj->pMagVec[0] *= (float)obj->ExpectedMagneticFieldStrength;
    obj->pMagVec[1] *= (float)obj->ExpectedMagneticFieldStrength;
    obj->pMagVec[2] *= (float)obj->ExpectedMagneticFieldStrength;
  }

  for (i = 0; i < 12; i++) {
    for (i1 = 0; i1 < 12; i1++) {
      t = 0.0F;
      for (xi = 0; xi < 6; xi++) {
        t += H[i + 12 * xi] * b_tmp_tmp[xi + 6 * i1];
      }

      xi = i + 12 * i1;
      Ppost[xi] = Qw[xi] - t;
    }
  }

  memset(&Qw[0], 0, 144U * sizeof(float));
  a = obj->pKalmanPeriod;
  a *= a;
  absxk = (float)(obj->GyroscopeDriftNoise + obj->GyroscopeNoise);
  b_a = -obj->pKalmanPeriod;
  c_a = obj->LinearAccelerationDecayFactor;
  c_a *= c_a;
  d_a = obj->MagneticDisturbanceDecayFactor;
  d_a *= d_a;
  Qw[0] = Ppost[0] + (float)a * (Ppost[39] + absxk);
  t = Ppost[39] + (float)obj->GyroscopeDriftNoise;
  Qw[39] = t;
  t *= (float)b_a;
  Qw[3] = t;
  Qw[36] = t;
  Qw[78] = (float)c_a * Ppost[78] + (float)obj->LinearAccelerationNoise;
  Qw[117] = (float)d_a * Ppost[117] + (float)obj->MagneticDisturbanceNoise;
  Qw[13] = Ppost[13] + (float)a * (Ppost[52] + absxk);
  t = Ppost[52] + (float)obj->GyroscopeDriftNoise;
  Qw[52] = t;
  t *= (float)b_a;
  Qw[16] = t;
  Qw[49] = t;
  Qw[91] = (float)c_a * Ppost[91] + (float)obj->LinearAccelerationNoise;
  Qw[130] = (float)d_a * Ppost[130] + (float)obj->MagneticDisturbanceNoise;
  Qw[26] = Ppost[26] + (float)a * (Ppost[65] + absxk);
  t = Ppost[65] + (float)obj->GyroscopeDriftNoise;
  Qw[65] = t;
  t *= (float)b_a;
  Qw[29] = t;
  Qw[62] = t;
  Qw[104] = (float)c_a * Ppost[104] + (float)obj->LinearAccelerationNoise;
  Qw[143] = (float)d_a * Ppost[143] + (float)obj->MagneticDisturbanceNoise;
  memcpy(&obj->pQw[0], &Qw[0], 144U * sizeof(float));
  qerr = obj->pOrientPost;
  *orientOut_a = qerr.a;
  *orientOut_b = qerr.b;
  *orientOut_c = qerr.c;
  *orientOut_d = qerr.d;
}

/*
 * File trailer for AHRSFilterBase.c
 *
 * [EOF]
 */
