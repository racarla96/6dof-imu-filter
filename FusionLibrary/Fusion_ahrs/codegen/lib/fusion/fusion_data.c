/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: fusion_data.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 13-Mar-2020 00:10:46
 */

/* Include Files */
#include "fusion_data.h"
#include "fusion.h"
#include "rt_nonfinite.h"

/* Variable Definitions */
const signed char iv[9] = { 1, 0, 0, 0, 1, 0, 0, 0, 1 };

boolean_T isInitialized_fusion = false;

/*
 * File trailer for fusion_data.c
 *
 * [EOF]
 */
