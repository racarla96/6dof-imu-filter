/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: fusion_terminate.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 13-Mar-2020 00:10:46
 */

/* Include Files */
#include "fusion_terminate.h"
#include "fusion.h"
#include "fusion_data.h"
#include "rt_nonfinite.h"

/* Function Definitions */

/*
 * Arguments    : void
 * Return Type  : void
 */
void fusion_terminate(void)
{
  /* (no terminate code required) */
  isInitialized_fusion = false;
}

/*
 * File trailer for fusion_terminate.c
 *
 * [EOF]
 */
