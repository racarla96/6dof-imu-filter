/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: quaternioncg.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 13-Mar-2020 00:10:46
 */

/* Include Files */
#include "quaternioncg.h"
#include "fusion.h"
#include "rt_nonfinite.h"
#include <math.h>

/* Function Definitions */

/*
 * Arguments    : const float varargin_1[3]
 *                float *obj_a
 *                float *obj_b
 *                float *obj_c
 *                float *obj_d
 * Return Type  : void
 */
void b_quaternioncg_quaternioncg(const float varargin_1[3], float *obj_a, float *
  obj_b, float *obj_c, float *obj_d)
{
  float theta;
  float st;
  *obj_a = 1.0F;
  *obj_b = 0.0F;
  *obj_c = 0.0F;
  *obj_d = 0.0F;
  theta = sqrtf((varargin_1[0] * varargin_1[0] + varargin_1[1] * varargin_1[1])
                + varargin_1[2] * varargin_1[2]);
  st = sinf(theta / 2.0F);
  if (theta != 0.0F) {
    *obj_a = cosf(theta / 2.0F);
    *obj_b = varargin_1[0] / theta * st;
    *obj_c = varargin_1[1] / theta * st;
    *obj_d = varargin_1[2] / theta * st;
  }
}

/*
 * Arguments    : const float varargin_1[9]
 *                float *obj_a
 *                float *obj_b
 *                float *obj_c
 *                float *obj_d
 * Return Type  : void
 */
void quaternioncg_quaternioncg(const float varargin_1[9], float *obj_a, float
  *obj_b, float *obj_c, float *obj_d)
{
  float pd;
  float psquared[4];
  int idx;
  int k;
  boolean_T exitg1;
  int i;
  float pa;
  pd = (varargin_1[0] + varargin_1[4]) + varargin_1[8];
  psquared[0] = (2.0F * pd + 1.0F) - pd;
  psquared[1] = (2.0F * varargin_1[0] + 1.0F) - pd;
  psquared[2] = (2.0F * varargin_1[4] + 1.0F) - pd;
  psquared[3] = (2.0F * varargin_1[8] + 1.0F) - pd;
  if (!rtIsNaNF(psquared[0])) {
    idx = 1;
  } else {
    idx = 0;
    k = 2;
    exitg1 = false;
    while ((!exitg1) && (k < 5)) {
      if (!rtIsNaNF(psquared[k - 1])) {
        idx = k;
        exitg1 = true;
      } else {
        k++;
      }
    }
  }

  if (idx == 0) {
    pd = psquared[0];
    idx = 1;
  } else {
    pd = psquared[idx - 1];
    i = idx + 1;
    for (k = i; k < 5; k++) {
      pa = psquared[k - 1];
      if (pd < pa) {
        pd = pa;
        idx = k;
      }
    }
  }

  switch (idx) {
   case 1:
    pa = sqrtf(pd);
    *obj_a = 0.5F * pa;
    pd = 0.5F / pa;
    *obj_b = pd * (varargin_1[7] - varargin_1[5]);
    *obj_c = pd * (varargin_1[2] - varargin_1[6]);
    *obj_d = pd * (varargin_1[3] - varargin_1[1]);
    break;

   case 2:
    pd = sqrtf(pd);
    *obj_b = 0.5F * pd;
    pd = 0.5F / pd;
    *obj_a = pd * (varargin_1[7] - varargin_1[5]);
    *obj_c = pd * (varargin_1[3] + varargin_1[1]);
    *obj_d = pd * (varargin_1[2] + varargin_1[6]);
    break;

   case 3:
    pd = sqrtf(pd);
    *obj_c = 0.5F * pd;
    pd = 0.5F / pd;
    *obj_a = pd * (varargin_1[2] - varargin_1[6]);
    *obj_b = pd * (varargin_1[3] + varargin_1[1]);
    *obj_d = pd * (varargin_1[7] + varargin_1[5]);
    break;

   default:
    pd = sqrtf(pd);
    *obj_d = 0.5F * pd;
    pd = 0.5F / pd;
    *obj_a = pd * (varargin_1[3] - varargin_1[1]);
    *obj_b = pd * (varargin_1[2] + varargin_1[6]);
    *obj_c = pd * (varargin_1[7] + varargin_1[5]);
    break;
  }

  if (*obj_a < 0.0F) {
    *obj_a = -*obj_a;
    *obj_b = -*obj_b;
    *obj_c = -*obj_c;
    *obj_d = -*obj_d;
  }
}

/*
 * File trailer for quaternioncg.c
 *
 * [EOF]
 */
