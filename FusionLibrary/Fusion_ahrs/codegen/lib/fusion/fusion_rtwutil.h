/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: fusion_rtwutil.h
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 13-Mar-2020 00:10:46
 */

#ifndef FUSION_RTWUTIL_H
#define FUSION_RTWUTIL_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "fusion_types.h"

/* Function Declarations */
extern float rt_atan2f_snf(float u0, float u1);

#endif

/*
 * File trailer for fusion_rtwutil.h
 *
 * [EOF]
 */
