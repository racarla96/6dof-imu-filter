/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: AHRSFilterBase.h
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 13-Mar-2020 00:10:46
 */

#ifndef AHRSFILTERBASE_H
#define AHRSFILTERBASE_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "fusion_types.h"

/* Function Declarations */
extern void AHRSFilterBase_stepImpl(c_fusion_internal_coder_ahrsfil *obj, const
  float accelIn[3], const float gyroIn[3], const float magIn[3], float
  *orientOut_a, float *orientOut_b, float *orientOut_c, float *orientOut_d,
  float av[3]);

#endif

/*
 * File trailer for AHRSFilterBase.h
 *
 * [EOF]
 */
