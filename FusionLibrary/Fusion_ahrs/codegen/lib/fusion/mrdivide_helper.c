/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: mrdivide_helper.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 13-Mar-2020 00:10:46
 */

/* Include Files */
#include "mrdivide_helper.h"
#include "fusion.h"
#include "rt_nonfinite.h"
#include <math.h>
#include <string.h>

/* Function Definitions */

/*
 * Arguments    : float A[72]
 *                const float B[36]
 * Return Type  : void
 */
void mrdiv(float A[72], const float B[36])
{
  float b_A[36];
  int i;
  int j;
  signed char ipiv[6];
  int mmj_tmp;
  int b;
  int iy;
  int jj;
  int jA;
  int jp1j;
  int k;
  int kBcol;
  float smax;
  int ix;
  float s;
  memcpy(&b_A[0], &B[0], 36U * sizeof(float));
  for (i = 0; i < 6; i++) {
    ipiv[i] = (signed char)(i + 1);
  }

  for (j = 0; j < 5; j++) {
    mmj_tmp = 4 - j;
    b = j * 7;
    jj = j * 7;
    jp1j = b + 2;
    jA = 6 - j;
    kBcol = 0;
    ix = b;
    smax = fabsf(b_A[jj]);
    for (k = 2; k <= jA; k++) {
      ix++;
      s = fabsf(b_A[ix]);
      if (s > smax) {
        kBcol = k - 1;
        smax = s;
      }
    }

    if (b_A[jj + kBcol] != 0.0F) {
      if (kBcol != 0) {
        iy = j + kBcol;
        ipiv[j] = (signed char)(iy + 1);
        ix = j;
        for (k = 0; k < 6; k++) {
          smax = b_A[ix];
          b_A[ix] = b_A[iy];
          b_A[iy] = smax;
          ix += 6;
          iy += 6;
        }
      }

      i = (jj - j) + 6;
      for (ix = jp1j; ix <= i; ix++) {
        b_A[ix - 1] /= b_A[jj];
      }
    }

    iy = b + 6;
    jA = jj;
    for (kBcol = 0; kBcol <= mmj_tmp; kBcol++) {
      smax = b_A[iy];
      if (b_A[iy] != 0.0F) {
        ix = jj + 1;
        i = jA + 8;
        jp1j = (jA - j) + 12;
        for (b = i; b <= jp1j; b++) {
          b_A[b - 1] += b_A[ix] * -smax;
          ix++;
        }
      }

      iy += 6;
      jA += 6;
    }
  }

  for (j = 0; j < 6; j++) {
    iy = 12 * j - 1;
    jA = 6 * j;
    for (k = 0; k < j; k++) {
      kBcol = 12 * k;
      i = k + jA;
      if (b_A[i] != 0.0F) {
        for (ix = 0; ix < 12; ix++) {
          jp1j = (ix + iy) + 1;
          A[jp1j] -= b_A[i] * A[ix + kBcol];
        }
      }
    }

    smax = 1.0F / b_A[j + jA];
    for (ix = 0; ix < 12; ix++) {
      i = (ix + iy) + 1;
      A[i] *= smax;
    }
  }

  for (j = 5; j >= 0; j--) {
    iy = 12 * j - 1;
    jA = 6 * j - 1;
    i = j + 2;
    for (k = i; k < 7; k++) {
      kBcol = 12 * (k - 1);
      jp1j = k + jA;
      if (b_A[jp1j] != 0.0F) {
        for (ix = 0; ix < 12; ix++) {
          b = (ix + iy) + 1;
          A[b] -= b_A[jp1j] * A[ix + kBcol];
        }
      }
    }
  }

  for (iy = 4; iy >= 0; iy--) {
    if (ipiv[iy] != iy + 1) {
      for (jA = 0; jA < 12; jA++) {
        kBcol = jA + 12 * iy;
        smax = A[kBcol];
        i = jA + 12 * (ipiv[iy] - 1);
        A[kBcol] = A[i];
        A[i] = smax;
      }
    }
  }
}

/*
 * File trailer for mrdivide_helper.c
 *
 * [EOF]
 */
