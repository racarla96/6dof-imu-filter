/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: fusion.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 13-Mar-2020 00:10:46
 */

/* Include Files */
#include "fusion.h"
#include "AHRSFilterBase.h"
#include "fusion_data.h"
#include "fusion_initialize.h"
#include "fusion_rtwutil.h"
#include "rt_nonfinite.h"
#include <math.h>
#include <string.h>

/* Function Definitions */

/*
 * Arguments    : const float acc[3]
 *                const float gyro[3]
 *                const float mag[3]
 *                float euler[3]
 *                float gyroRate[3]
 * Return Type  : void
 */
void fusion(const float acc[3], const float gyro[3], const float mag[3], float
            euler[3], float gyroRate[3])
{
  c_fusion_internal_coder_ahrsfil FUSE;
  int i;
  static const float fv[144] = { 6.09234849E-6F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F,
    0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 6.09234849E-6F, 0.0F, 0.0F, 0.0F,
    0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 6.09234849E-6F, 0.0F,
    0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F,
    7.61543561E-5F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F,
    0.0F, 0.0F, 7.61543561E-5F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F,
    0.0F, 0.0F, 0.0F, 0.0F, 7.61543561E-5F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F,
    0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.00962361F, 0.0F, 0.0F, 0.0F, 0.0F,
    0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.00962361F, 0.0F, 0.0F,
    0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.00962361F,
    0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.6F,
    0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.6F,
    0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.6F
  };

  signed char i1;
  int i2;
  float varargout_1_a;
  float varargout_1_b;
  float varargout_1_c;
  float varargout_1_d;
  float n;
  float x;
  float tmp;
  float b_x;
  float c_x;
  float qa;
  float qb;
  float qc;
  if (isInitialized_fusion == false) {
    fusion_initialize();
  }

  FUSE.MagnetometerNoise = 0.1;
  FUSE.MagneticDisturbanceNoise = 0.5;
  FUSE.MagneticDisturbanceDecayFactor = 0.5;
  FUSE.ExpectedMagneticFieldStrength = 50.0;
  FUSE.AccelerometerNoise = 0.0001924722;
  FUSE.GyroscopeNoise = 9.1385E-5;
  FUSE.GyroscopeDriftNoise = 3.0462E-13;
  FUSE.LinearAccelerationNoise = 0.0096236100000000012;
  FUSE.LinearAccelerationDecayFactor = 0.5;
  FUSE.isInitialized = 1;
  FUSE.pInputPrototype[0] = acc[0]; //
  FUSE.pInputPrototype[1] = acc[1]; //
  FUSE.pInputPrototype[2] = acc[2]; //
  FUSE.pSensorPeriod = 0.0025;
  FUSE.pKalmanPeriod = 0.0025;
  FUSE.TunablePropsChanged = false;
  FUSE.pOrientPost.a = 1.0F; //
  FUSE.pOrientPost.b = 0.0F; //
  FUSE.pOrientPost.c = 0.0F; //
  FUSE.pOrientPost.d = 0.0F; //
  FUSE.pGyroOffset[0] = 0.0F;
  FUSE.pGyroOffset[1] = 0.0F;
  FUSE.pGyroOffset[2] = 0.0F;

  FUSE.pMagVec[0] = 50.0F; //
  FUSE.pMagVec[1] = 0.0F;
  FUSE.pMagVec[2] = 0.0F;

  memset(&FUSE.pQv[0], 0, 36U * sizeof(double));
  for (i = 0; i < 3; i++) {
    i1 = iv[3 * i];
    FUSE.pQv[6 * i] = 0.0098160827711562537 * (double)i1;
    i2 = 6 * (i + 3);
    FUSE.pQv[i2 + 3] = 0.60000000057115621 * (double)i1;
    i1 = iv[3 * i + 1];
    FUSE.pQv[6 * i + 1] = 0.0098160827711562537 * (double)i1;
    FUSE.pQv[i2 + 4] = 0.60000000057115621 * (double)i1;
    i1 = iv[3 * i + 2];
    FUSE.pQv[6 * i + 2] = 0.0098160827711562537 * (double)i1;
    FUSE.pQv[i2 + 5] = 0.60000000057115621 * (double)i1;
  }

  memcpy(&FUSE.pQw[0], &fv[0], 144U * sizeof(float));
  FUSE.pLinAccelPost[0] = 0.0F;
  FUSE.pLinAccelPost[1] = 0.0F;
  FUSE.pLinAccelPost[2] = 0.0F;
  FUSE.pFirstTime = true;
  AHRSFilterBase_stepImpl(&FUSE, acc, gyro, mag, &varargout_1_a, &varargout_1_b,
    &varargout_1_c, &varargout_1_d, gyroRate);
  n = sqrtf(((varargout_1_a * varargout_1_a + varargout_1_b * varargout_1_b) +
             varargout_1_c * varargout_1_c) + varargout_1_d * varargout_1_d);
  x = varargout_1_a;
  varargout_1_a /= n;
  tmp = varargout_1_b;
  varargout_1_b /= n;
  b_x = varargout_1_c;
  varargout_1_c /= n;
  c_x = varargout_1_d;
  varargout_1_d /= n;
  qa = x / n;
  qb = tmp / n;
  qc = b_x / n;
  b_x = c_x / n;
  tmp = varargout_1_a * varargout_1_c * 2.0F + varargout_1_b * varargout_1_d *
    2.0F;
  if (tmp > 1.0F) {
    tmp = 1.0F;
  }

  if (tmp < -1.0F) {
    tmp = -1.0F;
  }

  x = varargout_1_a * varargout_1_a * 2.0F - 1.0F;
  euler[0] = 57.2957802F * rt_atan2f_snf(qa * qb * 2.0F - qc * b_x * 2.0F, x +
    varargout_1_d * varargout_1_d * 2.0F);
  euler[1] = 57.2957802F * asinf(tmp);
  euler[2] = 57.2957802F * rt_atan2f_snf(qa * b_x * 2.0F - qb * qc * 2.0F, x +
    varargout_1_b * varargout_1_b * 2.0F);
}

/*
 * File trailer for fusion.c
 *
 * [EOF]
 */
