/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: _coder_fusion_info.h
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 13-Mar-2020 00:10:46
 */

#ifndef _CODER_FUSION_INFO_H
#define _CODER_FUSION_INFO_H

/* Include Files */
#include "mex.h"

/* Function Declarations */
MEXFUNCTION_LINKAGE mxArray *emlrtMexFcnProperties(void);

#endif

/*
 * File trailer for _coder_fusion_info.h
 *
 * [EOF]
 */
