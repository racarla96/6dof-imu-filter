/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: _coder_fusion_api.h
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 13-Mar-2020 00:10:46
 */

#ifndef _CODER_FUSION_API_H
#define _CODER_FUSION_API_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"

/* Variable Declarations */
extern emlrtCTX emlrtRootTLSGlobal;
extern emlrtContext emlrtContextGlobal;

/* Function Declarations */
extern void fusion(real32_T acc[3], real32_T gyro[3], real32_T mag[3], real32_T
                   euler[3], real32_T gyroRate[3]);
extern void fusion_api(const mxArray * const prhs[3], int32_T nlhs, const
  mxArray *plhs[2]);
extern void fusion_atexit(void);
extern void fusion_initialize(void);
extern void fusion_terminate(void);
extern void fusion_xil_shutdown(void);
extern void fusion_xil_terminate(void);

#endif

/*
 * File trailer for _coder_fusion_api.h
 *
 * [EOF]
 */
