/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: fusion.h
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 13-Mar-2020 00:10:46
 */

#ifndef FUSION_H
#define FUSION_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "fusion_types.h"

/* Function Declarations */
extern void fusion(const float acc[3], const float gyro[3], const float mag[3],
                   float euler[3], float gyroRate[3]);

#endif

/*
 * File trailer for fusion.h
 *
 * [EOF]
 */
