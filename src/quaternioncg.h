//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
// File: quaternioncg.h
//
// MATLAB Coder version            : 4.1
// C/C++ source code generated on  : 27-Oct-2019 21:49:13
//
#ifndef QUATERNIONCG_H
#define QUATERNIONCG_H

// Include Files
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "fusion_types.h"

// Function Declarations
extern void quaternioncg_quaternioncg(const float varargin_1[3], float *obj_a,
  float *obj_b, float *obj_c, float *obj_d);

#endif

//
// File trailer for quaternioncg.h
//
// [EOF]
//
