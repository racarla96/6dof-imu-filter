//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
// File: mtimes.h
//
// MATLAB Coder version            : 4.1
// C/C++ source code generated on  : 27-Oct-2019 21:49:13
//
#ifndef MTIMES_H
#define MTIMES_H

// Include Files
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "fusion_types.h"

// Function Declarations
extern void quaternionBase_mtimes(float x_a, float x_b, float x_c, float x_d,
  float y_a, float y_b, float y_c, float y_d, float *o_a, float *o_b, float *o_c,
  float *o_d);

#endif

//
// File trailer for mtimes.h
//
// [EOF]
//
