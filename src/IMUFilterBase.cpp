//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
// File: IMUFilterBase.cpp
//
// MATLAB Coder version            : 4.1
// C/C++ source code generated on  : 27-Oct-2019 21:49:13
//

// Include Files
#include <cmath>
#include <string.h>
#include "rt_nonfinite.h"
#include "IMUFilterBase.h"
#include "mtimes.h"
#include "quaternioncg.h"
#include "rotmat.h"
#include "IMUFusionCommon.h"
#include "NED.h"

// Function Definitions

//
// Arguments    : c_fusion_internal_coder_imufilt *obj
//                const float accelIn[3]
//                const float gyroIn[3]
//                float *orientOut_a
//                float *orientOut_b
//                float *orientOut_c
//                float *orientOut_d
// Return Type  : void
//
void IMUFilterBase_stepImpl(c_fusion_internal_coder_imufilt *obj, const float
  accelIn[3], const float gyroIn[3], float *orientOut_a, float *orientOut_b,
  float *orientOut_c, float *orientOut_d)
{
  float offDiag[3];
  float Rpost[9];
  c_fusion_internal_coder_imufilt *r0;
  float invpd;
  float psquared[4];
  float Rprior[9];
  double a;
  int rtemp;
  int k;
  bool exitg1;
  int r1;
  float f0;
  float f1;
  float f2;
  float f3;
  float invpa;
  float invpb;
  float b_Rpost[9];
  float H[27];
  int H_tmp;
  c_matlabshared_rotations_intern b_obj;
  static const signed char iv0[9] = { 1, 0, 0, 0, 1, 0, 0, 0, 1 };

  float Qw[81];
  float y_tmp[27];
  int r2;
  int b_H_tmp;
  int r3;
  float y[27];
  float b_tmp[27];
  float qerr_d;
  float Ppost[81];
  offDiag[0] = accelIn[0];
  offDiag[1] = accelIn[1];
  offDiag[2] = accelIn[2];
  if (obj->pFirstTime) {
    NED_ecompass(offDiag, Rpost);
    obj->pFirstTime = false;
    invpd = (Rpost[0] + Rpost[4]) + Rpost[8];
    psquared[0] = (1.0F + 2.0F * ((Rpost[0] + Rpost[4]) + Rpost[8])) - invpd;
    psquared[1] = (1.0F + 2.0F * Rpost[0]) - invpd;
    psquared[2] = (1.0F + 2.0F * Rpost[4]) - invpd;
    psquared[3] = (1.0F + 2.0F * Rpost[8]) - invpd;
    if (!rtIsNaNF(psquared[0])) {
      rtemp = 1;
    } else {
      rtemp = 0;
      k = 2;
      exitg1 = false;
      while ((!exitg1) && (k < 5)) {
        if (!rtIsNaNF(psquared[k - 1])) {
          rtemp = k;
          exitg1 = true;
        } else {
          k++;
        }
      }
    }

    if (rtemp == 0) {
      invpd = psquared[0];
      rtemp = 1;
    } else {
      invpd = psquared[rtemp - 1];
      r1 = rtemp + 1;
      for (k = r1; k < 5; k++) {
        f0 = psquared[k - 1];
        if (invpd < f0) {
          invpd = f0;
          rtemp = k;
        }
      }
    }

    switch (rtemp) {
     case 1:
      f0 = std::sqrt(invpd);
      f1 = 0.5F * f0;
      invpa = 0.5F / f0;
      f2 = invpa * (Rpost[7] - Rpost[5]);
      f3 = invpa * (Rpost[2] - Rpost[6]);
      f0 = invpa * (Rpost[3] - Rpost[1]);
      break;

     case 2:
      f0 = std::sqrt(invpd);
      f2 = 0.5F * f0;
      invpb = 0.5F / f0;
      f1 = invpb * (Rpost[7] - Rpost[5]);
      f3 = invpb * (Rpost[3] + Rpost[1]);
      f0 = invpb * (Rpost[2] + Rpost[6]);
      break;

     case 3:
      f0 = std::sqrt(invpd);
      f3 = 0.5F * f0;
      invpd = 0.5F / f0;
      f1 = invpd * (Rpost[2] - Rpost[6]);
      f2 = invpd * (Rpost[3] + Rpost[1]);
      f0 = invpd * (Rpost[7] + Rpost[5]);
      break;

     default:
      invpd = std::sqrt(invpd);
      f0 = 0.5F * invpd;
      invpd = 0.5F / invpd;
      f1 = invpd * (Rpost[3] - Rpost[1]);
      f2 = invpd * (Rpost[2] + Rpost[6]);
      f3 = invpd * (Rpost[7] + Rpost[5]);
      break;
    }

    if (f1 < 0.0F) {
      f1 = -f1;
      f2 = -f2;
      f3 = -f3;
      f0 = -f0;
    }

    b_obj.a = f1;
    b_obj.b = f2;
    b_obj.c = f3;
    b_obj.d = f0;
    obj->pOrientPost = b_obj;
  }

  r0 = obj;
  obj->pOrientPrior = obj->pOrientPost;
  offDiag[0] = obj->pGyroOffset[0];
  offDiag[1] = obj->pGyroOffset[1];
  offDiag[2] = obj->pGyroOffset[2];
  c_IMUFusionCommon_predictOrient(r0, gyroIn, offDiag, &obj->pOrientPrior);
  quaternionBase_rotmat(obj->pOrientPrior.a, obj->pOrientPrior.b,
                        obj->pOrientPrior.c, obj->pOrientPrior.d, Rprior);
  a = obj->LinearAccelerationDecayFactor;
  obj->pLinAccelPrior[0] = (float)a * obj->pLinAccelPost[0];
  obj->pLinAccelPrior[1] = (float)a * obj->pLinAccelPost[1];
  obj->pLinAccelPrior[2] = (float)a * obj->pLinAccelPost[2];
  for (r1 = 0; r1 < 9; r1++) {
    Rpost[r1] = 0.0F;
  }

  Rpost[3] = Rprior[8];
  Rpost[6] = -Rprior[7];
  Rpost[7] = Rprior[6];
  for (r1 = 0; r1 < 3; r1++) {
    b_Rpost[3 * r1] = Rpost[3 * r1];
    rtemp = 1 + 3 * r1;
    b_Rpost[rtemp] = Rpost[rtemp] - Rpost[r1 + 3];
    rtemp = 2 + 3 * r1;
    b_Rpost[rtemp] = Rpost[rtemp] - Rpost[r1 + 6];
  }

  for (r1 = 0; r1 < 9; r1++) {
    Rpost[r1] = b_Rpost[r1];
  }

  for (r1 = 0; r1 < 3; r1++) {
    H[3 * r1] = Rpost[3 * r1];
    H_tmp = 1 + 3 * r1;
    H[H_tmp] = Rpost[H_tmp];
    H_tmp = 2 + 3 * r1;
    H[H_tmp] = Rpost[H_tmp];
  }

  for (r1 = 0; r1 < 3; r1++) {
    H_tmp = 3 * (r1 + 3);
    H[H_tmp] = -Rpost[3 * r1] * (float)obj->pKalmanPeriod;
    H[1 + H_tmp] = -Rpost[1 + 3 * r1] * (float)obj->pKalmanPeriod;
    H[2 + H_tmp] = -Rpost[2 + 3 * r1] * (float)obj->pKalmanPeriod;
  }

  for (r1 = 0; r1 < 3; r1++) {
    H_tmp = 3 * (r1 + 6);
    H[H_tmp] = iv0[3 * r1];
    H[1 + H_tmp] = iv0[1 + 3 * r1];
    H[2 + H_tmp] = iv0[2 + 3 * r1];
  }

  for (r1 = 0; r1 < 81; r1++) {
    Qw[r1] = obj->pQw[r1];
  }

  for (r1 = 0; r1 < 3; r1++) {
    for (H_tmp = 0; H_tmp < 9; H_tmp++) {
      rtemp = r1 + 3 * H_tmp;
      y_tmp[rtemp] = 0.0F;
      f0 = 0.0F;
      for (b_H_tmp = 0; b_H_tmp < 9; b_H_tmp++) {
        f0 += H[r1 + 3 * b_H_tmp] * Qw[b_H_tmp + 9 * H_tmp];
      }

      y_tmp[rtemp] = f0;
      b_tmp[H_tmp + 9 * r1] = H[rtemp];
    }
  }

  for (r1 = 0; r1 < 3; r1++) {
    for (H_tmp = 0; H_tmp < 3; H_tmp++) {
      f0 = 0.0F;
      for (b_H_tmp = 0; b_H_tmp < 9; b_H_tmp++) {
        f0 += y_tmp[H_tmp + 3 * b_H_tmp] * b_tmp[b_H_tmp + 9 * r1];
      }

      Rpost[r1 + 3 * H_tmp] = f0 + (float)obj->pQv[H_tmp + 3 * r1];
    }
  }

  for (r1 = 0; r1 < 9; r1++) {
    for (H_tmp = 0; H_tmp < 3; H_tmp++) {
      rtemp = r1 + 9 * H_tmp;
      y[rtemp] = 0.0F;
      f0 = 0.0F;
      for (b_H_tmp = 0; b_H_tmp < 9; b_H_tmp++) {
        f0 += Qw[r1 + 9 * b_H_tmp] * b_tmp[b_H_tmp + 9 * H_tmp];
      }

      y[rtemp] = f0;
    }
  }

  r1 = 0;
  r2 = 1;
  r3 = 2;
  invpd = std::abs(Rpost[0]);
  invpa = std::abs(Rpost[1]);
  if (invpa > invpd) {
    invpd = invpa;
    r1 = 1;
    r2 = 0;
  }

  if (std::abs(Rpost[2]) > invpd) {
    r1 = 2;
    r2 = 1;
    r3 = 0;
  }

  Rpost[r2] /= Rpost[r1];
  Rpost[r3] /= Rpost[r1];
  Rpost[3 + r2] -= Rpost[r2] * Rpost[3 + r1];
  Rpost[3 + r3] -= Rpost[r3] * Rpost[3 + r1];
  Rpost[6 + r2] -= Rpost[r2] * Rpost[6 + r1];
  Rpost[6 + r3] -= Rpost[r3] * Rpost[6 + r1];
  if (std::abs(Rpost[3 + r3]) > std::abs(Rpost[3 + r2])) {
    rtemp = r2;
    r2 = r3;
    r3 = rtemp;
  }

  Rpost[3 + r3] /= Rpost[3 + r2];
  Rpost[6 + r3] -= Rpost[3 + r3] * Rpost[6 + r2];
  for (k = 0; k < 9; k++) {
    H_tmp = k + 9 * r1;
    H[H_tmp] = y[k] / Rpost[r1];
    rtemp = k + 9 * r2;
    H[rtemp] = y[9 + k] - H[H_tmp] * Rpost[3 + r1];
    b_H_tmp = k + 9 * r3;
    H[b_H_tmp] = y[18 + k] - H[H_tmp] * Rpost[6 + r1];
    H[rtemp] /= Rpost[3 + r2];
    H[b_H_tmp] -= H[rtemp] * Rpost[6 + r2];
    H[b_H_tmp] /= Rpost[6 + r3];
    H[rtemp] -= H[b_H_tmp] * Rpost[3 + r3];
    H[H_tmp] -= H[b_H_tmp] * Rpost[r3];
    H[H_tmp] -= H[rtemp] * Rpost[r2];
  }

  invpd = (accelIn[0] + obj->pLinAccelPrior[0]) - Rprior[6];
  invpa = (accelIn[1] + obj->pLinAccelPrior[1]) - Rprior[7];
  invpb = (accelIn[2] + obj->pLinAccelPrior[2]) - Rprior[8];
  for (r1 = 0; r1 < 9; r1++) {
    Rpost[r1] = (H[r1] * invpd + H[r1 + 9] * invpa) + H[r1 + 18] * invpb;
  }

  quaternioncg_quaternioncg(*(float (*)[3])&Rpost[0], &invpd, &invpa, &invpb,
    &qerr_d);
  invpa = -invpa;
  invpb = -invpb;
  qerr_d = -qerr_d;
  quaternionBase_mtimes(obj->pOrientPrior.a, obj->pOrientPrior.b,
                        obj->pOrientPrior.c, obj->pOrientPrior.d, invpd, invpa,
                        invpb, qerr_d, &f0, &f1, &f2, &f3);
  obj->pOrientPost.d = f3;
  obj->pOrientPost.c = f2;
  obj->pOrientPost.b = f1;
  obj->pOrientPost.a = f0;
  if (obj->pOrientPost.a < 0.0F) {
    b_obj = obj->pOrientPost;
    b_obj.a = -b_obj.a;
    b_obj.b = -b_obj.b;
    b_obj.c = -b_obj.c;
    b_obj.d = -b_obj.d;
    obj->pOrientPost = b_obj;
  }

  b_obj = obj->pOrientPost;
  invpd = std::sqrt(((b_obj.a * b_obj.a + b_obj.b * b_obj.b) + b_obj.c * b_obj.c)
                    + b_obj.d * b_obj.d);
  b_obj.a /= invpd;
  b_obj.b /= invpd;
  b_obj.c /= invpd;
  b_obj.d /= invpd;
  obj->pOrientPost = b_obj;
  obj->pGyroOffset[0] -= Rpost[3];
  obj->pGyroOffset[1] -= Rpost[4];
  obj->pGyroOffset[2] -= Rpost[5];
  obj->pLinAccelPost[0] = obj->pLinAccelPrior[0] - Rpost[6];
  obj->pLinAccelPost[1] = obj->pLinAccelPrior[1] - Rpost[7];
  obj->pLinAccelPost[2] = obj->pLinAccelPrior[2] - Rpost[8];
  for (r1 = 0; r1 < 9; r1++) {
    for (H_tmp = 0; H_tmp < 9; H_tmp++) {
      rtemp = r1 + 9 * H_tmp;
      Ppost[rtemp] = Qw[rtemp] - ((H[r1] * y_tmp[3 * H_tmp] + H[r1 + 9] * y_tmp
        [1 + 3 * H_tmp]) + H[r1 + 18] * y_tmp[2 + 3 * H_tmp]);
    }
  }

  memset(&Qw[0], 0, 81U * sizeof(float));
  a = obj->pKalmanPeriod;
  a *= a;
  invpd = (float)(obj->GyroscopeDriftNoise + obj->GyroscopeNoise);
  Qw[0] = Ppost[0] + (float)a * (Ppost[30] + invpd);
  Qw[10] = Ppost[10] + (float)a * (Ppost[40] + invpd);
  Qw[20] = Ppost[20] + (float)a * (Ppost[50] + invpd);
  Qw[30] = Ppost[30] + (float)obj->GyroscopeDriftNoise;
  Qw[40] = Ppost[40] + (float)obj->GyroscopeDriftNoise;
  Qw[50] = Ppost[50] + (float)obj->GyroscopeDriftNoise;
  a = -obj->pKalmanPeriod;
  f0 = (float)a * Qw[30];
  Qw[3] = f0;
  Qw[27] = f0;
  f0 = (float)a * Qw[40];
  Qw[13] = f0;
  Qw[37] = f0;
  f0 = (float)a * Qw[50];
  Qw[23] = f0;
  Qw[47] = f0;
  a = obj->LinearAccelerationDecayFactor;
  a *= a;
  Qw[60] = (float)a * Ppost[60] + (float)obj->LinearAccelerationNoise;
  Qw[70] = (float)a * Ppost[70] + (float)obj->LinearAccelerationNoise;
  Qw[80] = (float)a * Ppost[80] + (float)obj->LinearAccelerationNoise;
  for (r1 = 0; r1 < 81; r1++) {
    obj->pQw[r1] = Qw[r1];
  }

  b_obj = obj->pOrientPost;
  *orientOut_a = b_obj.a;
  *orientOut_b = b_obj.b;
  *orientOut_c = b_obj.c;
  *orientOut_d = b_obj.d;
}

//
// File trailer for IMUFilterBase.cpp
//
// [EOF]
//
