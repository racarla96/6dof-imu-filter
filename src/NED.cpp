//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
// File: NED.cpp
//
// MATLAB Coder version            : 4.1
// C/C++ source code generated on  : 27-Oct-2019 21:49:13
//

// Include Files
#include <cmath>
#include "rt_nonfinite.h"
#include "NED.h"

// Function Definitions

//
// Arguments    : const float a[3]
//                float R[9]
// Return Type  : void
//
void NED_ecompass(const float a[3], float R[9])
{
  float Reast[3];
  int xi;
  float x[9];
  int xpageoffset;
  bool y[3];
  bool b[9];
  bool exitg1;
  bool nanPageIdx;
  Reast[0] = a[1] * 0.0F - a[2] * 0.0F;
  Reast[1] = a[2] - a[0] * 0.0F;
  Reast[2] = a[0] * 0.0F - a[1];
  R[6] = a[0];
  R[3] = Reast[0];
  R[7] = a[1];
  R[4] = Reast[1];
  R[8] = a[2];
  R[5] = Reast[2];
  R[0] = Reast[1] * a[2] - Reast[2] * a[1];
  R[1] = Reast[2] * a[0] - Reast[0] * a[2];
  R[2] = Reast[0] * a[1] - Reast[1] * a[0];
  for (xi = 0; xi < 9; xi++) {
    x[xi] = R[xi] * R[xi];
  }

  for (xi = 0; xi < 3; xi++) {
    xpageoffset = xi * 3;
    Reast[xi] = x[xpageoffset];
    Reast[xi] += x[xpageoffset + 1];
    Reast[xi] += x[xpageoffset + 2];
  }

  Reast[0] = std::sqrt(Reast[0]);
  Reast[1] = std::sqrt(Reast[1]);
  Reast[2] = std::sqrt(Reast[2]);
  for (xi = 0; xi < 9; xi++) {
    x[xi] = R[xi];
  }

  for (xi = 0; xi < 3; xi++) {
    R[3 * xi] = x[3 * xi] / Reast[xi];
    xpageoffset = 1 + 3 * xi;
    R[xpageoffset] = x[xpageoffset] / Reast[xi];
    xpageoffset = 2 + 3 * xi;
    R[xpageoffset] = x[xpageoffset] / Reast[xi];
  }

  for (xi = 0; xi < 9; xi++) {
    b[xi] = rtIsNaNF(R[xi]);
  }

  y[0] = false;
  y[1] = false;
  y[2] = false;
  xi = 1;
  exitg1 = false;
  while ((!exitg1) && (xi <= 3)) {
    if (b[xi - 1]) {
      y[0] = true;
      exitg1 = true;
    } else {
      xi++;
    }
  }

  xi = 4;
  exitg1 = false;
  while ((!exitg1) && (xi <= 6)) {
    if (b[xi - 1]) {
      y[1] = true;
      exitg1 = true;
    } else {
      xi++;
    }
  }

  xi = 7;
  exitg1 = false;
  while ((!exitg1) && (xi <= 9)) {
    if (b[xi - 1]) {
      y[2] = true;
      exitg1 = true;
    } else {
      xi++;
    }
  }

  nanPageIdx = false;
  xi = 0;
  exitg1 = false;
  while ((!exitg1) && (xi < 3)) {
    if (y[xi]) {
      nanPageIdx = true;
      exitg1 = true;
    } else {
      xi++;
    }
  }

  if (nanPageIdx) {
    for (xi = 0; xi < 9; xi++) {
      R[xi] = 0.0F;
    }

    R[0] = 1.0F;
    R[4] = 1.0F;
    R[8] = 1.0F;
  }
}

//
// File trailer for NED.cpp
//
// [EOF]
//
