/*
 * BlockProcess.h
 *
 *  Created on: 27 oct. 2019
 *      Author: racarla
 */

#ifndef BLOCKPROCESS_H_
#define BLOCKPROCESS_H_

class BlockProcess { // @suppress("Class has a virtual method and non-virtual destructor")
protected:
	int T; // period in microseconds
	float *my_inputs;
	int my_insize;
	float *my_outputs;
	int my_outsize;
public:
	BlockProcess(float *inputs, int insize, float *outputs, int outsize, int period);
	virtual void step() = 0;
	int getPeriod();
};

#endif /* BLOCKPROCESS_H_ */
