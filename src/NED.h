//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
// File: NED.h
//
// MATLAB Coder version            : 4.1
// C/C++ source code generated on  : 27-Oct-2019 21:49:13
//
#ifndef NED_H
#define NED_H

// Include Files
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "fusion_types.h"

// Function Declarations
extern void NED_ecompass(const float a[3], float R[9]);

#endif

//
// File trailer for NED.h
//
// [EOF]
//
