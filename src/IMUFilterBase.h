//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
// File: IMUFilterBase.h
//
// MATLAB Coder version            : 4.1
// C/C++ source code generated on  : 27-Oct-2019 21:49:13
//
#ifndef IMUFILTERBASE_H
#define IMUFILTERBASE_H

// Include Files
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "fusion_types.h"

// Function Declarations
extern void IMUFilterBase_stepImpl(c_fusion_internal_coder_imufilt *obj, const
  float accelIn[3], const float gyroIn[3], float *orientOut_a, float
  *orientOut_b, float *orientOut_c, float *orientOut_d);

#endif

//
// File trailer for IMUFilterBase.h
//
// [EOF]
//
