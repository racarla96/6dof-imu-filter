/*
 * FilterTest.h
 *
 *  Created on: 27 oct. 2019
 *      Author: racarla
 */

#ifndef FILTERTEST_H_
#define FILTERTEST_H_

#include "BlockProcess.h"
#include "fusion_types.h"
#include "rt_nonfinite.h"


class KFAccGyro: public BlockProcess { // @suppress("Class has a virtual method and non-virtual destructor")
private:
	c_fusion_internal_coder_imufilt FUSE;
	int rpy_unwrap[3];
	float last_rpy[3];
	const float neg_limit = -200.0f;
	const float pos_limit = 200.0f;
public:
	KFAccGyro(float *inputs, int insize, float *outputs, int outsize,
			int period) :
			BlockProcess(inputs, insize, outputs, outsize, period) {
		rt_InitInfAndNaN(8U);
		FUSE.isInitialized = 0;

		rpy_unwrap[0] = 0;
		rpy_unwrap[1] = 0;
		rpy_unwrap[2] = 0;
		
		last_rpy[0] = 0;
		last_rpy[1] = 0;
		last_rpy[2] = 0;
	};
	void step();
};

#endif /* FILTERTEST_H_ */
