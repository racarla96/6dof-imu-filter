//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
// File: mtimes.cpp
//
// MATLAB Coder version            : 4.1
// C/C++ source code generated on  : 27-Oct-2019 21:49:13
//

// Include Files
#include "rt_nonfinite.h"
#include "mtimes.h"

// Function Definitions

//
// Arguments    : float x_a
//                float x_b
//                float x_c
//                float x_d
//                float y_a
//                float y_b
//                float y_c
//                float y_d
//                float *o_a
//                float *o_b
//                float *o_c
//                float *o_d
// Return Type  : void
//
void quaternionBase_mtimes(float x_a, float x_b, float x_c, float x_d, float y_a,
  float y_b, float y_c, float y_d, float *o_a, float *o_b, float *o_c, float
  *o_d)
{
  *o_a = ((x_a * y_a - x_b * y_b) - x_c * y_c) - x_d * y_d;
  *o_b = ((x_a * y_b + x_b * y_a) + x_c * y_d) - x_d * y_c;
  *o_c = ((x_a * y_c - x_b * y_d) + x_c * y_a) + x_d * y_b;
  *o_d = ((x_a * y_d + x_b * y_c) - x_c * y_b) + x_d * y_a;
}

//
// File trailer for mtimes.cpp
//
// [EOF]
//
