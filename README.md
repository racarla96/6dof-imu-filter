# 6 dof IMU Kalman Filter

This library is a 6 dof lib for imu with gyroscope and accelerometer of 3 axys each.

Inertial measurement unit

## Examples

This library contains examples with IMU MPU9250 using accelerometer (m/s^2) and
gyroscope (rad/s). The inputs are linear acceleration and angular velocity with 
the units specified before and the return are the angles in degrees.

The roll, pitch and yaw are unwrapped [-inf,inf], but you can modified 'KFAccGyro::step()'
at the end of function to correct for desired operation.

PD: I know that is rarely that the input are in radians and the output in degrees,
but for my purpose I need the angles in degrees. Convert the angles is a multiplication, so
it's free multiply and modify the code! ;) 


