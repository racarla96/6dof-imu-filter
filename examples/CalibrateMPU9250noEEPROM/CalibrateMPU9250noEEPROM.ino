#include "MPU9250.h"
//#include "EEPROM.h"


// an MPU-9250 object on SPI bus 0 with chip select 10
MPU9250 Imu(SPI,10);
int status;

// EEPROM buffer and variable to save accel and mag bias 
// and scale factors
uint8_t EepromBuffer[48];
float value;

void setup() {
  // SerialUSB to display instructions
  SerialUSB.begin(57600);
  while(!SerialUSB) {}
  // start communication with IMU 
  status = Imu.begin();
  if (status < 0) {
    SerialUSB.println("IMU initialization unsuccessful");
    SerialUSB.println("Check IMU wiring or try cycling power");
    SerialUSB.print("Status: ");
    SerialUSB.println(status);
    while(1) {}
  }
  // calibrating accelerometer
  SerialUSB.println("Starting Accelerometer Calibration");
  Imu.calibrateAccel();
  SerialUSB.println("Switch");
  delay(5000);
  Imu.calibrateAccel();
  SerialUSB.println("Switch");
  delay(5000);
  Imu.calibrateAccel();
  SerialUSB.println("Switch");
  delay(5000);
  Imu.calibrateAccel();
  SerialUSB.println("Switch");
  delay(5000);
  Imu.calibrateAccel();
  SerialUSB.println("Switch");
  delay(5000);
  Imu.calibrateAccel();
  SerialUSB.println("Done");
  SerialUSB.println("Starting Magnetometer Calibration");
  delay(5000);
  // calibrating magnetometer
  Imu.calibrateMag();
  SerialUSB.println("Calibrated");
  SerialUSB.println("-------------------------------------------");
  // saving to EEPROM
  value = Imu.getAccelBiasX_mss();
  SerialUSB.print("float AccelBiasX =  ");
  SerialUSB.print(value, 4);
  SerialUSB.println(";");
  //memcpy(EepromBuffer,&value,4);
  
  value = Imu.getAccelScaleFactorX();
  SerialUSB.print("float AccelScaleFactorX =  ");
    SerialUSB.print(value, 4);
  SerialUSB.println(";");
  //memcpy(EepromBuffer+4,&value,4);
  
  value = Imu.getAccelBiasY_mss();
  SerialUSB.print("float AccelBiasY =  ");
    SerialUSB.print(value, 4);
  SerialUSB.println(";");
  //memcpy(EepromBuffer+8,&value,4);
  
  value = Imu.getAccelScaleFactorY();
  SerialUSB.print("float AccelScaleFactorY =  ");
    SerialUSB.print(value, 4);
  SerialUSB.println(";");
  //memcpy(EepromBuffer+12,&value,4);
  
  value = Imu.getAccelBiasZ_mss();
  SerialUSB.print("float AccelBiasZ =  ");
    SerialUSB.print(value, 4);
  SerialUSB.println(";");
  //memcpy(EepromBuffer+16,&value,4);
  
  value = Imu.getAccelScaleFactorZ();
  SerialUSB.print("float AccelScaleFactorZ =  ");
    SerialUSB.print(value, 4);
  SerialUSB.println(";");
  //memcpy(EepromBuffer+20,&value,4);
  
  value = Imu.getMagBiasX_uT();
  SerialUSB.print("float MagBiasX =  ");
    SerialUSB.print(value, 4);
  SerialUSB.println(";");
  //memcpy(EepromBuffer+24,&value,4);
  
  value = Imu.getMagScaleFactorX();
  SerialUSB.print("float MagScaleFactorX =  ");
    SerialUSB.print(value, 4);
  SerialUSB.println(";");
  //memcpy(EepromBuffer+28,&value,4);
  
  value = Imu.getMagBiasY_uT();
  SerialUSB.print("float MagBiasY =  ");
    SerialUSB.print(value, 4);
  SerialUSB.println(";");
  //memcpy(EepromBuffer+32,&value,4);
  
  value = Imu.getMagScaleFactorY();
  SerialUSB.print("float MagScaleFactorY =  ");
    SerialUSB.print(value, 4);
  SerialUSB.println(";");
  //memcpy(EepromBuffer+36,&value,4);
  
  value = Imu.getMagBiasZ_uT();
  SerialUSB.print("float MagBiasZ =  ");
    SerialUSB.print(value, 4);
  SerialUSB.println(";");
  //memcpy(EepromBuffer+40,&value,4);
  
  value = Imu.getMagScaleFactorZ();
  SerialUSB.print("float MagScaleFactorZ =  ");
    SerialUSB.print(value, 4);
  SerialUSB.println(";");
  //memcpy(EepromBuffer+44,&value,4);
  
  //for (size_t i=0; i < sizeof(EepromBuffer); i++) {
  //  EEPROM.write(i,EepromBuffer[i]);
  //}
  SerialUSB.println("-------------------------------------------");
  SerialUSB.println("Done");
}

void loop() {}
