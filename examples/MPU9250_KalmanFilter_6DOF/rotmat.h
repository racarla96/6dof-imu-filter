//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
// File: rotmat.h
//
// MATLAB Coder version            : 4.1
// C/C++ source code generated on  : 27-Oct-2019 21:49:13
//
#ifndef ROTMAT_H
#define ROTMAT_H

// Include Files
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "fusion_types.h"

// Function Declarations
extern void quaternionBase_rotmat(float q_a, float q_b, float q_c, float q_d,
  float r[9]);

#endif

//
// File trailer for rotmat.h
//
// [EOF]
//
