/*
 * BlockProcess.cpp
 *
 *  Created on: 27 oct. 2019
 *      Author: racarla
 */

#include "BlockProcess.h"

BlockProcess::BlockProcess(float *inputs, int insize, float *outputs,
		int outsize, int period) {
	T = period;
	my_inputs = inputs;
	my_insize = insize;
	my_outputs = outputs;
	my_outsize = outsize;
}

int BlockProcess::getPeriod() {
	return T;
}

