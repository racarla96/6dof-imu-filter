//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
// File: IMUFusionCommon.cpp
//
// MATLAB Coder version            : 4.1
// C/C++ source code generated on  : 27-Oct-2019 21:49:13
//

// Include Files
#include "rt_nonfinite.h"
#include "IMUFusionCommon.h"
#include "mtimes.h"
#include "quaternioncg.h"

// Function Definitions

//
// Arguments    : const c_fusion_internal_coder_imufilt *obj
//                const float gfast[3]
//                const float offset[3]
//                c_matlabshared_rotations_intern *qorient
// Return Type  : void
//
void c_IMUFusionCommon_predictOrient(const c_fusion_internal_coder_imufilt *obj,
  const float gfast[3], const float offset[3], c_matlabshared_rotations_intern
  *qorient)
{
  float c[3];
  float t0_a;
  float t0_b;
  float t0_c;
  float t0_d;
  float f4;
  float f5;
  float f6;
  float f7;
  c[0] = (gfast[0] - offset[0]) * (float)obj->pSensorPeriod;
  c[1] = (gfast[1] - offset[1]) * (float)obj->pSensorPeriod;
  c[2] = (gfast[2] - offset[2]) * (float)obj->pSensorPeriod;
  quaternioncg_quaternioncg(c, &t0_a, &t0_b, &t0_c, &t0_d);
  quaternionBase_mtimes(qorient->a, qorient->b, qorient->c, qorient->d, t0_a,
                        t0_b, t0_c, t0_d, &f4, &f5, &f6, &f7);
  qorient->d = f7;
  qorient->c = f6;
  qorient->b = f5;
  qorient->a = f4;
  if (qorient->a < 0.0F) {
    qorient->a = -qorient->a;
    qorient->b = -qorient->b;
    qorient->c = -qorient->c;
    qorient->d = -qorient->d;
  }
}

//
// File trailer for IMUFusionCommon.cpp
//
// [EOF]
//
