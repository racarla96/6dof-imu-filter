//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
// File: fusion_types.h
//
// MATLAB Coder version            : 4.1
// C/C++ source code generated on  : 27-Oct-2019 21:49:13
//
#ifndef FUSION_TYPES_H
#define FUSION_TYPES_H

// Include Files
#include "rtwtypes.h"

// Type Definitions
typedef struct {
  float a;
  float b;
  float c;
  float d;
} c_matlabshared_rotations_intern;

typedef struct {
  unsigned int f1[8];
} cell_wrap_3;

typedef struct {
  int isInitialized;
  bool TunablePropsChanged;
  cell_wrap_3 inputVarSize[2];
  double AccelerometerNoise;
  double GyroscopeNoise;
  double GyroscopeDriftNoise;
  double LinearAccelerationNoise;
  double LinearAccelerationDecayFactor;
  float pQw[81];
  double pQv[9];
  c_matlabshared_rotations_intern pOrientPost;
  c_matlabshared_rotations_intern pOrientPrior;
  bool pFirstTime;
  double pSensorPeriod;
  double pKalmanPeriod;
  float pGyroOffset[3];
  float pLinAccelPrior[3];
  float pLinAccelPost[3];
  float pInputPrototype[3];
} c_fusion_internal_coder_imufilt;

#endif

//
// File trailer for fusion_types.h
//
// [EOF]
//
