#include "MPU9250.h"
#include "KFAccGyro.h"
#include "BlockProcess.h"

void runFilter();
void callAbstract(BlockProcess *bp);

float in[6];
float out[3];
KFAccGyro ejem1(in, 6, out, 3, 10);

unsigned long t1;

float AccelBiasX =  0.0000;
float AccelScaleFactorX =  1.0000;
float AccelBiasY =  0.0000;
float AccelScaleFactorY =  1.0000;
float AccelBiasZ =  0.0000;
float AccelScaleFactorZ =  1.0000;
float MagBiasX =  20.8018;
float MagScaleFactorX =  0.5780;
float MagBiasY =  -8.9154;
float MagScaleFactorY =  1.3373;
float MagBiasZ =  5.9940;
float MagScaleFactorZ =  1.9157;

// an MPU-9250 object on SPI bus 0 with chip select 10
MPU9250 Imu(SPI,10);
int begin_status;
// a flag for when the MPU-9250 has new data
volatile int newData;
// Load accel and mag bias 
// and scale factors from CalibrateMPU9250.ino
float axb, axs, ayb, ays, azb, azs;
float hxb, hxs, hyb, hys, hzb, hzs;

// trim
float roll_trim = 0.35f;
float pitch_trim = -2.40f;

void setup() {
  // serial to display data
  Serial.begin(57600);
  while(!Serial) {}

    
  //Serial.println("Started...");
  // start communication with IMU 
  begin_status = Imu.begin();
  if (begin_status < 0) {
    Serial.println("IMU initialization unsuccessful");
    Serial.println("Check IMU wiring or try cycling power");
    Serial.print("Status: ");
    Serial.println(begin_status);
    while(1) {}
  }
    
  axb = AccelBiasX;
  axs = AccelScaleFactorX;
  ayb = AccelBiasY;
  ays = AccelScaleFactorY;
  azb = AccelBiasZ;
  azs = AccelScaleFactorZ;
  hxb = MagBiasX;
  hxs = MagScaleFactorX;
  hyb = MagBiasY;
  hys = MagScaleFactorY;
  hzb = MagBiasZ;
  hzs = MagScaleFactorZ;

  Imu.setAccelCalX(axb,axs);
  Imu.setAccelCalY(ayb,ays);
  Imu.setAccelCalZ(azb,azs);

  Imu.setMagCalX(hxb,hxs);
  Imu.setMagCalY(hyb,hys);
  Imu.setMagCalZ(hzb,hzs);

  // setting a 41 Hz DLPF bandwidth
  Imu.setDlpfBandwidth(MPU9250::DLPF_BANDWIDTH_41HZ);
  // setting SRD to 9 for a 100 Hz update rate
  Imu.setSrd(9);
  // enabling the data ready interrupt
  Imu.enableDataReadyInterrupt();
  // attaching the interrupt to microcontroller pin 1
  pinMode(2,INPUT);
  attachInterrupt(2,runFilter,RISING);

}

void loop() {
  
  if (newData == 1) {
    newData = 0;
//    Serial.print(micros() - t1); // LoopTime
//    t1 = micros();

    // read the sensor
    Imu.readSensor();
    
    in[0] = Imu.getAccelX_mss();
    in[1] = Imu.getAccelY_mss();
    in[2] = Imu.getAccelZ_mss();
    in[3] = Imu.getGyroX_rads();
    in[4] = Imu.getGyroY_rads();
    in[5] = Imu.getGyroZ_rads();

    callAbstract(&ejem1);

//    Serial.print(0);
    Serial.print(",");
    Serial.print(out[0] + roll_trim);
    Serial.print(",");
    Serial.print(out[1] + pitch_trim);
    Serial.print(",");
    Serial.print(out[2]);

//        Serial.print(",");
//
//    Serial.print(in[3]);
//    Serial.print(",");
//    Serial.print(in[4]);
//    Serial.print(",");
//    Serial.print(in[5]);

    //Serial.print(";");

//    Serial.print(",");
//    Serial.print(micros() - t1); // ProcessTime
    
    Serial.println();

    

  }
}

void runFilter() {
  newData = 1;
}

void callAbstract(BlockProcess *bp){
  bp->step();
}
