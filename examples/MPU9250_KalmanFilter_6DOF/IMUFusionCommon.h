//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
// File: IMUFusionCommon.h
//
// MATLAB Coder version            : 4.1
// C/C++ source code generated on  : 27-Oct-2019 21:49:13
//
#ifndef IMUFUSIONCOMMON_H
#define IMUFUSIONCOMMON_H

// Include Files
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "fusion_types.h"

// Function Declarations
extern void c_IMUFusionCommon_predictOrient(const
  c_fusion_internal_coder_imufilt *obj, const float gfast[3], const float
  offset[3], c_matlabshared_rotations_intern *qorient);

#endif

//
// File trailer for IMUFusionCommon.h
//
// [EOF]
//
