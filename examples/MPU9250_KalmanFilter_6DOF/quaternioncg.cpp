//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
// File: quaternioncg.cpp
//
// MATLAB Coder version            : 4.1
// C/C++ source code generated on  : 27-Oct-2019 21:49:13
//

// Include Files
#include <cmath>
#include "rt_nonfinite.h"
#include "quaternioncg.h"

// Function Definitions

//
// Arguments    : const float varargin_1[3]
//                float *obj_a
//                float *obj_b
//                float *obj_c
//                float *obj_d
// Return Type  : void
//
void quaternioncg_quaternioncg(const float varargin_1[3], float *obj_a, float
  *obj_b, float *obj_c, float *obj_d)
{
  float theta;
  float st;
  *obj_a = 1.0F;
  *obj_b = 0.0F;
  *obj_c = 0.0F;
  *obj_d = 0.0F;
  theta = std::sqrt((varargin_1[0] * varargin_1[0] + varargin_1[1] * varargin_1
                     [1]) + varargin_1[2] * varargin_1[2]);
  st = std::sin(theta / 2.0F);
  if (theta != 0.0F) {
    *obj_a = std::cos(theta / 2.0F);
    *obj_b = varargin_1[0] / theta * st;
    *obj_c = varargin_1[1] / theta * st;
    *obj_d = varargin_1[2] / theta * st;
  }
}

//
// File trailer for quaternioncg.cpp
//
// [EOF]
//
