/*
 * FilterTest.cpp
 *
 *  Created on: 27 oct. 2019
 *      Author: racarla
 */

#include "KFAccGyro.h"

#include <cmath>

// Include Files
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "fusion_types.h"

#include "rt_defines.h"
#include "rt_nonfinite.h"
#include <string.h>
#include "IMUFilterBase.h"

//
// Arguments    : float u0
//                float u1
// Return Type  : float
//
static float rt_atan2f_snf(float u0, float u1)
{
  float y;
  int b_u0;
  int b_u1;
  if (rtIsNaNF(u0) || rtIsNaNF(u1)) {
    y = rtNaNF;
  } else if (rtIsInfF(u0) && rtIsInfF(u1)) {
    if (u0 > 0.0F) {
      b_u0 = 1;
    } else {
      b_u0 = -1;
    }

    if (u1 > 0.0F) {
      b_u1 = 1;
    } else {
      b_u1 = -1;
    }

    y = std::atan2((float)b_u0, (float)b_u1);
  } else if (u1 == 0.0F) {
    if (u0 > 0.0F) {
      y = RT_PIF / 2.0F;
    } else if (u0 < 0.0F) {
      y = -(RT_PIF / 2.0F);
    } else {
      y = 0.0F;
    }
  } else {
    y = std::atan2(u0, u1);
  }

  return y;
}

void KFAccGyro::step(){

	if(my_insize < 6) return;
	if(my_outsize < 3) return;

	float accX =  my_inputs[0];
	float accY =  my_inputs[1];
	float accZ =  my_inputs[2];

	float gyroX = my_inputs[3];
	float gyroY = my_inputs[4];
	float gyroZ = my_inputs[5];

  float rpy[3];
  float varargin_1[3];
  float b_gyroX[3];
  double accelMeasNoiseVar;
  int i0;
  float q_a;
  float q_b;
  float q_c;
  float q_d;
  float n;
  signed char b_I[9];
  float x;
  float tmp;
  float b_x;
  float c_x;
  float qa;
  float qb;
  float qc;
  static const float fv0[81] = { 6.09234849E-6F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F,
    0.0F, 0.0F, 0.0F, 0.0F, 6.09234849E-6F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F,
    0.0F, 0.0F, 0.0F, 6.09234849E-6F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F,
    0.0F, 0.0F, 7.61543561E-5F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F,
    0.0F, 7.61543561E-5F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F,
    7.61543561E-5F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F,
    0.00962361F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F,
    0.00962361F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F,
    0.00962361F };

  varargin_1[0] = accX;
  varargin_1[1] = accY;
  varargin_1[2] = accZ;
  if (FUSE.isInitialized != 1) {
    FUSE.AccelerometerNoise = 0.0001924722;
    FUSE.GyroscopeNoise = 9.1385E-5;
    FUSE.GyroscopeDriftNoise = 3.0462E-13;
    FUSE.LinearAccelerationNoise = 0.0096236100000000012;
    FUSE.LinearAccelerationDecayFactor = 0.5;
    FUSE.isInitialized = 1;
    FUSE.pInputPrototype[0] = accX;
    FUSE.pInputPrototype[1] = accY;
    FUSE.pInputPrototype[2] = accZ;
    FUSE.pSensorPeriod = ((float) T)/1000.0f; // ms to seconds
    FUSE.pKalmanPeriod = FUSE.pSensorPeriod;
    FUSE.TunablePropsChanged = false;
    FUSE.pOrientPost.a = 1.0F;
    FUSE.pOrientPost.b = 0.0F;
    FUSE.pOrientPost.c = 0.0F;
    FUSE.pOrientPost.d = 0.0F;
    FUSE.pGyroOffset[0] = 0.0F;
    FUSE.pGyroOffset[1] = 0.0F;
    FUSE.pGyroOffset[2] = 0.0F;
    accelMeasNoiseVar = FUSE.pKalmanPeriod;
    accelMeasNoiseVar = (FUSE.AccelerometerNoise + FUSE.LinearAccelerationNoise)
      + accelMeasNoiseVar * accelMeasNoiseVar * (FUSE.GyroscopeDriftNoise +
      FUSE.GyroscopeNoise);
    for (i0 = 0; i0 < 9; i0++) {
      b_I[i0] = 0;
    }

    b_I[0] = 1;
    b_I[4] = 1;
    b_I[8] = 1;
    for (i0 = 0; i0 < 9; i0++) {
      FUSE.pQv[i0] = accelMeasNoiseVar * (double)b_I[i0];
    }

    memcpy(&FUSE.pQw[0], &fv0[0], 81U * sizeof(float));
    FUSE.pLinAccelPost[0] = 0.0F;
    FUSE.pLinAccelPost[1] = 0.0F;
    FUSE.pLinAccelPost[2] = 0.0F;
    FUSE.pFirstTime = true;
  }

  if (FUSE.TunablePropsChanged) {
    FUSE.TunablePropsChanged = false;
    accelMeasNoiseVar = FUSE.pKalmanPeriod;
    accelMeasNoiseVar = (FUSE.AccelerometerNoise + FUSE.LinearAccelerationNoise)
      + accelMeasNoiseVar * accelMeasNoiseVar * (FUSE.GyroscopeDriftNoise +
      FUSE.GyroscopeNoise);
    for (i0 = 0; i0 < 9; i0++) {
      b_I[i0] = 0;
    }

    b_I[0] = 1;
    b_I[4] = 1;
    b_I[8] = 1;
    for (i0 = 0; i0 < 9; i0++) {
      FUSE.pQv[i0] = accelMeasNoiseVar * (double)b_I[i0];
    }
  }

  b_gyroX[0] = gyroX;
  b_gyroX[1] = gyroY;
  b_gyroX[2] = gyroZ;
  IMUFilterBase_stepImpl(&FUSE, varargin_1, b_gyroX, &q_a, &q_b, &q_c, &q_d);
  n = std::sqrt(((q_a * q_a + q_b * q_b) + q_c * q_c) + q_d * q_d);
  x = q_a;
  q_a /= n;
  tmp = q_b;
  q_b /= n;
  b_x = q_c;
  q_c /= n;
  c_x = q_d;
  q_d /= n;
  qa = x / n;
  qb = tmp / n;
  qc = b_x / n;
  b_x = c_x / n;
  tmp = q_a * q_c * 2.0F + q_b * q_d * 2.0F;
  if (tmp > 1.0F) {
    tmp = 1.0F;
  }

  if (tmp < -1.0F) {
    tmp = -1.0F;
  }

  x = q_a * q_a * 2.0F - 1.0F;
  rpy[0] = 57.2957802F * rt_atan2f_snf(qa * qb * 2.0F - qc * b_x * 2.0F, x + q_d
    * q_d * 2.0F);
  rpy[1] = 57.2957802F * std::asin(tmp);
  rpy[2] = 57.2957802F * rt_atan2f_snf(qa * b_x * 2.0F - qb * qc * 2.0F, x + q_b
    * q_b * 2.0F);

  // Unwrap
  tmp = last_rpy[0] - rpy[0];
  if(tmp > pos_limit) rpy_unwrap[0]++;
  if(tmp < neg_limit) rpy_unwrap[0]--;

  tmp = last_rpy[1] - rpy[1];
  if(tmp > pos_limit) rpy_unwrap[1]++;
  if(tmp < neg_limit) rpy_unwrap[1]--;
  
  tmp = last_rpy[2] - rpy[2];
  if(tmp > pos_limit) rpy_unwrap[2]++;
  if(tmp < neg_limit) rpy_unwrap[2]--;

  my_outputs[0] = rpy[0] + 360.0f * (float) rpy_unwrap[0] - 180.0f;
  my_outputs[1] = rpy[1] + 360.0f * (float) rpy_unwrap[1];
  my_outputs[2] = rpy[2] + 360.0f * (float) rpy_unwrap[2];

  memcpy(&last_rpy[0], &rpy[0], 3U * sizeof(float));
}
